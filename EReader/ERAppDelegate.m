//
//  ERAppDelegate.m
//  EReader
//
//  Created by Hudajiang on 13-11-20.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import "ERAppDelegate.h"
#import "MIMenuViewController.h"
#import "ERHomeViewController.h"
#import "MMDrawerController.h"

#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WeiboApi.h"
#import "EROfflineManagerViewController.h"
#import "DirectoryWatcher.h"
#import "EROfflineBookObj.h"

@implementation ERAppDelegate
@synthesize homeViewController;
@synthesize menuViewController;
@synthesize drawerViewController;
@synthesize window = _window;

- (void)dealloc
{
    self.drawerViewController = nil;
    self.homeViewController = nil;
    self.menuViewController = nil;
    [_window release];
    [super dealloc];
}

- (void)initializePlat
{
    
    //添加新浪微博应用
    [ShareSDK connectSinaWeiboWithAppKey:@"3179622900"
                               appSecret:@"ed985a6618c4c28fd9d51e06673f1b58"
                             redirectUri:@"http://weibo.com/u/5044717379"];
    
    
    //添加QQ应用
    [ShareSDK connectQZoneWithAppKey:@"101098010"
                           appSecret:@"d2240e80297dc72b9c7fba4e2fe32583"
                   qqApiInterfaceCls:[QQApiInterface class]
                     tencentOAuthCls:[TencentOAuth class]];
    
    // 连接腾讯微博
    [ShareSDK connectTencentWeiboWithAppKey:@"801508638"
                                  appSecret:@"e7d25b2d7b9ee3e4de9813d11ff9202e"
                                redirectUri:APPStoreURL
                                   wbApiCls:[WeiboApi class]];
    
    //添加微信应用
    [ShareSDK connectWeChatWithAppId:@"wx97ae16c038d7654a"        //此参数为申请的微信AppID
                           wechatCls:[WXApi class]];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //ShareSDK
    [ShareSDK registerApp:@"1d78910bf460"];     //参数为ShareSDK官网中添加应用后得到的AppKey
    [ShareSDK useAppTrusteeship:NO];
    [self initializePlat];

    //Umeng  糖醋小说 key
    [MobClick startWithAppkey:@"537b0a4f56240b9be102ccd0" reportPolicy:(ReportPolicy) REALTIME channelId:nil];
    [MobClick updateOnlineConfig];  //在线参数配置
    [MobClick checkUpdate];
    
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    self.homeViewController = [[[ERHomeViewController alloc] initWithNibName:nil bundle:nil] autorelease];
    self.menuViewController = [[[MIMenuViewController alloc] initWithNibName:nil bundle:nil] autorelease];
    
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:self.homeViewController];
    if (!IsIOS7) {
//        [navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_bg"] forBarMetrics:UIBarMetricsDefault];
        navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], UITextAttributeTextColor,[UIFont systemFontOfSize:18.0],UITextAttributeFont,nil];
        
        navigationController.navigationBar.tintColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:0.9];
    }
    
    self.drawerViewController = [[[MMDrawerController alloc]
                                  initWithCenterViewController:navigationController
                                  leftDrawerViewController:self.menuViewController
                                  rightDrawerViewController:nil] autorelease];
    [self.drawerViewController setMaximumLeftDrawerWidth:270];
    [self.drawerViewController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.drawerViewController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    self.window.rootViewController = self.drawerViewController;
    
//        [navigationController release];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    [self watcherDocument];
    
    [navigationController release];
    return YES;
}

- (BOOL)application:(UIApplication *)application  handleOpenURL:(NSURL *)url
{
    return [ShareSDK handleOpenURL:url
                        wxDelegate:self];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [ShareSDK handleOpenURL:url
                 sourceApplication:sourceApplication
                        annotation:annotation
                        wxDelegate:self];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    

    //强制更新
    NSString *updateStr = [MobClick getConfigParams:@"forcedUpdates"];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    
    if (updateStr != nil && ![updateStr isEqualToString:@"nil"]) {
       NSArray *array =  [updateStr componentsSeparatedByString:@","];
        if ([array containsObject:version]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"更新" message:@"由于应用做了重大修改，请更新后再使用，谢谢！" delegate:self cancelButtonTitle:@"更新" otherButtonTitles: nil];
            [alertView show];
            [alertView release];
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"savePageIndexs" object:nil userInfo:nil];
}
-(void)setIsSearching:(BOOL)isSearching
{
    _isSearching = isSearching;
    if(_isSearching)
    {
        [self.drawerViewController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
        [self.drawerViewController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeNone];
    }
    else{
        [self.drawerViewController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
        [self.drawerViewController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    }
    
}

- (NSString *)applicationDocumentsDirectory
{
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}
//
-(void)watcherDocument
{
     [DirectoryWatcher watchFolderWithPath:[self applicationDocumentsDirectory] delegate:(id)self];
}
NSString * gen_uuid()
{
    CFUUIDRef uuid_ref = CFUUIDCreate(NULL);
    CFStringRef uuid_string_ref= CFUUIDCreateString(NULL, uuid_ref);
    CFRelease(uuid_ref);
    NSString *uuid = [NSString stringWithString:(NSString*)uuid_string_ref];
    CFRelease(uuid_string_ref);
    return uuid;
}
//-(NSString*) uuid {
//    CFUUIDRef puuid = CFUUIDCreate( nil );
//    CFStringRef uuidString = CFUUIDCreateString( nil, puuid );
//    NSString * result = (NSString *)CFStringCreateCopy( NULL, uuidString);
//    CFRelease(puuid);
//    CFRelease(uuidString);
//    return [result autorelease];
//}
- (void)directoryDidChange:(DirectoryWatcher *)folderWatcher
{
	
	NSString *documentsDirectoryPath = [self applicationDocumentsDirectory];
	
	NSArray *documentsDirectoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectoryPath
                                                                                              error:NULL];
    BOOL hasChange = NO;
	for (NSString* curFileName in [documentsDirectoryContents objectEnumerator])
	{
		NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:curFileName];
	
		BOOL isDirectory;
        [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
		
        // proceed to add the document URL to our list (ignore the "Inbox" folder)
        if (!isDirectory && [[curFileName lowercaseString] hasSuffix:@"txt"])
        {
            //move
            NSDictionary *fileDic = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
            
            
            EROfflineBookObj*offlineBookElement=[[EROfflineBookObj alloc] init];
            offlineBookElement.gId=gen_uuid();
            offlineBookElement.author =@"用户导入";
            offlineBookElement.title =[curFileName stringByDeletingPathExtension];
            offlineBookElement.size =[fileDic objectForKey:NSFileSize];

            [[NSFileManager defaultManager] moveItemAtPath:filePath toPath:[offlineBookElement bookPath] error:nil];
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            [offlineBookElement insertLocBook];
            [offlineBookElement release];
            
            hasChange = YES;
        }
	}
    if(hasChange)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userAddTxtFile" object:nil];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex == buttonIndex) {
        // go app store
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:APPStoreURL]];
        exit(0);
    }
}

@end
