//
//  ERAppDelegate.h
//  EReader
//
//  Created by Hudajiang on 13-11-20.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"
#import "MobClick.h"

@class MIMenuViewController;
@class ERHomeViewController;
@interface ERAppDelegate : UIResponder <UIApplicationDelegate>

@property (retain, nonatomic) UIWindow *window;
@property (retain, nonatomic) MMDrawerController *drawerViewController;
@property (retain, nonatomic) MIMenuViewController *menuViewController;
@property (retain, nonatomic) ERHomeViewController *homeViewController;

@property (nonatomic, assign) BOOL isSearching;

@end
