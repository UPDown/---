//
//  ERApi.h
//  ERReadder
//
//  Created by zym on 13-12-22.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import "ERApi.h"
#import "SBJson.h"
#import "ERUserObj.h"
#import "GTMBase64.h"
#import <CommonCrypto/CommonHMAC.h>
#import <CommonCrypto/CommonCryptor.h>
@interface ERApi()
{
    NSMutableData *receiveData; //接收数据
    NSURLConnection *connection;  //
    
    
 
}

@end
@implementation ERApi
@synthesize apiDelegate;

@synthesize fileUrlStr;
-(id)init{
    
    self = [super init];
    if(self)
    {
        receiveData = [[NSMutableData alloc] init];
    }
    return self;
}
-(void)dealloc
{
    if(connection)
    {
        [connection cancel];
        [connection release];
        connection = nil;
    }
    
    Block_release(filishBolck);
    Block_release(failBolck);
    self.fileUrlStr = nil;
    self.apiDelegate = nil;
    [receiveData release];
    [super dealloc];
}

-(void)setfinishBlock:(void (^)(NSDictionary *jsob))filish failBlock:(void (^)(NSString *errorStr))faild
{
    filishBolck = Block_copy(filish);
    failBolck = Block_copy(faild);
    

}


-(void)cancel
{
    if(connection)
    {
        [connection cancel];
        [connection release];
        connection = nil;
    }
    self.apiDelegate = nil;
}

-(NSTimeInterval)getTime
{
    NSDate *date = [NSDate date];
    return [date timeIntervalSince1970];
}
-(NSString *)timeStr
{
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"yy/MM/dd HH:mm:ss"];
    NSString *str =[formatter stringFromDate:date];
    return str;
}
-(NSString *)dateStr
{
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"yy-MM-dd-HH-mm"];
    NSString *str =[formatter stringFromDate:date];
    return str;
}



-(void)getCommentForBookId:(NSString *)bookId
                     count:(int)count
                     maxId:(NSString *)maxId
                     mixId:(NSString *)minId
{
    NSString *urlPath =[kAppServerPath stringByAppendingString:@"commentList.php"]; 
  
    NSString *dataStr = @"?";
    if(maxId)
    {
        dataStr= [dataStr stringByAppendingFormat:@"maxId=%@",maxId];
    }
    if(minId)
    {
        dataStr= [dataStr stringByAppendingFormat:@"minId=%@",minId];
    }
    
    
    if(bookId)
    {
        dataStr= [dataStr stringByAppendingFormat:@"&bookId=%@",bookId];
    }
    if(count >0)
    {
        dataStr= [dataStr stringByAppendingFormat:@"&count=%i",count];
    }
    
    urlPath = [urlPath stringByAppendingString:dataStr];
    urlPath = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *requst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
    
    [self creatConnectionWithRequst:requst];
}
-(void)getFavListForUserToken:(NSString *)userToken
                        count:(int)count
                        maxId:(NSString *)maxId
                        mixId:(NSString *)minId
{
    NSString *urlPath =    [kAppServerPath stringByAppendingString:@"favList.php"];    
    NSString *dataStr = @"?";
    if(maxId)
    {
        dataStr= [dataStr stringByAppendingFormat:@"maxId=%@",maxId];
    }
    if(minId)
    {
        dataStr= [dataStr stringByAppendingFormat:@"minId=%@",minId];
    }
    if(userToken)
    {
        dataStr= [dataStr stringByAppendingFormat:@"&token=%@",userToken];
    }
    if(count >0)
    {
        dataStr= [dataStr stringByAppendingFormat:@"&count=%i",count];
    }
    
    urlPath = [urlPath stringByAppendingString:dataStr];
    urlPath = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *requst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
    
    [self creatConnectionWithRequst:requst];
}
-(void)getMyPostForUserToken:(NSString *)userToken
                       count:(int)count
                       maxId:(NSString *)maxId
                       mixId:(NSString *)minId
{
    NSString *urlPath =[kAppServerPath stringByAppendingString:@"MyPost.php"];    
    NSString *dataStr = @"?";
    if(maxId)
    {
        dataStr= [dataStr stringByAppendingFormat:@"maxId=%@",maxId];
    }
    if(minId)
    {
        dataStr= [dataStr stringByAppendingFormat:@"minId=%@",minId];
    }
    if(userToken)
    {
        dataStr= [dataStr stringByAppendingFormat:@"&token=%@",userToken];
    }
    if(count >0)
    {
        dataStr= [dataStr stringByAppendingFormat:@"&count=%i",count];
    }
    
    urlPath = [urlPath stringByAppendingString:dataStr];
    urlPath = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *requst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]];

    [self creatConnectionWithRequst:requst];
}

-(void)loginWithUserEmail:(NSString *)email userPassword:(NSString *)userPassword
{
    NSString *urlPath = [kAppServerPath stringByAppendingString:@"login.php"];     
    NSString *dataStr = [NSString stringWithFormat:@"email=%@&password=%@",email,userPassword];
    
    urlPath = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *requst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
    [requst setHTTPMethod:@"POST"];
    [requst setHTTPBody:[dataStr dataUsingEncoding:NSUTF8StringEncoding]];
    
    [self creatConnectionWithRequst:requst];
}


-(void)registerWithUserEmail:(NSString *)email userPassword:(NSString *)userPassword
{
    NSString *urlPath =  [kAppServerPath stringByAppendingString:@"registered.php"];    
    NSString *dataStr = [NSString stringWithFormat:@"email=%@&password=%@",email,userPassword];
    
    urlPath = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *requst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
    [requst setHTTPMethod:@"POST"];
    [requst setHTTPBody:[dataStr dataUsingEncoding:NSUTF8StringEncoding]];
    
    [self creatConnectionWithRequst:requst];
}


-(void)postCommentWithBookId:(NSString *)bookId
                   userToken:(NSString *)userToken
                     comment:(NSString *)comment
{
    NSString *urlPath =[kAppServerPath stringByAppendingString:@"postComment.php"];     
    
    NSString *dataStr = [NSString stringWithFormat:@"commentContent=%@&bookId=%@&token=%@&creatDate=%f",comment,bookId,userToken,[self getTime]];
    urlPath = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *requst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
    [requst setHTTPMethod:@"POST"];
    [requst setHTTPBody:[dataStr dataUsingEncoding:NSUTF8StringEncoding]];
    //    [requst setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [self creatConnectionWithRequst:requst];
}


-(void)addFavWithBookId:(NSString *)bookId
              userToken:(NSString *)userToken
{
    
    NSString *urlPath = [kAppServerPath stringByAppendingString:@"addFav.php"];     
    NSString *dataStr = [NSString stringWithFormat:@"bookId=%@&token=%@&creatDate=%f",bookId,userToken,[self getTime]];
    
    urlPath = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *requst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
    [requst setHTTPMethod:@"POST"];

    [requst setHTTPBody:[dataStr dataUsingEncoding:NSUTF8StringEncoding]];
    
    [self creatConnectionWithRequst:requst];
}

-(void)removeFavWithFavId:(NSString *)favId
                 userToken:(NSString *)userToken
{
    NSString *urlPath =[kAppServerPath stringByAppendingString:@"removeFav.php"];    
    NSString *dataStr = [NSString stringWithFormat:@"favId=%@&token=%@",favId,userToken];
    
    urlPath = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *requst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
    [requst setHTTPMethod:@"POST"];

    [requst setHTTPBody:[dataStr dataUsingEncoding:NSUTF8StringEncoding]];
    
    [self creatConnectionWithRequst:requst];
}


//TODO: 用户修改信息接口
#define MULTIPART @"multipart/form-data; boundary=------------0x0x0x0x0x0x0x0x"
#define IMAGE_CONTENT @"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\nContent-Type:image/jpg\r\n\r\n"
#define STRING_CONTENT @"Content-Disposition: form-data; name=\"%@\"\r\n\r\n"

+ (NSString *)hmac_sha1:(NSString *)Content text:(NSString *)SecretKey{
    
    const char *cKey  = [Content cStringUsingEncoding:NSUTF8StringEncoding];
    const char *cData = [SecretKey cStringUsingEncoding:NSUTF8StringEncoding];
    
    char cHMAC[CC_SHA1_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:CC_SHA1_DIGEST_LENGTH];
    NSString *hash = [GTMBase64 stringByEncodingData:HMAC];//base64 编码。
    [HMAC release];
    return hash;
}
- (NSString*)encodeURLString:(NSString *)string
{
    
    NSString *newString = NSMakeCollectable([(NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                                 
                                                                                                 kCFAllocatorDefault,
                                                                                                 
                                                                                                 (CFStringRef)string, NULL, CFSTR(":?/#[]@!$ &'()*+,;=\"<>%{}|\\^~`"),
                                                                                                 
                                                                                                 kCFStringEncodingUTF8) autorelease]);
    
    if (newString) {
        
        return newString;
        
    }
    
    return @"";
    
}
-(NSString *)getsinWithFileName:(NSString *)fileName
{
    NSString *ak = @"E7cad353203067747bf6b55cb729bad6";
    NSString *sk= @"8b7f16904f657fd42f0934a4ec530d19";
    NSString *flag = @"MBO";
    NSString *content= [NSString stringWithFormat:@"%@\nMethod=%@\nBucket=%@\nObject=/%@\n",flag,@"PUT",kBucket,fileName];
    NSString *Signature = [[self class] hmac_sha1:content text:sk];
    NSString *sign =[NSString stringWithFormat:@"%@:%@:%@",flag,ak,[self encodeURLString:Signature]];
    return sign;
}
-(NSString *)getrandFileName
{
    CFUUIDRef uuidRef = CFUUIDCreate(nil);
    CFStringRef stringRef = CFUUIDCreateString(nil, uuidRef);
    NSString *uuidString = (NSString *)CFStringCreateCopy(nil, stringRef);
    CFRelease(uuidRef);
    CFRelease(stringRef);
    
    NSString *fileName = [uuidString stringByAppendingFormat:@"-%@",[self dateStr]];
    [uuidString release];
    
    return fileName;
}

-(void)postImageData:(NSData *)imageData isUserPic:(BOOL)isPic
{
    NSString *flieName = [self getrandFileName];
    if(isPic)
    {
        flieName = [@"userPic/" stringByAppendingString:flieName];
    }

    NSString *sign =[self getsinWithFileName:flieName];
    NSString *urlPath = [NSString stringWithFormat:@"http://bcs.duapp.com/%@/%@?sin=%@",kPicStoreName,flieName,sign];
   
    self.fileUrlStr = [NSString stringWithFormat:@"http://bcs.duapp.com/%@/%@",kPicStoreName,flieName];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
    [request setHTTPMethod:@"POST"];
    
    
    id boundary = @"------------0x0x0x0x0x0x0x0x";
    NSMutableData* postData = [NSMutableData data];
    
    //设置aid
    [postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *formstring = [NSString stringWithFormat:STRING_CONTENT, @"Filename"];
    [postData appendData: [formstring dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"%@",flieName] dataUsingEncoding:NSUTF8StringEncoding]];
    
    formstring = @"\r\n";
    [postData appendData: [formstring dataUsingEncoding:NSUTF8StringEncoding]];
    
    //文件data
    [postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    formstring = [NSString stringWithFormat:IMAGE_CONTENT, @"Filedata",flieName];
    [postData appendData: [formstring dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:imageData];
    
    formstring = @"\r\n";
    [postData appendData: [formstring dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    formstring = [NSString stringWithFormat:STRING_CONTENT, @"Upload"];
    [postData appendData: [formstring dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"%@",@"Submit Query"] dataUsingEncoding:NSUTF8StringEncoding]];
    
//    formstring = @"\r\n";
    
    
    //表单结束
    formstring =[NSString stringWithFormat:@"--%@--\r\n", boundary];
    [postData appendData: [formstring dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setValue:MULTIPART forHTTPHeaderField: @"Content-Type"];
    [request setHTTPBody:postData];
    
    [self creatConnectionWithRequst:request];
}
#pragma mark 创建一个url请求
-(void)creatConnectionWithRequst:(NSURLRequest *)requst
{
    if(connection)
    {
        [connection cancel];
        [connection release];
        connection = nil;
    }
    connection = [[NSURLConnection alloc] initWithRequest:requst delegate:self];
    [connection start];
}
#pragma mark connection代理

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    if(failBolck)
    {
        failBolck(@"获取失败");
    }
    else{
        if([self.apiDelegate respondsToSelector:@selector(ERApiDidFail:error:)])
        {
            [self.apiDelegate ERApiDidFail:self error:@"获取失败"];
        }
    }
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [receiveData setLength:0];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receiveData appendData:data];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSString *receiveStr = [[NSString alloc] initWithData:receiveData encoding:NSUTF8StringEncoding];
    id jsonValue = [receiveStr JSONValue];
    if(filishBolck)
    {
     
            filishBolck(jsonValue);
    }
    else{
        if([self.apiDelegate respondsToSelector:@selector(ERApiDidFinish:respone:)])
        {
            [self.apiDelegate ERApiDidFinish:self respone:jsonValue];
        }
    }
    [receiveStr release];
}
@end
