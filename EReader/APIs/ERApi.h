//
//  ERApi.h
//  ERReadder
//
//  Created by zym on 13-12-22.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol ERApiDelegate <NSObject>

//json
-(void)ERApiDidFinish:(id)api  respone:(id)jsonStr;
//获取信息失败
-(void)ERApiDidFail:(id)api  error:(NSString *)errorStr;

@end
#define kAppServerPath @"http://helfyreader.duapp.com/"
#define kPicStoreName @"helfyreader"
@interface ERApi : NSObject
{
    //如果设置有block。则使用block
    void (^failBolck)(id json);
    void (^filishBolck)(id json);
    
     
}
-(void)setfinishBlock:(void (^)(NSDictionary *jsob))filish failBlock:(void (^)(NSString *errorStr))faild;

@property(nonatomic,assign)id<ERApiDelegate>apiDelegate;
@property(nonatomic,retain)NSString *fileUrlStr;

-(void)getCommentForBookId:(NSString *)bookId
                     count:(int)count
                     maxId:(NSString *)maxId
                     mixId:(NSString *)minId;

-(void)getFavListForUserToken:(NSString *)userToken
                        count:(int)count
                        maxId:(NSString *)maxId
                        mixId:(NSString *)minId;

-(void)getMyPostForUserToken:(NSString *)userToken
                       count:(int)count
                       maxId:(NSString *)maxId
                       mixId:(NSString *)minId;

-(void)loginWithUserEmail:(NSString *)email userPassword:(NSString *)userPassword;

-(void)registerWithUserEmail:(NSString *)email userPassword:(NSString *)userPassword;

-(void)postCommentWithBookId:(NSString *)bookId
                   userToken:(NSString *)userToken
                     comment:(NSString *)comment;

-(void)addFavWithBookId:(NSString *)bookId
              userToken:(NSString *)userToken;

-(void)removeFavWithFavId:(NSString *)favId
                userToken:(NSString *)userToken;

-(void)postImageData:(NSData *)imageData isUserPic:(BOOL)isPic;

//取消正在获取数据的请求
-(void)cancel;
@end
