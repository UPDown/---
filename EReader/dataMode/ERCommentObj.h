//
//  ERCommentObj.h
//  ERReadder
//
//  Created by zym on 13-12-22.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ERUserObj.h"
@interface ERCommentObj : NSObject
@property (nonatomic,retain)ERUserObj *userInfo;
@property (nonatomic,copy)NSString *gameId;
@property (nonatomic,copy)NSString *commentId;
@property (nonatomic,copy)NSString *commentContent;
@property (nonatomic,copy)NSString *creatDate;

+(ERCommentObj *)objForDic:(NSDictionary *)dic;
@end
