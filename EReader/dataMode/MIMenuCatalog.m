//
//  MIMenuCatalog.m
//  GameRaiders
//
//  Created by zym on 13-10-11.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import "MIMenuCatalog.h"

@implementation MIMenuCatalog
@synthesize title,type,sraechText,categoryID;

-(void)dealloc
{
    self.sraechText =nil;
    self.title = nil;
    self.categoryID = nil;
    [super dealloc];
}
-(id)proxyForJson
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(self.title)
    {
        [dic setObject:self.title forKey:@"catename"];
    }
    if(self.categoryID)
    {
        [dic setObject:self.categoryID forKey:@"cateid"];
    }
  
    
    
    return dic;
}

@end
