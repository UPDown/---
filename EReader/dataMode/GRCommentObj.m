//
//  GRCommentObj.m
//  GameRaiders
//
//  Created by helfy  on 13-10-13.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import "GRCommentObj.h"

@implementation GRCommentObj

@synthesize gameId;
@synthesize commentId;
@synthesize commentContent;
@synthesize creatDate;
@synthesize userInfo;


+(GRCommentObj *)objForDic:(NSDictionary *)dic
{
    GRCommentObj *obj = [[GRCommentObj alloc] init];
    obj.gameId = [dic objectForKey:@"gameId"];
    obj.commentId = [dic objectForKey:@"commentId"];
    obj.commentContent = [dic objectForKey:@"content"];
    obj.creatDate = [dic objectForKey:@"creatDate"];

    obj.userInfo =[[[ERUserObj alloc] init] autorelease];
//    obj.userInfo.userId = [[dic objectForKey:@"userInfo"] objectForKey:@"userId"];
//    obj.userInfo.userName = [[dic objectForKey:@"userInfo"] objectForKey:@"userName"];
    [obj.userInfo setUserInfoWithDic:[dic objectForKey:@"userInfo"]];
    return [obj autorelease];
}

-(id)proxyForJson
{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:self.gameId forKey:@"gameId"];
    [dic setObject:self.commentId forKey:@"commentId"];
    [dic setObject:self.commentContent forKey:@"content"];
    [dic setObject:self.creatDate forKey:@"creatDate"];
    [dic setObject:[self.userInfo proxyForJson] forKey:@"userInfo"];
    return dic;
}
-(void)dealloc{
    self.userInfo = nil;
    self.gameId =nil;
    self.commentId =nil;
    self.commentContent =nil;
    self.creatDate =nil;
    [super dealloc];
}

@end
