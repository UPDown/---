//
//  ERBook.m
//  CoreText
//
//  Created by helfy on 14-3-3.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "ERBook.h"
#import <CoreText/CoreText.h>
@implementation ERBook

@synthesize parserFinish;
@synthesize pageRanges;
@synthesize bookObj;
@synthesize delegate;
-(id)init{
    self = [super init];
    if(self)
    {
        self.pageRanges  = [NSMutableArray array];
        self.parserFinish = NO;
    }
    return self;
}
-(void)dealloc
{
//    dispatch_source_cancel(source);
    if(queue)
    {
    dispatch_suspend(queue);
 
//    dispatch_release(source);
    
    
//    source =NULL;
    queue =NULL;
    }
    ObjRelease(bookString);
    self.pageRanges = nil;
    self.delegate = nil;
       ObjRelease(bookObj);
    [super dealloc];
}

-(void)getTextWithIndex:(int)index withfinishBlock:(void (^)(NSString *string))getblock
{
    if(index<pageRanges.count)
    {
        NSRange range = NSRangeFromString([pageRanges objectAtIndex:index]);
        NSString *string = [bookString substringWithRange:range];
        getblock(string);
    }
    else{
        
        //貌似也不会越界取。。。
        
        
//        //添加soure监控  貌似有点慢～ 可能是都使用了pageRanges  导致此线程卡住
//        if(source == NULL)
//        {
//            source = dispatch_source_create(DISPATCH_SOURCE_TYPE_DATA_ADD, 0, 0, dispatch_get_main_queue());
//             dispatch_retain(source);
//            
//            dispatch_source_set_event_handler(source, ^{
//                if(index<pageRanges.count)
//                {
//                    NSRange range = NSRangeFromString([pageRanges objectAtIndex:index]);
//                    NSString *string = [bookString substringWithRange:range];
//                    getblock(string);
//                    dispatch_suspend(source);
//                }
//            });
//        }
//      
//            dispatch_resume(source);
        
    }

}
-(void)saveCache
{
    if(self.pageRanges)
    {
    NSString *cachePath = [bookObj bookRangesCachePath];
    [self.pageRanges writeToFile:cachePath atomically:YES];
    }
}
-(NSArray *)readCache
{
     NSString *cachePath = [bookObj bookRangesCachePath];
    return [NSArray arrayWithContentsOfFile:cachePath];
}

-(void)parserCancel
{
    [self saveCache];
    cancel = YES;
}


-(void)parserBookWithBookObj:(EROfflineBookObj *)obj withBounds:(CGRect)bounds withfinishBlock:(void (^)(void))psrserFinishblock
{

    ObjRelease(bookObj);
    bookObj =[obj retain];
    self.parserFinish = NO;
    [self.pageRanges removeAllObjects];
    cancel = NO;
    
    NSString *filePath = [bookObj bookPath];
    
    bookString = [[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil] retain];
   if(bookString == nil)
   {
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"文件本损坏，请重新下载" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
       [alert show];
       [alert release];
       return;
   
   }
    NSArray *cacheArray = [self readCache];
    
    NSRange lastRange =NSMakeRange(0, 0);
    BOOL needParser = YES;
    
    
    if(cacheArray.count >0)
    {
        lastRange = NSRangeFromString([cacheArray lastObject]);
        if(lastRange.location +lastRange.length >= bookString.length)
        {
            needParser = NO;
        }
          [self.pageRanges addObjectsFromArray:cacheArray];
    }

    if(needParser)
    {
        queue = dispatch_queue_create("com.helfy.parserBookText", NULL);

        dispatch_async(queue, ^{
        
//            NSMutableAttributedString* attString = [[self class]creatAttributedStringWithText:bookString];
//            CFRange fitRange=CFRangeMake(0, 0);
//            int pageCount =0;
//            CFRange pageRange;
//            CTFramesetterRef framesetterRef = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attString);
//            
//            while (fitRange.location + fitRange.length < attString.string.length) {
//                
//                CTFramesetterSuggestFrameSizeWithConstraints(framesetterRef, CFRangeMake(fitRange.length,0), NULL, bounds.size, &pageRange);
//             
//                pageCount++;
//                
////                NSString *rangeStr =  NSStringFromRange(NSMakeRange(fitRange.length, pageRange.length));
////                  [pageRanges addObject:rangeStr];
//                fitRange.length = pageRange.length + fitRange.length;
//                       NSLog(@"%i",pageCount);
//              
//            }
    
        int pageMaxLength =1000;
        CFIndex loc =lastRange.location+lastRange.length;
        int textLength = bookString.length;
        CFRange range ;

        do {

            NSString *currentString =[bookString substringWithRange:NSMakeRange(loc, ((loc+pageMaxLength>=bookString.length)?(bookString.length-loc):(pageMaxLength)))];
            range =  [[self class] getShowTextRang:currentString withDrawRect:bounds];
            range.location = loc;
            loc = range.location + range.length;
            if (range.length >0) {
                NSString *rangeStr =  NSStringFromRange(NSMakeRange(range.location, range.length));
                [pageRanges addObject:rangeStr];
//                if(source){
//                  dispatch_source_merge_data(source, 1);
//                }
                if(self.delegate && [self.delegate respondsToSelector:@selector(ERBookPageParser:percentage:)])
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.delegate ERBookPageParser:pageRanges.count-1 percentage:(float)loc/textLength];
                    });
         
                }
     
            }
            else
            {
                break;
            }
             if(cancel)return;
        }
        while (range.location != NSNotFound && range.length >0 && cancel==NO);

            if(cancel)return;
            
            
        self.parserFinish = YES;
        dispatch_async(dispatch_get_main_queue(),psrserFinishblock);
        [self saveCache];
    });
    }
    else{
      
        if(self.delegate && [self.delegate respondsToSelector:@selector(ERBookPageParser:percentage:)])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate ERBookPageParser:pageRanges.count-1 percentage:1.0];
            });
            
        }
        self.parserFinish = YES;
        dispatch_async(dispatch_get_main_queue(),psrserFinishblock);
    }
}


+(CTFontRef)createCTFont:(UIFont *)uifont;
{
    CTFontRef font = CTFontCreateWithName((CFStringRef)uifont.fontName, uifont.pointSize, NULL);
    return font;
}

static     CGFloat  _lineSpacing =5.0;
+(NSMutableAttributedString*)creatAttributedStringWithText:(NSString *)text
{
    NSMutableAttributedString* attString = [[NSMutableAttributedString alloc] initWithString:text];
    NSRange range = NSMakeRange(0, [text length]);
    CTFontRef font = [self createCTFont:[UIFont systemFontOfSize:18]];
    [attString addAttribute:(NSString *)kCTFontAttributeName
                      value:(__bridge id)font
                      range:range];
    CFRelease(font);
    UIColor *color = [UIColor blackColor];
    int mode = [[NSUserDefaults standardUserDefaults] integerForKey:@"mode"];
    switch (mode) {
        case -1:
            color = [UIColor blackColor];
        break;
        case 0:
            color = [UIColor blackColor];
            break;
        case 1:
            color = [UIColor whiteColor];
              break;
        default:
            break;
        }
    [attString addAttribute:(NSString *)kCTForegroundColorAttributeName
                      value:(__bridge id)color.CGColor
                      range:range];
//    CTLineBreakMode             _lineBreakMode;
    CTTextAlignment             _textAlignment;
//    CGFloat                     _firstLineHeadIndent;
//    CGFloat                     _spacing;
//    CGFloat                     _topSpacing;
    
    
//    _lineBreakMode = kCTLineBreakByWordWrapping;
    _textAlignment = kCTJustifiedTextAlignment;
//    _firstLineHeadIndent = 0.0;
//    _spacing = 10.0;
//    _topSpacing = 4.0;
    
    
    CFIndex theNumberOfSettings = 2;
    CTParagraphStyleSetting theSettings[2] =
    {
        { kCTParagraphStyleSpecifierAlignment, sizeof(CTTextAlignment), &_textAlignment },
        ////        { kCTParagraphStyleSpecifierLineBreakMode, sizeof(CTLineBreakMode), &_lineBreakMode },
        ////        { kCTParagraphStyleSpecifierFirstLineHeadIndent, sizeof(CGFloat), &_firstLineHeadIndent },
        ////        { kCTParagraphStyleSpecifierParagraphSpacing, sizeof(CGFloat), &_spacing },
        ////        { kCTParagraphStyleSpecifierParagraphSpacingBefore, sizeof(CGFloat), &_topSpacing },
        { kCTParagraphStyleSpecifierLineSpacing, sizeof(CGFloat), &_lineSpacing }
    };
    
    CTParagraphStyleRef paragraphStyle = CTParagraphStyleCreate(theSettings, theNumberOfSettings);
    [attString addAttribute:(NSString *)kCTParagraphStyleAttributeName
                      value:(__bridge id)paragraphStyle
                      range:range];
    CFRelease(paragraphStyle);
    
    return [attString autorelease];
    
}
+(CFRange)getShowTextRang:(NSString *)text withDrawRect:(CGRect)rect{
    
    //要绘制的文本
    NSMutableAttributedString* attString = [self creatAttributedStringWithText:text];
    CTFramesetterRef framesetterRef = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attString);
//    
//    CGMutablePathRef path = CGPathCreateMutable(); //1
//    CGPathAddRect(path, NULL, rect );
//    CTFrameRef tempFrameRef = CTFramesetterCreateFrame(framesetterRef,
//                                                       CFRangeMake(0, [attString length]), path, NULL);
//
//    CFRelease(framesetterRef);
//    CGPathRelease(path);
//    CFRange frameRange=CTFrameGetVisibleStringRange(tempFrameRef); //5
//    CFRelease(tempFrameRef);
    
    
    CFRange pageRange;
    CTFramesetterSuggestFrameSizeWithConstraints(framesetterRef, CFRangeMake(0,0), NULL, rect.size, &pageRange);
    

    
    
    return pageRange;
    
  
}

@end
