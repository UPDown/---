//
//  NSString+conver.m
//  ERReadder
//
//  Created by zym on 13-12-22.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import "NSString+conver.h"
#import "NSDate+Helper.h"
@implementation NSString_conver

+(BOOL)checkobjIsgNULL:(id)obj
{
    return [obj isKindOfClass:[NSNull class]];
}

+(NSString *)NULLStrChangeToStr:(id)obj
{
    if([self checkobjIsgNULL:obj])
    {
        return nil;
    }
    if(![obj isKindOfClass:[NSString class]])
    {
        return [NSString stringWithFormat:@"%@",obj];
    }
    if(((NSString *)obj).length >0)return obj;
    return nil;
}

+(NSString *)dateWithString:(NSString *)string
{
    NSString *str=@"";
    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    
//    NSDate *date = [formatter dateFromString:string];
    
    //发布时间
    NSTimeInterval time = [string floatValue];
    //当前时间
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    //今天0点
    NSTimeInterval today = [[[NSDate date] beginningOfDay] timeIntervalSince1970];
    //昨天0点
    NSTimeInterval lastDay = today-(24*3600);
//    [formatter release];
    
    if (now - time < 60) {
        
        str = NSLocalizedString(@"刚刚", nil);
    }
    else if(now - time < 60*60)
    {
        str = [NSString stringWithFormat:NSLocalizedString(@"%d分钟前", nil),(int)(now - time)/60];
    }
    else if(time > today){
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:NSLocalizedString(@"今天 HH:mm", nil)];
        str = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:time]];
        [formatter release];
    }
    else if(time > lastDay){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:NSLocalizedString(@"昨天 HH:mm", nil)];
        str = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:time]];
        [formatter release];
        
    }
    else{
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:NSLocalizedString(@"yy-M-d", nil)];
        str = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:time]];
        [formatter release];
    }
    
    return str;
}
@end
