//
//  ERDataBase.m
//  EReader
//
//  Created by helfy  on 14-1-12.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import "ERDataBase.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMDatabasePool.h"
#import "FMDatabaseQueue.h"
#import "EROfflineBookObj.h"
#define KCreatBookTableSQL  @"CREATE TABLE IF NOT EXISTS bookRecord(BOOKINDEX INTEGER PRIMARY KEY AUTOINCREMENT,gid TEXT,TITLE TEXT,fav BOOLEAN, lastRedUrl TEXT,lastRedTitle TEXT,author TEXT)"

#define KQueryBookExist     @"select * from bookRecord where gid = '%@'"

#define kInsrtBookSQL  @"INSERT INTO bookRecord(gid, TITLE, fav, lastRedUrl,lastRedTitle, author) VALUES (?,?,?,?,?,?)"

#define KUpdataBookIfExistDataSQL   @"UPDATE bookRecord SET  gid = ?, TITLE = ?,fav = ?,lastRedUrl=?,lastRedTitle = ?,author = ? WHERE gid = ?"
#define KQueryAllFavBookDataSQL        @"select * from bookRecord where fav =1"



static ERDataBase *dbControll = nil;
@implementation ERDataBase
+ (ERDataBase *)shareDBControl
{
    @synchronized(self)
    {
        if (dbControll == nil) {
            dbControll = [[ERDataBase alloc]init];
        }
    }
    return dbControll;
}

+ (void)DBControlRelease
{
    ObjRelease(dbControll);
}

- (void)dealloc
{
    
    ObjRelease(dbQueue);
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        NSString *dbpath = [[[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/book.db"]] autorelease];
        dbQueue = [[FMDatabaseQueue databaseQueueWithPath:dbpath] retain];
        [self creatBookTable];
        [self creatOffLineBookTable];
    }
    return self;
}

-(FMDatabaseQueue *)dbQueue{
    return dbQueue;
}
- (void)creatBookTable
{
    [self.dbQueue inDatabase:^(FMDatabase *db){
        [db open];
        NSString *sql = KCreatBookTableSQL;
        [db executeUpdate:sql];
        [db close];
    }];
}
//查询gid是否存在
- (BOOL)queryBookExist:(NSString *)gid
{
    __block BOOL exist = NO;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSString *sql = [NSString stringWithFormat:KQueryBookExist,gid];
      

        FMResultSet *rs = [db executeQuery:sql];
        int counts = 0;
        while (rs.next) {
            counts ++;
        }
        [db close];
        if(counts == 0){
            exist = NO;
        }else{
            exist = YES;
        }
    }];
    return exist;
}
//查询是否收藏
- (BOOL)queryBookIsFav:(NSString *)gid
{
    __block BOOL exist = NO;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSString *sql = [NSString stringWithFormat:@"select * from bookRecord where gid='%@' and fav=1",gid];
        FMResultSet *rs = [db executeQuery:sql];
        int counts = 0;
        while (rs.next) {
            counts ++;
        }
        [db close];
        if(counts == 0){
            exist = NO;
        }
        else{
            exist = YES;
        }
    }];
    return exist;
}

- (void)insertBook:(NSDictionary *)dic
{
    if (![self queryBookExist:[dic objectForKey:@"gid"]]) {
        [self.dbQueue inDatabase:^(FMDatabase *db){
            [db open];
            
          [db executeUpdate:kInsrtBookSQL,[dic objectForKey:@"gid"],[dic objectForKey:@"title"],[dic objectForKey:@"fav"],[dic objectForKey:@"lastRedUrl"],[dic objectForKey:@"lastRedTitle"],[dic objectForKey:@"author"]];
            [db close];
        }];
    }
    else{
        
        [self updateBookWithDic:dic];
    }
}
- (void)updateBookWithDic:(NSDictionary *)dic
{
    [self.dbQueue inDatabase:^(FMDatabase *db){
        [db open];
        [db executeUpdate:KUpdataBookIfExistDataSQL,[dic objectForKey:@"gid"],[dic objectForKey:@"title"],[dic objectForKey:@"fav"],[dic objectForKey:@"lastRedUrl"],[dic objectForKey:@"lastRedTitle"],[dic objectForKey:@"author"],[dic objectForKey:@"gid"]];
        [db close];
    }];
}

-(NSDictionary *)queryBookforGid:(NSString *)gid
{
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [self.dbQueue inDatabase:^(FMDatabase *db){
        [db open];
        NSString *sql = @"select * from bookRecord where gid =?";
        
        
        FMResultSet *rs = [db executeQuery:sql,gid];
        while ([rs next]) {
 
            [dic setObject:[rs stringForColumn:@"gid"] forKey:@"gid"];
            [dic setObject:[rs stringForColumn:@"title"] forKey:@"title"];
            [dic setObject:[NSNumber numberWithInt:[rs intForColumn:@"fav"]] forKey:@"fav"];
            [dic setObject:[rs stringForColumn:@"lastRedUrl"] forKey:@"lastRedUrl"];
            [dic setObject:[rs stringForColumn:@"lastRedTitle"] forKey:@"lastRedTitle"];
            [dic setObject:[rs stringForColumn:@"author"] forKey:@"author"];
      
        }
        [db close];
    }];
    return dic;
}
-(NSArray *)queryFavBooks
{
    NSMutableArray *dataArray = [[[NSMutableArray alloc]init]autorelease];
    [self.dbQueue inDatabase:^(FMDatabase *db){
        [db open];
        NSString *sql = KQueryAllFavBookDataSQL;
        
      
        FMResultSet *rs = [db executeQuery:sql];
        while ([rs next]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            [dic setObject:[rs stringForColumn:@"gid"] forKey:@"gid"];
            [dic setObject:[rs stringForColumn:@"title"] forKey:@"title"];
            [dic setObject:[NSNumber numberWithInt:[rs intForColumn:@"fav"]] forKey:@"fav"];
            [dic setObject:[rs stringForColumn:@"lastRedUrl"] forKey:@"lastRedUrl"];
            [dic setObject:[rs stringForColumn:@"lastRedTitle"] forKey:@"lastRedTitle"];
            [dic setObject:[rs stringForColumn:@"author"] forKey:@"author"];
            [dataArray addObject:dic];
        }
        [db close];
    }];
    return dataArray;
}

//==============本地书管理=======

#define KCreatOffLineBookTableSQL  @"CREATE TABLE IF NOT EXISTS OfflineBook(BOOKINDEX INTEGER PRIMARY KEY AUTOINCREMENT,gid TEXT,TITLE TEXT,author TEXT,size TEXT,lastReadText TEXT,downloadstatus INTEGER)"
#define kInsrtOfflineBookSQL  @"INSERT INTO OfflineBook(gid, TITLE, author,size, lastReadText,downloadstatus) VALUES (?,?,?,?,?,?)"
#define KUpdataOfflineBookIfExistDataSQL   @"UPDATE OfflineBook SET  gid = ?, TITLE = ?,author = ?,size=?,lastReadText = ?,downloadstatus = ? WHERE gid = ?"
#define KQueryOfflineBookExist     @"select * from OfflineBook where gid = '%@'"
#define KQueryAllOfflineBookDataSQL        @"select * from OfflineBook where 1 order by BOOKINDEX desc"
#define KQuerysingleOfflineBookDataSQL        @"select * from OfflineBook where gid =?"
-(void)creatOffLineBookTable
{
    [self.dbQueue inDatabase:^(FMDatabase *db){
        [db open];
        NSString *sql = KCreatOffLineBookTableSQL;
        [db executeUpdate:sql];
        [db close];
    }];

}

-(BOOL)queryOfflineBookExist:(NSString *)gid
{
    __block BOOL exist = NO;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSString *sql = [NSString stringWithFormat:KQueryOfflineBookExist,gid];
        FMResultSet *rs = [db executeQuery:sql];
        int counts = 0;
        while (rs.next) {
            counts ++;
        }
        [db close];
        if(counts == 0){
            exist = NO;
        }else{
            exist = YES;
        }
    }];
    return exist;
}
- (void)insertOfflineBook:(EROfflineBookObj *)bookObj
{
        if([self queryOfflineBookExist:bookObj.gId])
        {
            [self updateOfflineBookWithDic:bookObj];
        }
        else{
        [self.dbQueue inDatabase:^(FMDatabase *db){
            [db open];
            
            [db executeUpdate:kInsrtOfflineBookSQL,bookObj.gId,bookObj.title,bookObj.author,bookObj.size,bookObj.lastReadText,[NSNumber numberWithInt:bookObj.downLoadStatus]];
            [db close];
        }];
        }
  
}
- (void)updateOfflineBookWithDic:(EROfflineBookObj *)bookObj
{
    [self.dbQueue inDatabase:^(FMDatabase *db){
        [db open];
        [db executeUpdate:KUpdataOfflineBookIfExistDataSQL,bookObj.gId,bookObj.title,bookObj.author,bookObj.size,bookObj.lastReadText,[NSNumber numberWithInt:bookObj.downLoadStatus],bookObj.gId];
        [db close];
    }];
}
- (void)deleteOfflineBook:(EROfflineBookObj *)bookObj
{
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSString *sql = [NSString stringWithFormat:@"delete from OfflineBook where gid='%@'",bookObj.gId];
        [db executeUpdate:sql];
        [db close];
    }];
}
-(EROfflineBookObj *)queryOfflineBookWithGid:(NSString *)gid
{
    EROfflineBookObj *bookObj = [[EROfflineBookObj alloc] init];
    [self.dbQueue inDatabase:^(FMDatabase *db){
        [db open];
        NSString *sql = KQuerysingleOfflineBookDataSQL;
        
        
        FMResultSet *rs = [db executeQuery:sql,gid];
        while ([rs next]) {
  
            bookObj.gId= [rs stringForColumn:@"gid"];
            bookObj.title= [rs stringForColumn:@"title"] ;
            bookObj.size =[rs stringForColumn:@"size"];
            bookObj.downLoadStatus=[rs intForColumn:@"downloadstatus"];
          
            bookObj.lastReadText= [rs stringForColumn:@"lastReadText"];
            bookObj.author= [rs stringForColumn:@"author"];
        }
        [db close];
    }];
    return [bookObj autorelease];
}
-(NSArray *)queryOfflineBooks
{
    NSMutableArray *dataArray = [[[NSMutableArray alloc]init]autorelease];
    [self.dbQueue inDatabase:^(FMDatabase *db){
        [db open];
        NSString *sql = KQueryAllOfflineBookDataSQL;
        
        
        FMResultSet *rs = [db executeQuery:sql];
        while ([rs next]) {
            EROfflineBookObj *bookObj = [[EROfflineBookObj alloc] init];
            bookObj.gId= [rs stringForColumn:@"gid"];
            bookObj.title= [rs stringForColumn:@"title"] ;
            bookObj.size =[rs stringForColumn:@"size"];
            bookObj.downLoadStatus=[rs intForColumn:@"downloadstatus"];
            bookObj.lastReadText= [rs stringForColumn:@"lastReadText"];
            bookObj.author= [rs stringForColumn:@"author"];
            [dataArray addObject:bookObj];
            [bookObj release];
        }
        [db close];
    }];
    return dataArray;
}



@end
