//
//  EROfflineDownManger.h
//  EReader
//
//  Created by zym on 14-1-17.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "EROfflineBookObj.h"
@interface EROfflineDownManger : NSObject
{
    ASINetworkQueue *downQueue;
    NSMutableArray *downBookArray;
    EROfflineDownManger *currentDownBook;
}
+(EROfflineDownManger *)shareManger;
+(void)mangerReleass;
-(void)addDownOperator:(EROfflineBookObj *)book;
-(void)pauseDownOperator:(EROfflineBookObj *)book;

-(void)cancelDownOperator:(EROfflineBookObj *)book;

-(BOOL)queueContainsBook:(EROfflineBookObj *)book;

- (void)cancelAllOperator;

-(NSArray *)mergerArray:(NSArray *)objArray;
-(EROfflineBookObj *)getBookObjWithGid:(NSString *)gid;
+(void)creatFolder:(NSString *)fileName;
@end
