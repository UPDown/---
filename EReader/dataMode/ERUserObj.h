//
//  ERUserObj.h
//  ERReadder
//
//  Created by zym on 13-12-22.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kUserLoginFinish @"userLoginFinish"
#define kUserLogOut @"userLogOut"
#define kUserEmail @"userEmail"
#define kUserInfoJsonStr @"userInfoJson"
@interface ERUserObj : NSObject
@property(nonatomic,copy)NSString *userId;
@property(nonatomic,copy)NSString *userName;
@property(nonatomic,copy)NSString *email;
@property(nonatomic,copy)NSString *userPic;
@property(nonatomic,assign)int userSex;  //0 未设置  1 男 ，2 女
@property(nonatomic,assign)int isAdmin;
-(id)proxyForJson;
-(void)setUserInfoWithDic:(NSDictionary *)dic;
@end


@interface ERCurrentUserObj : ERUserObj
@property(nonatomic,copy)NSString *userToken;
@property(nonatomic,copy)NSString *password;
@property(nonatomic,assign)BOOL userLogined;
+(ERCurrentUserObj *)currentUserInfo;
-(void)userLogFinish;
-(void)userLogOut;
-(void)userAutoLogin;
@end