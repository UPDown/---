//
//  ERMeunCellObj.m
//  ERReadder
//
//  Created by zym on 13-12-22.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import "ERMeunCellObj.h"

@implementation ERMeunCellObj
@synthesize title;
@synthesize action;
@synthesize level;
@synthesize image;
@synthesize isCatalog;
-(void)dealloc{
    self.title = nil;
    self.action = nil;
    [super dealloc];
}
+(ERMeunCellObj *)cellObjWith:(NSString *)title level:(int)level action:(SEL)action
{

    ERMeunCellObj *obj = [[[ERMeunCellObj alloc] init] autorelease];
    obj.title = title;
    obj.level = level;
    obj.action = action;
    
    return obj;
    
}
@end
