//
//  EROfflineBookObj.h
//  EReader
//
//  Created by zym on 14-1-17.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol EROfflineBookObjDelegate <NSObject>
@optional

-(void)EROfflineBookPauseDown;      //暂停下载
-(void)EROfflineBookDownStartDown;  // 开始下载
-(void)EROfflineBookDownFailed;   // 下载失败
-(void)EROfflineBookDownFinish;   // 下载失败
-(void)EROfflineBookCancelDown;  // 取消下载
-(void)EROfflineBookDowning;      //下载中
-(void)EROfflineBookDownProgress:(float)progressValue;
-(void)EROfflineBookWillDelete;  //书将要被删除
-(void)EROfflineBookDidDelete;  //书被删除
@end
typedef enum
{
    EROfflineBookDownLoadStatusIsUnDown = 0,  //未下载/已删除　需要下载
    EROfflineBookLoadStatusIsWait =1,
    EROfflineBookLoadStatusIsDowning = 2, //　正在下载中
    
    EROfflineBookDownLoadStatusIsPaused = 3, //　暂停中
    EROfflineBookDownLoadStatusIsDowned = 4, //　　下载完成
}ERBookDownLoadStatus;

@interface EROfflineBookObj : NSObject
{
    NSMutableArray *targets;
}
@property (nonatomic,assign) NSInteger bookIndex;
@property (nonatomic,copy) NSString *gId;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *author;
@property (nonatomic,copy) NSString *size;
@property (nonatomic,copy) NSString *lastReadText;

//@property (nonatomic,copy) NSString *bookPath;
@property (nonatomic,assign) ERBookDownLoadStatus downLoadStatus;
@property (nonatomic,assign) float downProgress;
-(void)startDownBook;
-(void)paushDown;
-(void)insertLocBook;
-(void)deleteBook;
-(void)setProgress:(float)progress;
-(float)progress;
-(void)addTarget:(id<EROfflineBookObjDelegate>)target;
-(void)removeTarget:(id<EROfflineBookObjDelegate>)target;

-(NSString *)downURl;
-(NSString *)bookPath;
-(NSString *)bookCatalogPath;
-(NSString *)bookRangesCachePath;
-(NSString *)bookReadRecordPath;
//
-(void)bookDownFailed;
-(void)bookDownFinished;
-(void)bookDowning;
@end
