//
//  ERDataBase.h
//  EReader
//
//  Created by helfy  on 14-1-12.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabaseQueue.h"
@class EROfflineBookObj;
@interface ERDataBase : NSObject
{
     FMDatabaseQueue *dbQueue;
}
+ (ERDataBase *)shareDBControl;
+ (void)DBControlRelease;
//建表
- (void)creatBookTable;
- (void)insertBook:(NSDictionary *)dic;
-(NSArray *)queryFavBooks;
- (void)updateBookWithDic:(NSDictionary *)gid;
- (BOOL)queryBookIsFav:(NSString *)gid;
-(NSDictionary *)queryBookforGid:(NSString *)gid;
- (BOOL)queryBookExist:(NSString *)gid;


- (BOOL)queryOfflineBookExist:(NSString *)gid;
-(EROfflineBookObj *)queryOfflineBookWithGid:(NSString *)gid;
- (void)deleteOfflineBook:(EROfflineBookObj *)bookObj;
- (void)insertOfflineBook:(EROfflineBookObj *)bookObj;
- (void)updateOfflineBookWithDic:(EROfflineBookObj *)bookObj;

-(NSArray *)queryOfflineBooks;
@end
