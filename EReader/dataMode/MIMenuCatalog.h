//
//  MIMenuCatalog.h
//  GameRaiders
//
//  Created by zym on 13-10-11.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    MIMenuCatalogHome=0,        //推荐
    MIMenuCatalogSearch=1,
    MIMenuCatalogMyPost,
    MIMenuCatalogMyFav,
    MIMenuCatalogCatalogList,   //分类
    MIMenuCatalogRank,      //排行
}CatalogType;

@interface MIMenuCatalog : NSObject
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy) NSString *sraechText;
@property(nonatomic,assign) CatalogType type;
@property (nonatomic,copy) NSString *categoryID;

-(id)proxyForJson;
@end
