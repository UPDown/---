//
//  ERBook.h
//  CoreText
//
//  Created by helfy on 14-3-3.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EROfflineBookObj.h"

@protocol ERBookDelegate <NSObject>

-(void)ERBookPageParser:(int)currentIndex percentage:(float) percentage;

@end

@interface ERBook : NSObject
{
    NSString *bookString;
    EROfflineBookObj *bookObj;
    
    dispatch_queue_t queue;
    dispatch_source_t source;
    
    BOOL cancel;  //因为queue 是block粒度的。在解析的时候是一个block。所以只有在循环的时候断掉
}
@property (nonatomic ,assign) id<ERBookDelegate>delegate;
@property (nonatomic,assign)BOOL parserFinish;    //解析完成标准
@property (nonatomic,strong)NSMutableArray *pageRanges;   //每个page对于的range
@property (nonatomic,readonly) EROfflineBookObj *bookObj;
//获取某个页面字符串。。可能会出现翻动到的页面还没解析过去，需等待。所以用block返回
-(void)getTextWithIndex:(int)index withfinishBlock:(void (^)(NSString *string))getblock;

//解析整本书，完成后调用block
-(void)parserBookWithBookObj:(EROfflineBookObj *)obj withBounds:(CGRect)bounds withfinishBlock:(void (^)(void))psrserFinishblock;

+(NSMutableAttributedString*)creatAttributedStringWithText:(NSString *)text;

-(void)parserCancel;
@end
