
#import "FileManager.h"


#define KDOCUMENT_NAME @"Library/Caches/CacheLists"
#define FAVORITE_NAME   @"Documents/Faverites.plist"
#define CHASEBOOK_NAME  @"Documents/ChaseBook.plist"


@implementation FileManager
+(void)saveJsonStr:(NSString *)jsonStr name:(NSString *)nameStr
{
    if (nameStr) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *documentPath = [[[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:KDOCUMENT_NAME]] autorelease];
        
        BOOL isDir = FALSE;
        BOOL isDirExist = [fileManager fileExistsAtPath:documentPath isDirectory:&isDir];
        
        if(!(isDirExist && isDir))
        {
            BOOL bCreateDir = [fileManager createDirectoryAtPath:documentPath withIntermediateDirectories:YES attributes:nil error:nil];
            if(!bCreateDir){
                NSLog(@"Create Audio Directory Failed.");
            }
        }
        NSString *filePath = [documentPath stringByAppendingPathComponent:nameStr];
        
        if(![fileManager fileExistsAtPath:filePath])
        {
            [fileManager createFileAtPath:filePath contents:NULL attributes:NULL];
        }
        [jsonStr writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
}

+(NSString *)jsonStrWithName:(NSString *)nameStr
{
    NSString *str = nil;
    if (nameStr) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *documentPath = [[[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:KDOCUMENT_NAME]] autorelease];
        
        NSString *filePath = [documentPath stringByAppendingPathComponent:nameStr];
        
        if(![fileManager fileExistsAtPath:filePath])
        {
            [fileManager createFileAtPath:filePath contents:NULL attributes:NULL];
        }
        str = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
    }
    return str;
}

+ (BOOL)writeImage:(UIImage*)image toFileAtPath:(NSString*)aPath
{
    if ((image == nil) || (aPath == nil) || ([aPath isEqualToString:@""]))
        return NO;
    
    NSFileManager *fileM = [NSFileManager defaultManager];
    NSString *fileDirectory = [aPath stringByDeletingLastPathComponent];
    
    if (![fileM fileExistsAtPath:fileDirectory]) {
        if ([fileM createDirectoryAtPath:fileDirectory withIntermediateDirectories:YES attributes:nil error:nil]==NO) {
            NSLog(@"文件夹创建失败");
        }
    }
    
    @try
    {
        NSData *imageData = nil;
        NSString *ext = [aPath pathExtension];
        if ([ext isEqualToString:@"png"])
            imageData = UIImagePNGRepresentation(image);
        else
        {
            // the rest, we write to jpeg
            // 0. best, 1. lost. about compress.
            imageData = UIImageJPEGRepresentation(image, 0);
        }
        if ((imageData == nil) || ([imageData length] <= 0))
            
            return NO;
        [imageData writeToFile:aPath atomically:YES];
        return YES;
    }
    @catch (NSException *e)
    
    {
        NSLog(@"create thumbnail exception.");
    }
    return NO;
}


+(void)writeFavoritesFile:(NSMutableDictionary*)_object
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:FAVORITE_NAME]];
    
    if(![fileManager fileExistsAtPath:filePath])
    {
        [fileManager createFileAtPath:filePath contents:NULL attributes:NULL];
    }

    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[FileManager readFavoritesFile]];
    if (!array) {
        array = [[NSMutableArray alloc] init];
    }
//    if (![array containsObject:_object]) {
        [array insertObject:_object atIndex:0];
        [array writeToFile:filePath atomically:YES];
//    }
    [array release];


    [filePath release];
}

+ (NSMutableArray *)readFavoritesFile
{

    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [[[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:FAVORITE_NAME]] autorelease];
    
    if(![fileManager fileExistsAtPath:filePath])
    {
        [fileManager createFileAtPath:filePath contents:NULL attributes:NULL];
        return nil;
    }
    
    NSMutableArray *resultArray = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    return [resultArray autorelease];
}

+ (void)deleteFavorite:(NSMutableDictionary*)_object
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:FAVORITE_NAME]];
    
    if(![fileManager fileExistsAtPath:filePath])
    {
        [fileManager createFileAtPath:filePath contents:NULL attributes:NULL];
    }
    else{
        NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[FileManager readFavoritesFile]];
        if (!array) {
            array = [[NSMutableArray alloc] init];
        }
        if ([array containsObject:_object]) {
            [array removeObject:_object];
            [array writeToFile:filePath atomically:YES];
        }
        [array release];
    }
    
    [filePath release];
}

+ (void)deleteFavoriteAtIndex:(int)_index
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:FAVORITE_NAME]];
    
    if(![fileManager fileExistsAtPath:filePath])
    {
        [fileManager createFileAtPath:filePath contents:NULL attributes:NULL];
    }
    else{
        NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[FileManager readFavoritesFile]];
        if (!array) {
            array = [[NSMutableArray alloc] init];
        }
        if (_index<array.count) {
            [array removeObjectAtIndex:_index];
            [array writeToFile:filePath atomically:YES];
        }
        [array release];
    }
    
    [filePath release];
}

#pragma mark -追书
+(void)writeChaseBookFile:(NSMutableDictionary*)_object
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:CHASEBOOK_NAME]];
    
    if(![fileManager fileExistsAtPath:filePath])
    {
        [fileManager createFileAtPath:filePath contents:NULL attributes:NULL];
    }

    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[FileManager readChaseBookFile]];
    if (!array) {
        array = [[NSMutableArray alloc] init];
    }

    for (NSDictionary *dic in array) {
        if ([[dic objectForKey:@"bookName"] isEqualToString:[_object objectForKey:@"bookName"]]) {
            [array removeObject:dic];
            break;
        }
    }

    [array insertObject:_object atIndex:0];
    [array writeToFile:filePath atomically:YES];

    [array release];

    
    [filePath release];
}

+ (NSMutableArray *)readChaseBookFile
{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [[[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:CHASEBOOK_NAME]] autorelease];
    
    if(![fileManager fileExistsAtPath:filePath])
    {
        [fileManager createFileAtPath:filePath contents:NULL attributes:NULL];
        return nil;
    }
    
    NSMutableArray *resultArray = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    return [resultArray autorelease];
}

+ (void)deleteChaseBook:(NSMutableDictionary*)_object
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:CHASEBOOK_NAME]];
    
    if(![fileManager fileExistsAtPath:filePath])
    {
        [fileManager createFileAtPath:filePath contents:NULL attributes:NULL];
    }
    else{
        NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[FileManager readChaseBookFile]];
        if (!array) {
            array = [[NSMutableArray alloc] init];
        }
        
        for (NSDictionary *dic in array) {
            if ([[dic objectForKey:@"bookName"] isEqualToString:[_object objectForKey:@"bookName"]]) {
                [array removeObject:dic];
                [array writeToFile:filePath atomically:YES];
                break;
            }
        }
        
        [array release];
    }
    
    [filePath release];
}

@end
