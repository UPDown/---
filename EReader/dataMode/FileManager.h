//  Created by helfy  on 12-8-14.
//
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject
+(void)saveJsonStr:(NSString *)jsonStr name:(NSString *)nameStr;
+(NSString *)jsonStrWithName:(NSString *)nameStr;
+ (BOOL)writeImage:(UIImage*)image toFileAtPath:(NSString*)aPath;

//收藏
+(void)writeFavoritesFile:(NSMutableDictionary*)_object;
+ (NSMutableArray *)readFavoritesFile;
+ (void)deleteFavorite:(NSMutableDictionary*)_object;
+ (void)deleteFavoriteAtIndex:(int)_index;

//追书
+(void)writeChaseBookFile:(NSMutableDictionary*)_object;
+ (NSMutableArray *)readChaseBookFile;
+ (void)deleteChaseBook:(NSMutableDictionary*)_object;
@end
