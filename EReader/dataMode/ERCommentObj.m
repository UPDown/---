//
//  ERCommentObj.m
//  ERReadder
//
//  Created by zym on 13-12-22.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import "ERCommentObj.h"

@implementation ERCommentObj

@synthesize gameId;
@synthesize commentId;
@synthesize commentContent;
@synthesize creatDate;
@synthesize userInfo;


+(ERCommentObj *)objForDic:(NSDictionary *)dic
{
    ERCommentObj *obj = [[ERCommentObj alloc] init];
    obj.gameId = [dic objectForKey:@"gameId"];
    obj.commentId = [dic objectForKey:@"commentId"];
    obj.commentContent = [dic objectForKey:@"content"];
    obj.creatDate = [dic objectForKey:@"creatDate"];

    
    obj.userInfo =[[[ERUserObj alloc] init] autorelease];
 
    [obj setUserInfo:[dic objectForKey:@"userInfo"] ];
    return [obj autorelease];
}

-(id)proxyForJson
{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:self.gameId forKey:@"gameId"];
    [dic setObject:self.commentId forKey:@"commentId"];
    [dic setObject:self.commentContent forKey:@"content"];
    [dic setObject:self.creatDate forKey:@"creatDate"];
    [dic setObject:[self.userInfo proxyForJson] forKey:@"userInfo"];
    return dic;
}
-(void)dealloc{
    self.userInfo = nil;
    self.gameId =nil;
    self.commentId =nil;
    self.commentContent =nil;
    self.creatDate =nil;
    [super dealloc];
}

@end
