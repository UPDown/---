//
//  EROfflineDownManger.m
//  EReader
//
//  Created by zym on 14-1-17.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import "EROfflineDownManger.h"
#import "ASIHTTPRequest.h"
#import "SBJson.h"
static EROfflineDownManger *manger = nil;
@implementation EROfflineDownManger

+(EROfflineDownManger *)shareManger
{
    if(manger==nil)
    {
        manger = [[EROfflineDownManger alloc] init];
    }
    
    return manger;
}
+(void)mangerReleass
{
    ObjRelease(manger);
}
-(id)init
{
    self = [super init];
    if(self)
    {
        downQueue = [[ASINetworkQueue alloc] init];
        downQueue.maxConcurrentOperationCount = 1;
//        [downQueue setSuspended:NO];
        [downQueue setShouldCancelAllRequestsOnFailure:NO];
//        [downQueue setDownloadProgressDelegate:nil];
        [downQueue setShowAccurateProgress:YES];
        [downQueue go];
        downBookArray = [[NSMutableArray alloc] init];
    
    }
    return self;
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    ObjRelease(downQueue);
    ObjRelease(downBookArray);
    [downQueue cancelAllOperations];
     ObjRelease(downQueue);
    [super dealloc];
}

+(void)creatFolder:(NSString *)fileName
{
	NSString *folderName = [fileName stringByDeletingLastPathComponent];
	if(![[NSFileManager defaultManager] fileExistsAtPath:folderName])
	{
		[[NSFileManager defaultManager] createDirectoryAtPath:folderName withIntermediateDirectories:YES attributes:nil error:nil];
	}
}


-(void)addDownOperator:(EROfflineBookObj *)book
{
    NSString *fileDownLoadDestinaPaht = [book bookPath];
    if([[NSFileManager defaultManager] fileExistsAtPath:fileDownLoadDestinaPaht])
    {  //后台已经下该文件
        [book setProgress:1.0f];
        [book bookDownFinished];
  
        return;
    }
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    [downBookArray addObject:book];
    NSString *filePath = [book downURl]; //下载链接
    [[self class] creatFolder:fileDownLoadDestinaPaht];
    
    ASIHTTPRequest* requst = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:filePath]];
    [requst setDownloadProgressDelegate:book];
    [requst setShowAccurateProgress:YES];

    [requst setDownloadDestinationPath:fileDownLoadDestinaPaht];
    [requst setTemporaryFileDownloadPath:[fileDownLoadDestinaPaht stringByAppendingString:@".downLoad"]];
    [requst setAllowResumeForFileDownloads:YES];
    [requst setDidFinishSelector:@selector(downloadFinished:)];
    [requst setDidStartSelector:@selector(downloadStart:)];
    [requst setDidFailSelector:@selector(downloadFailed:)];
    requst.userInfo = [NSDictionary dictionaryWithObject:book forKey:@"target"];
    requst.username = book.gId;
    requst.delegate = self;
//    [requst setContentLength:[book.size longLongValue]];
//    [requst addRequestHeader:@"Content-Length" value:book.size];
    [requst addRequestHeader:@"Range" value:@"bytes=0-"];
    [requst setShouldContinueWhenAppEntersBackground:YES];
    [downQueue addOperation:requst];
  

}
-(void)pauseDownOperator:(EROfflineBookObj *)book{
    NSArray *array = [downQueue operations];
    for (ASIHTTPRequest* requst in array) {
        if([requst.username isEqualToString:book.gId])
        {
            //            [requst cancel];//使request触发代理方法requestFailed:
            [requst clearDelegatesAndCancel];
            requst = nil;
            [downBookArray removeObject:book];
        }
    }
    if(downBookArray.count == 0)
    {
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    }
}

-(void)cancelDownOperator:(EROfflineBookObj *)book{
    NSArray *array = [downQueue operations];
    for (ASIHTTPRequest* requst in array) {
        if([requst.username isEqualToString:book.gId])
        {
            [requst clearDelegatesAndCancel];
            requst = nil;
            [downBookArray removeObject:book];
        }
    }
    if(downBookArray.count == 0)
    {
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    }
}

-(BOOL)queueContainsBook:(EROfflineBookObj *)book{
    return [downBookArray containsObject:book];
}

- (void)cancelAllOperator{
    NSArray *operationsArray = [downQueue operations];
    for (ASIHTTPRequest* requst in operationsArray) {
        [requst clearDelegatesAndCancel];
        requst = nil;
    }
    [downBookArray removeAllObjects];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
  
}

-(void)downloadStart:(ASIHTTPRequest *)requst
{
    //TODO
    EROfflineBookObj *bookElement = ( EROfflineBookObj *)[[requst userInfo] objectForKey:@"target"];
    [bookElement bookDowning];
}

-(void)downloadFinished:(ASIHTTPRequest *)requst
{
//    NSString *downFile = [requst downloadDestinationPath];
    //    NSString *bookId = requst.username;
    //requst.userInfo;//[NSDictionary dictionaryWithObject:book forKey:@"target"];
    
    EROfflineBookObj *bookElement = ( EROfflineBookObj *)[[requst userInfo] objectForKey:@"target"];
   
    NSString *string = [NSString stringWithContentsOfFile:[bookElement bookPath] encoding:NSUTF8StringEncoding error:nil];
    
    //TODO  判断文件大小。如果差距太大，则为错误文件
    //一般来说，下载的书字数不应该少于5000字
    if(string.length >5000)
    {
      

        //异步处理目录
        dispatch_queue_t queue = dispatch_queue_create("com.helfy.parserCatlog", NULL);
        dispatch_async(queue, ^{
            //读取目录
            NSDate *date = [NSDate date];
            NSString *catalogStr = [NSString stringWithContentsOfFile:[bookElement bookCatalogPath] encoding:NSUTF8StringEncoding error:nil];
        
            NSMutableArray *removeArray = [NSMutableArray array];
           NSMutableArray* listArray = [NSMutableArray arrayWithArray:[catalogStr JSONValue]];
          if(listArray.count)
          {
              
              //全部判断太慢了 判断前10个目录。一般来说，前面会有契子会对不上，如果有一个能对上，判定为正常
              //一章算5000个字
              NSString *subString = [string substringToIndex:50000>string.length?string.length:50000];
              
              
              BOOL removeCatalog = YES;
                  for (int i =0; i< listArray.count ; i++) {
                      if(i>10)break;
                      NSDictionary *currentDic = [listArray objectAtIndex:i];
                      NSString *catalogName = [currentDic objectForKey:@"text"];
//                      //首先遍历 看是否存在该目录   。。暂时忽略效率 用range。后改正则
                      ////                      NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF CONTAINS '%@'", catalogName];
                      
                      NSRange range = [subString rangeOfString:catalogName];
                      if( range.location!=NSNotFound)
                      {
                          removeCatalog = NO;
                      }
                      else{
                          //去掉这项
                          [removeArray addObject:currentDic];
                      }
                  }

              if(removeCatalog == NO)
              {
                  [listArray removeObjectsInArray:removeArray];
                  [[listArray JSONRepresentation] writeToFile:[bookElement bookCatalogPath] atomically:YES encoding:NSUTF8StringEncoding error:nil];
              }
              else{
                  //移除目录
                   [[NSFileManager defaultManager] removeItemAtPath:[bookElement bookCatalogPath] error:nil];
              }
          }

            dispatch_async(dispatch_get_main_queue(), ^{
                     [bookElement bookDownFinished];
            });
         
        });

    }
    else{
        [bookElement bookDownFailed];
    }
    
    [downBookArray removeObject:bookElement];
    if(downBookArray.count == 0)
    {
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    }


    
}

-(void)downloadFailed:(ASIHTTPRequest *)requst
{
    
    //    NSString *bookId = requst.username;
    EROfflineBookObj *bookElement = ( EROfflineBookObj *)[[requst userInfo] objectForKey:@"target"];
    //TODO 定义新类
    [bookElement bookDownFailed];
    [downBookArray removeObject:bookElement];
    if(downBookArray.count == 0)
    {
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    }
}

//防止多个obj被初始化，做一个相同gid的合并
-(NSArray *)mergerArray:(NSArray *)objArray
{
    NSMutableArray*newArray = [NSMutableArray arrayWithArray:objArray] ;
    for (EROfflineBookObj *obj in downBookArray) {
        for (EROfflineBookObj *otherObj in objArray) {
            if([obj.gId isEqualToString:otherObj.gId])
            {
                [newArray removeObject:otherObj];
                [newArray addObject:obj];
                break;
            }
        }
    }

    return newArray;
    
}
-(EROfflineBookObj *)getBookObjWithGid:(NSString *)gid
{
    for (EROfflineBookObj *obj in downBookArray) {

            if([obj.gId isEqualToString:gid])
            {
                return obj;
            }
        
    }
    return nil;
}

@end
