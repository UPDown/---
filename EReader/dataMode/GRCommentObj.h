//
//  GRCommentObj.h
//  GameRaiders
//
//  Created by helfy  on 13-10-13.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ERUserObj.h"
@interface GRCommentObj : NSObject
@property (nonatomic,retain)ERUserObj *userInfo;
@property (nonatomic,copy)NSString *gameId;
@property (nonatomic,copy)NSString *commentId;
@property (nonatomic,copy)NSString *commentContent;
@property (nonatomic,copy)NSString *creatDate;

+(GRCommentObj *)objForDic:(NSDictionary *)dic;
@end
