//
//  EROfflineBookObj.m
//  EReader
//
//  Created by zym on 14-1-17.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import "EROfflineBookObj.h"
#import "EROfflineDownManger.h"
#import "ERDataBase.h"
@implementation EROfflineBookObj
@synthesize  gId;
@synthesize title;
@synthesize author;
@synthesize size;
@synthesize lastReadText;
//@synthesize bookPath;
@synthesize downProgress;
@synthesize downLoadStatus = _downLoadStatus;
-(id)init
{
    self = [super init];
    if(self)
    {
        targets = [[NSMutableArray alloc] init];
 
    }
    return self;
    
}
-(void)dealloc
{
    [targets removeAllObjects];
    ObjRelease(targets);
    self.gId = nil;
    self.title = nil;
    self.author = nil;
    self.size = nil;
    self.lastReadText = nil;

    [super dealloc];
}
-(void)addTarget:(id<EROfflineBookObjDelegate>)target
{
    if([targets containsObject:target] == NO)
    {
        [targets addObject:target];
    }
}
-(void)removeTarget:(id<EROfflineBookObjDelegate>)target
{
    [targets removeObject:target];
}


-(NSString *)downURl
{
    
    return [@"http://npacking.baidu.com/novel/packing?gid=" stringByAppendingString:[NSString stringWithFormat:@"%@&filetype=txt",self.gId]];
}
-(NSString *)bookPath
{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/book/%@/%@.txt",self.gId,self.title]];
    return path;

}
-(NSString *)bookCatalogPath
{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/book/%@/%@.catalog",self.gId,self.gId]];
    return path;
    
}

-(NSString *)bookRangesCachePath
{
    
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/book/%@/RangesCachePath_%@.plist",self.gId,self.gId]];
    return path;
    
}
-(NSString *)bookReadRecordPath
{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/book/%@/readRecord_%@.plist",self.gId,self.gId]];
    return path;
}

//读取已下载文件大小
-(float)readFileLenth
{
    NSString *tempFilePath = [[self bookPath]  stringByAppendingString:@".downLoad"];
    
    self.downProgress = 0.0f;
    if([[NSFileManager defaultManager] fileExistsAtPath:tempFilePath])
    {
        NSDictionary *info = [[NSFileManager defaultManager] attributesOfItemAtPath:tempFilePath error:nil];
        double fileSize = [[info objectForKey:NSFileSize] doubleValue];
        
        if ([self.size doubleValue]>0) {
            self.downProgress = (double)fileSize/ [self.size doubleValue];
        }
    }
    return self.downProgress;
}
-(void)setDownLoadStatus:(ERBookDownLoadStatus)status
{
    _downLoadStatus= status;
    
    switch (_downLoadStatus) {
        case EROfflineBookDownLoadStatusIsUnDown:
            self.downProgress=0.0f;
            break;
        case EROfflineBookLoadStatusIsDowning:
        {
            if([[EROfflineDownManger shareManger] queueContainsBook:self])
            {
                _downLoadStatus = EROfflineBookLoadStatusIsDowning;
            }
            else{
                _downLoadStatus = EROfflineBookDownLoadStatusIsPaused;
//                [self readFileLenth];
            }
        }
//            break;
        case EROfflineBookDownLoadStatusIsPaused:
            [self readFileLenth];
            break;
        case EROfflineBookDownLoadStatusIsDowned:
            self.downProgress=1.0f;
            
            break;
        default:
            break;
    }
}

//set downProgress
-(void)setProgress:(float)progress
{
    self.downProgress = progress;
    
    for (id<EROfflineBookObjDelegate> delegate in targets) {
        if([delegate respondsToSelector:@selector(EROfflineBookDownProgress:)])
        {
            [delegate EROfflineBookDownProgress:progress];
        }
    }
    
}
-(float)progress
{
    return self.downProgress;
}

-(void)insertLocBook
{
    self.downLoadStatus = EROfflineBookDownLoadStatusIsDowned;
    [[ERDataBase shareDBControl] insertOfflineBook:self];
    
}
-(void)startDownBook
{
    if([[EROfflineDownManger shareManger] queueContainsBook:self])return;
    
    self.downLoadStatus = EROfflineBookLoadStatusIsDowning;
    [[ERDataBase shareDBControl] insertOfflineBook:self];
    
    self.downLoadStatus = EROfflineBookLoadStatusIsWait;
    [[EROfflineDownManger shareManger] addDownOperator:self];
    
    for (id<EROfflineBookObjDelegate> delegate in targets) {
        if([delegate respondsToSelector:@selector(EROfflineBookDownStartDown)])
        {
            [delegate EROfflineBookDownStartDown];
        }
    }
}
-(void)paushDown
{
    self.downLoadStatus = EROfflineBookDownLoadStatusIsPaused;
    [[EROfflineDownManger shareManger] pauseDownOperator:self];
    for (id<EROfflineBookObjDelegate> delegate in targets) {
        if([delegate respondsToSelector:@selector(EROfflineBookPauseDown)])
        {
            [delegate EROfflineBookPauseDown];
        }
    }
}
-(void)deleteBook
{
    [[EROfflineDownManger shareManger] cancelDownOperator:self];
    [[ERDataBase shareDBControl] deleteOfflineBook:self];
    [[NSFileManager defaultManager] removeItemAtPath:[self bookPath] error:nil];
    NSString *downPath = [[self bookPath] stringByAppendingString:@".downLoad"];
    [[NSFileManager defaultManager] removeItemAtPath:downPath error:nil];
    [[NSFileManager defaultManager] removeItemAtPath:[self bookCatalogPath] error:nil];
    [[NSFileManager defaultManager] removeItemAtPath:[self bookRangesCachePath] error:nil];
    
    
    NSString *bookPath=  [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/book/%@",self.gId]];
    [[NSFileManager defaultManager] removeItemAtPath:bookPath error:nil];
    
    for (id<EROfflineBookObjDelegate> delegate in targets) {
        if([delegate respondsToSelector:@selector(EROfflineBookDidDelete)])
        {
            [delegate EROfflineBookDidDelete];
        }
    }
}
-(void)bookDownFailed
{
    self.downLoadStatus = EROfflineBookDownLoadStatusIsPaused;
    for (id<EROfflineBookObjDelegate> delegate in targets) {
        if([delegate respondsToSelector:@selector(EROfflineBookDownFailed)])
        {
            [delegate EROfflineBookDownFailed];
        }
    }
}

-(void)bookDownFinished
{
    self.downLoadStatus = EROfflineBookDownLoadStatusIsDowned;
        [[ERDataBase shareDBControl] updateOfflineBookWithDic:self];
    for (id<EROfflineBookObjDelegate> delegate in targets) {
        if([delegate respondsToSelector:@selector(EROfflineBookDownFinish)])
        {
            [delegate EROfflineBookDownFinish];
        }
    }
}
-(void)bookDowning
{
      self.downLoadStatus = EROfflineBookLoadStatusIsDowning;
    [[ERDataBase shareDBControl] updateOfflineBookWithDic:self];
    for (id<EROfflineBookObjDelegate> delegate in targets) {
        if([delegate respondsToSelector:@selector(EROfflineBookDowning)])
        {
            [delegate EROfflineBookDowning];
        }
    }
}
@end
