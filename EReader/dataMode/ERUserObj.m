//
//  ERUserObj.m
//  ERReadder
//
//  Created by zym on 13-12-22.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import "ERUserObj.h"
#import "NSString+conver.h"
#import "ERApi.h"
#import "SBJson.h"
@implementation ERUserObj
@synthesize userId;
@synthesize userName;
@synthesize email;
@synthesize userPic;
@synthesize userSex;
-(void)dealloc
{
    self.userId = nil;
    self.userName = nil;
    [super dealloc];
}
-(id)proxyForJson
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(self.userId)
        [dic setObject:self.userId forKey:@"userId"];
    if(self.userName)
        [dic setObject:self.userName forKey:@"userName"];
    if(self.email )
        [dic setObject:self.email forKey:@"email"];
    if(self.userPic)
        [dic setObject:self.userPic forKey:@"userPic"];
    
    
    [dic setObject:[NSNumber numberWithInt:self.isAdmin] forKey:@"isAdmin"];
    [dic setObject:[NSNumber numberWithInt:self.userSex] forKey:@"sex"];
    return dic;
}
-(void)setUserInfoWithDic:(NSDictionary *)dic
{
    self.userId = [NSString_conver NULLStrChangeToStr:[dic objectForKey:@"userId"]];
    self.userName = [NSString_conver NULLStrChangeToStr:[dic objectForKey:@"userName"]];
    self.email = [NSString_conver NULLStrChangeToStr:[dic objectForKey:@"email"]];
    self.userPic = [NSString_conver NULLStrChangeToStr:[dic objectForKey:@"userPic"]];
    self.userSex = [[NSString_conver NULLStrChangeToStr:[dic objectForKey:@"sex"]] intValue];
    self.isAdmin = [[NSString_conver NULLStrChangeToStr:[dic objectForKey:@"isAdmin"]] intValue];
}
@end


static ERCurrentUserObj *currentUserInfo=nil;
@implementation ERCurrentUserObj
@synthesize userToken;
@synthesize password;
@synthesize userLogined;
+(ERCurrentUserObj *)currentUserInfo
{
    if(currentUserInfo == nil)
    {
        currentUserInfo = [[ERCurrentUserObj alloc] init];
    }
    return currentUserInfo;
}
-(id)init
{
    self = [super init];
    if(self)
    {
        self.userLogined = NO;
        //检测之前是否登陆
        //        NSString *token =[[NSUserDefaults standardUserDefaults] objectForKey:kUserToken];
        //        if(token)
        //        {
        //            self.userLogined = YES;
        //            self.userToken = token;
        //            self.userName =[[NSUserDefaults standardUserDefaults] objectForKey:kUserName];
        //            self.userId =[[NSUserDefaults standardUserDefaults] objectForKey:kUserId];
        //            self.isAdmin =[[NSUserDefaults standardUserDefaults] boolForKey:kUserIsAdmin];
        NSDictionary *userDic = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kUserInfoJsonStr];
        [self setUserInfoWithDic:userDic];
        self.email = [[NSUserDefaults standardUserDefaults] objectForKey:kUserEmail];
        if(self.userToken)
        {
            self.userLogined = YES;
        }
        
    }
    return self;
}
-(id)proxyForJson
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:self.userToken forKey:@"token"];
    if(self.password)
    {
        [dic setObject:self.password forKey:@"password"];
    }
    [dic addEntriesFromDictionary:[super proxyForJson]];
    return dic;
}
-(void)setUserInfoWithDic:(NSDictionary *)dic
{
    self.userToken = [dic objectForKey:@"token"];
    if([dic objectForKey:@"password"])
    {
        self.password = [dic objectForKey:@"password"];
    }
    [super setUserInfoWithDic:dic];
}


-(void)userAutoLogin
{
    if(self.email && self.password)
    {
        ERApi *api = [[ERApi alloc] init];
        [api setfinishBlock:^(NSDictionary *jsonValue) {
            int status = [[jsonValue objectForKey:@"status"] intValue];
            if(status!=1)
            {
                if(status==2)
                {
                    [[ERCurrentUserObj currentUserInfo] userLogOut];
                }
            }
            else
            {
                NSDictionary *data =[jsonValue objectForKey:@"data"];
                [self setUserInfoWithDic:data];
                [self userLogFinish];
            }
        } failBlock:^(NSString *errorStr) {
            [self userLogOut];
        }];
        [api loginWithUserEmail:self.email userPassword:self.password];
        [api release];
    }
}
-(void)userLogFinish
{
    self.userLogined = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserLoginFinish object:nil];
    [[NSUserDefaults standardUserDefaults] setObject:[self proxyForJson] forKey:kUserInfoJsonStr];
    [[NSUserDefaults standardUserDefaults] setObject:self.email forKey:kUserEmail];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void)userLogOut
{
    if(self.userLogined)
    {
        self.userLogined = NO;
        self.userId = nil;
        self.userName =nil;
        self.email = nil;
        self.userPic =nil;
        self.userSex = 0;
        self.isAdmin = 0;
        self.userToken = nil;
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserEmail];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserInfoJsonStr];
        //    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserId];
        //    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserIsAdmin];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserLogOut object:nil];
    }
}
-(void)dealloc{
    self.userToken = nil;
    self.password = nil;
    [super dealloc];
}
@end
