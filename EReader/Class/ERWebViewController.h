//
//  ERWebViewController.h
//  EReader
//
//  Created by Hudajiang on 14-1-7.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIMenuCatalog.h"

@interface ERWebViewController : UIViewController
{

}
@property (nonatomic,assign) CatalogType   currentCatalogType;
@property (nonatomic,copy) void(^downBlock)();
//阅读地址组装（目录）
- (void)loadInfo:(NSMutableDictionary *)infoDic withCateID:(int)_cateID;
//直接打开阅读章节
- (void)loadWebView:(NSString *)urlStr withBookName:(NSString *)_bookName withInfo:(NSMutableDictionary *)_infos;
@end
