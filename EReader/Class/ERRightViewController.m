//
//  ERRightViewController.m
//  EReader
//
//  Created by Hudajiang on 13-11-20.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import "ERRightViewController.h"

@interface ERRightViewController ()

@end

@implementation ERRightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [super dealloc];
}
@end
