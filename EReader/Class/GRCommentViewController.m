//
//  GRCommentViewController.m
//  GameRaiders
//
//  Created by helfy  on 13-10-13.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import "GRCommentViewController.h"
#import "ERApi.h"
#import "MIImageBGView.h"

#import "GRCommentObj.h"
//#import "MIADViewer.h"
#import "GRCommentListCell.h"
#import "ERUserLoginViewController.h"
#import "MBProgressHUD.h"
#import "UIScrollView+SpiralPullToRefresh.h"
@interface GRCommentViewController ()
{
    UITableView *tableView;
    ERApi *grApi;
    ERApi * subitApi;
    
    NSMutableArray *dataSource;
    BOOL isAddMore;
    BOOL isLoading;

    UIImageView *bottomBar;
    UITextView *messageField;
    UIButton *sendButton;
    UIImageView *massageBgView;
    UITapGestureRecognizer *tapHiddeKeyBoard;
    int remain;
    float keyBoardHeigth ;
    
    MBProgressHUD *commentHud;
    
    UIView *line;
    
    NSDictionary *targetObj;
}
@end

@implementation GRCommentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        dataSource = [[NSMutableArray alloc] init];
    }
    return self;
}
-(void)dealloc
{

    [super dealloc];
}
-(void)setGameObj:(NSDictionary *)obj
{
    targetObj = [obj retain];

    [tableView triggerPullToRefresh];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    CGRect frame = CGRectMake(0, 0, 0, 0);
    frame.size.height = 0;
    frame.size.width = 320;
    
//    self.view.backgroundColor = kBgColor;
    
    MIImageBGView *backImage = [[MIImageBGView alloc] initWithFrame:self.view.bounds];
    [backImage setImage:[UIImage imageNamed:@"comment_tableview_bg.png"]];
    [self.view addSubview:backImage];
    [backImage release];
    
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, frame.size.height, frame.size.width, self.view.bounds.size.height-frame.size.height-frame.size.height-self.navigationController.navigationBar.frame.size.height-42) style:UITableViewStylePlain];
    tableView.delegate = (id)self;
    tableView.dataSource = (id)self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    tableView.initialCellTransformBlock = ADLivelyTransformFade;
    [self.view addSubview:tableView];
    tableView.backgroundColor =[UIColor clearColor];
    [tableView addPullToRefreshWithActionHandler:^ {
        int64_t delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self refData];
        });
    }];
    self.title = @"评论";
    
//    line = [[UIView alloc] initWithFrame:CGRectMake(16, -200, 5, 1000)];
//    line.backgroundColor =[UIColor colorWithRed:240/255.0 green:240/255.0 blue:243/255.0 alpha:1];
//    [self.view addSubview:line];
//    [line release];
    
//    UINavigationBar *naviBar = [[UINavigationBar alloc] initWithFrame:frame];
//    UIImageView *imageabg = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"navbar_bg.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:20]] ;
//    imageabg.frame = naviBar.bounds;
//    [naviBar addSubview:imageabg];
//    [self.view addSubview:naviBar];
//    
//    [imageabg release];
//
//   UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0,160, frame.size.height)];
//    titleLabel.text = @"评论";
//    titleLabel.backgroundColor = [UIColor clearColor];
//    titleLabel.adjustsFontSizeToFitWidth = YES;
//    titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleLabel.textColor = [UIColor colorWithRed:249/255.0 green:248/255.0 blue:241/255.0 alpha:1];
//    titleLabel.font = [UIFont boldSystemFontOfSize:20];
//    [naviBar addSubview:titleLabel];
//    
//    UIButton *leftBtn_=[UIButton buttonWithType:UIButtonTypeCustom];
//    [leftBtn_ setBackgroundImage:[UIImage imageNamed:@"navBar_button_back_portrait"] forState:UIControlStateNormal];
//    //    [leftBtn_ setBackgroundImage:[[UIImage imageNamed:@"header_leftbtn_press.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:15] forState:UIControlStateHighlighted];
//    [leftBtn_ setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
//    [leftBtn_ addTarget:self action:@selector(popBack) forControlEvents:UIControlEventTouchUpInside];
//    [leftBtn_.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
//    [leftBtn_ setTitle:@"返 回" forState:UIControlStateNormal];
//    [leftBtn_ setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
//    leftBtn_.frame=CGRectMake(20, 8, 55, 30);
    
//    UIButton *commentBtn_=[UIButton buttonWithType:UIButtonTypeCustom];
//    [commentBtn_ setBackgroundImage:[[UIImage imageNamed:@"navBar_button_bg_portrait"] stretchableImageWithLeftCapWidth:20 topCapHeight:15] forState:UIControlStateNormal];
//    //    [commentBtn_ setBackgroundImage:[[UIImage imageNamed:@"btnbbb.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:15] forState:UIControlStateHighlighted];
//    [commentBtn_ setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
//    [commentBtn_ addTarget:self action:@selector(showReportListActionSheet) forControlEvents:UIControlEventTouchUpInside];
//    [commentBtn_.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
//    [commentBtn_ setTitle:@"反 馈" forState:UIControlStateNormal];
//    commentBtn_.frame=CGRectMake(20, 7, 55, 30);
//    UIBarButtonItem *commentItem_=[[UIBarButtonItem alloc]initWithCustomView:commentBtn_];
//    
    
//    UIBarButtonItem *leftbtnItem_=[[UIBarButtonItem alloc]initWithCustomView:leftBtn_];
    //naviBar.leftBarButtonItem=leftbtnItem_;
//    UINavigationItem *item= [[UINavigationItem alloc] initWithTitle:nil];
//    item.leftBarButtonItem = leftbtnItem_;
//    item.rightBarButtonItem = commentItem_;
//    [self.navigationController.navigationItem pushNavigationItem:item animated:NO];
//    [self.view addSubview:naviBar];
    
//    [leftbtnItem_ release];
//    [commentItem_ release];
//    [item release];

//    [naviBar release];
    

    
    bottomBar =[[UIImageView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-42, 320, 42)];
    bottomBar.image = [[UIImage imageNamed:@"input-bar"] resizableImageWithCapInsets:UIEdgeInsetsMake(19.0f, 3.0f, 19.0f, 3.0f)];
    bottomBar.userInteractionEnabled = YES;
    bottomBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:bottomBar];
    
    massageBgView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    
    messageField = [[UITextView alloc] initWithFrame:CGRectMake(10, 3, 246, 100)];
    messageField.backgroundColor = [UIColor clearColor];
    messageField.font = [UIFont systemFontOfSize:16];
    messageField.delegate = (id)self;
    messageField.scrollIndicatorInsets = UIEdgeInsetsMake(13.0f, 0.0f, 14.0f, 7.0f);
    messageField.contentInset = UIEdgeInsetsMake(0, 0.0f, 10.0f, 0.0f);
    [bottomBar addSubview:messageField];
    
    massageBgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 246, 42)];
    massageBgView.image = [[UIImage imageNamed:@"input-field.png"] stretchableImageWithLeftCapWidth:12 topCapHeight:20];
    
    //    massageBgView.userInteractionEnabled = YES;
    massageBgView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [bottomBar addSubview:massageBgView];
    
    sendButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [sendButton setBackgroundImage:[UIImage imageNamed:@"navBar_button_bg_portrait.png"]  forState:UIControlStateNormal];
//    [sendButton setBackgroundImage:[UIImage imageNamed:@"send12.png"]  forState:UIControlStateHighlighted];
    [sendButton setTitle:@"发送" forState:UIControlStateNormal];
    sendButton.enabled = NO;
    [sendButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [sendButton addTarget:self action:@selector(submitComment) forControlEvents:UIControlEventTouchUpInside];
    [sendButton.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    //    [sendButton setTitle:@"OK" forState:UIControlStateNormal];
    sendButton.frame=CGRectMake(320-60, 5, 50, 32);
    sendButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [bottomBar addSubview:sendButton];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    swipe.numberOfTouchesRequired = 1;
    [bottomBar addGestureRecognizer:swipe];
    [swipe release];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)setfinishBlock:(void (^)(int count))filish
{
    filishBolck = Block_copy(filish);
    
}
-(void)submitComment
{
    [messageField resignFirstResponder];
    if(![[ERCurrentUserObj currentUserInfo] userLogined])
    {
        UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
        ERUserLoginViewController *login =[[[ERUserLoginViewController alloc] init] autorelease];
        
        UINavigationController *flip = [[[UINavigationController alloc] initWithRootViewController:login] autorelease];
        //    login.delegate = self;
        flip.navigationBarHidden = YES;
        [rootViewController presentModalViewController:flip animated:YES];
        return;
    }
    if(messageField.text == nil)
    {
        MBProgressHUD *newHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        newHud.mode = MBProgressHUDModeText;
        newHud.labelText = @"您什么都还没说呢";
        [newHud setRemoveFromSuperViewOnHide:YES];
        [newHud show:YES];
        [newHud hide:YES afterDelay:1.5];
        newHud = nil;
        return;
    }
    else {
        
        NSInteger count = [[messageField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length] ;
        if(count < 1)
        {
            MBProgressHUD *newHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            newHud.mode = MBProgressHUDModeText;
            newHud.labelText = @"能不能长点~~";
            [newHud setRemoveFromSuperViewOnHide:YES];
            [newHud show:YES];
            [newHud hide:YES afterDelay:1.5];
            newHud = nil;
            return;
        }
        if (count >300) {
            MBProgressHUD *newHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            newHud.mode = MBProgressHUDModeText;
            newHud.labelText = @"亲～太长了！服务器受不了呢～";
            [newHud setRemoveFromSuperViewOnHide:YES];
            [newHud show:YES];
            [newHud hide:YES afterDelay:1.5];
            newHud = nil;
            return;
        }
    }
    commentHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	commentHud.mode = MBProgressHUDModeIndeterminate;
    [commentHud setRemoveFromSuperViewOnHide:YES];
    [commentHud show:YES];
    
    commentHud.labelText = @"评论中";
    
    
    if(subitApi)
    {
        [subitApi cancel];
        ObjRelease(subitApi);
    }
    //提示
    subitApi = [[ERApi alloc] init];
    subitApi.apiDelegate = (id)self;
    [subitApi postCommentWithBookId:[targetObj objectForKey:@"gid"] userToken:[[ERCurrentUserObj currentUserInfo] userToken] comment:messageField.text];    
}
#pragma mark 输入框
-(void)addTap
{
    if(tapHiddeKeyBoard == nil)
    {
        tapHiddeKeyBoard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenKeyBoard)];
    }
    [tableView addGestureRecognizer:tapHiddeKeyBoard];
}
-(void)removeTap
{
    if(tapHiddeKeyBoard)
    {
        [tableView removeGestureRecognizer:tapHiddeKeyBoard];
    }
    
}
- (void)handleSwipe:(UIGestureRecognizer *)guestureRecognizer
{
    [messageField resignFirstResponder];
}

-(void)hiddenKeyBoard
{
    [messageField resignFirstResponder];
}
-(void)keyboardWillShow:(NSNotification *)notify
{
    [self addTap];
    CGSize keyBoardSize = [[notify.userInfo objectForKey:@"UIKeyboardBoundsUserInfoKey"] CGRectValue].size;
    float animationDuration = [[notify.userInfo objectForKey:@"UIKeyboardAnimationDurationUserInfoKey"] floatValue];
    keyBoardHeigth = keyBoardSize.height;
    
    //      messageField.text=messageString;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    
    
    CGFloat textViewContentHeight = messageField.contentSize.height;
    CGRect textFrame = messageField.frame;
    textFrame.size.height = textViewContentHeight;
    float barHeigth =(textViewContentHeight +6)<42?42:(textViewContentHeight+6);
    barHeigth= barHeigth >100?100:barHeigth;
    
    if(messageField.text.length ==0)barHeigth=42;
    bottomBar.frame = CGRectMake(bottomBar.frame.origin.x,self.view.frame.size.height-keyBoardHeigth-barHeigth, bottomBar.frame.size.width,barHeigth);

   
    tableView.frame=CGRectMake(0, tableView.frame.origin.y, 320, self.view.frame.size.height-tableView.frame.origin.y-(keyBoardHeigth+bottomBar.frame.size.height));
  
    [tableView setContentInset:UIEdgeInsetsMake(5, 0, 0, 0)];
    //
    //
    //    [self textViewDidChange:messageField];
    //    bottomBar.frame =CGRectMake(0, self.view.bounds.size.height-bottomBar.frame.size.height-keyBoardSize.height, 320,   bottomBar.frame.size.height);
    //    commentTable.frame=CGRectMake(0, 44, 320, kScreenHightWithOutStatusBar-44-  bottomBar.frame.size.height-keyBoardSize.height);
    
    [UIView commitAnimations];
}

-(void)keyboardWillHide:(NSNotification *)notify
{
    float animationDuration = [[notify.userInfo objectForKey:@"UIKeyboardAnimationDurationUserInfoKey"] floatValue];
    keyBoardHeigth=0;
    [self removeTap];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    //    bottomBar.frame.size.height
    bottomBar.frame =CGRectMake(0, self.view.bounds.size.height-42, 320, 42);
    tableView.frame=CGRectMake(0, tableView.frame.origin.y, 320, self.view.frame.size.height-tableView.frame.origin.y-bottomBar.frame.size.height);
 
    //    ObjRelease(messageString);
    //    messageString = [messageField.text copy];
    
    [UIView commitAnimations];
}
- (NSString *)trimWhitespace:(NSString *)str
{
    NSMutableString *endstr = [str mutableCopy];
    CFStringTrimWhitespace(( CFMutableStringRef)endstr);
    return [endstr autorelease];
}

- (void)textViewDidChange:(UITextView *)textView
{
    //    CGFloat maxHeight = [MessageInputView maxHeight];
    CGFloat textViewContentHeight = textView.contentSize.height;
    CGRect textFrame = textView.frame;
    textFrame.size.height = textViewContentHeight;
    
    sendButton.enabled = ([self trimWhitespace:textView.text].length > 0);
    
//    if (sendButton.enabled) {
//        [sendButton setBackgroundImage:[UIImage imageNamed:@"send12.png"]  forState:UIControlStateNormal];
//        [sendButton setBackgroundImage:[UIImage imageNamed:@"send11.png"]  forState:UIControlStateHighlighted];
//    }
//    else{
//        [sendButton setBackgroundImage:[UIImage imageNamed:@"send11.png"]  forState:UIControlStateNormal];
//        [sendButton setBackgroundImage:[UIImage imageNamed:@"send12.png"]  forState:UIControlStateHighlighted];
//    }
    
    float barHeigth =(textViewContentHeight +6)<42?42:(textViewContentHeight+6);
    barHeigth= barHeigth >100?100:barHeigth;
    
    if(textView.text.length ==0)barHeigth=42;
    if(barHeigth != bottomBar.frame.size.height)
    {
        [UIView beginAnimations:nil context:nil];
        bottomBar.frame = CGRectMake(bottomBar.frame.origin.x,self.view.frame.size.height-keyBoardHeigth-barHeigth, bottomBar.frame.size.width,barHeigth);
//        bottomScroller.frame = bottomBar.bounds;
//        commentTable.frame=CGRectMake(0, 44, 320, kScreenHightWithOutStatusBar-44-(keyBoardHeigth+bottomBar.frame.size.height));
        
        //       massageBgView.frame = CGRectMake(massageBgView.frame.origin.x,massageBgView.frame.origin.y, massageBgView.frame.size.width,barHeigth);
        //       textView.frame = CGRectInset(massageBgView.bounds, 0, 12);
        
        
        [UIView commitAnimations];
        //       if(textView.text.length >0)
        //       {
        //           [textView scrollRangeToVisible:NSMakeRange(textView.text.length-1, 1)];
        //       }
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)popBack
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowMainViewNotification" object:nil];
//    if (isModeView) {
//        [self dismissModalViewControllerAnimated:YES];
//    }
//    else{
        [self.navigationController popViewControllerAnimated:YES];
//    }
}
- (void)showReportListActionSheet
{
    
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"投诉类型" delegate:(id)self cancelButtonTitle:@"取消" destructiveButtonTitle:@"色情内容" otherButtonTitles:@"不当发言",@"虚假广告",@"其他",nil];
    
    [action showInView:self.view];
    [action release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex != 4) {
        
        ERApi *reopt = [[[ERApi alloc] init] autorelease];
        [reopt setfinishBlock:^(NSDictionary *jsonValue) {
            int status = [[jsonValue objectForKey:@"status"] intValue];
            if(status!=1)
            {
                MBProgressHUD*  hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.labelText = [jsonValue objectForKey:@"error"];
                [hud setRemoveFromSuperViewOnHide:YES];
                [hud show:YES];
                [hud hide:YES afterDelay:2];
                
                if(status==2)
                {
                    [[ERCurrentUserObj currentUserInfo] userLogOut];
                }
            }
            else
            {
                MBProgressHUD*  hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.labelText = [jsonValue objectForKey:@"data"];
                [hud setRemoveFromSuperViewOnHide:YES];
                [hud show:YES];
                [hud hide:YES afterDelay:2];
                
            }
        } failBlock:^(NSString *errorStr) {
            
            MBProgressHUD*  hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = errorStr;
            [hud setRemoveFromSuperViewOnHide:YES];
            [hud show:YES];
            [hud hide:YES afterDelay:2];
        }];
//        [reopt reportWithBookId:[targetObj objectForKey:@"gid"] userToken:[[ERCurrentUserObj currentUserInfo] userToken] reportType:buttonIndex];
    }

    
  
}

#pragma mark ad

#define kCoummentCount 30
-(void)refData
{
    if(isLoading)return;
    isAddMore = NO;
    isLoading = YES;
    if(grApi)
    {
        grApi.apiDelegate = nil;
        [grApi cancel];
        ObjRelease(grApi);
    }

    grApi = [[ERApi alloc] init];
    grApi.apiDelegate = (id)self;
    [grApi getCommentForBookId:[targetObj objectForKey:@"gid"] count:kCoummentCount maxId:@"0" mixId:nil];
}
-(void)addMore
{
    if(isAddMore)return;
    if(grApi)
    {
        grApi.apiDelegate = nil;
        [grApi cancel];
        ObjRelease(grApi);
    }
    grApi = [[ERApi alloc] init];
    grApi.apiDelegate = (id)self;
    isAddMore = YES;
    isLoading = NO;
    GRCommentObj *obj =[dataSource lastObject];
    
    NSString *minId = obj.commentId;
    [grApi getCommentForBookId:[targetObj objectForKey:@"gid"] count:kCoummentCount maxId:nil mixId:minId];
}
//json
-(void)ERApiDidFinish:(id)api  respone:(id)jsonValue
{
    
    if(api == subitApi)
    {       
        int status = [[jsonValue objectForKey:@"status"] intValue];
        if(status!=1)
        {
            [self ERApiDidFail:api error:[jsonValue objectForKey:@"error"]];
            if(status==2)
            {
                [[ERCurrentUserObj currentUserInfo] userLogOut];
            }
        }
        else
        {
            commentHud.mode = MBProgressHUDModeText;
            commentHud.labelText = @"评论成功";
            [commentHud hide:YES afterDelay:1];
            messageField.text = @"";
            
            [self textViewDidChange:messageField];
            [self hiddenKeyBoard];
            [tableView triggerPullToRefresh];
        
        }
    }
    else if(api == grApi)
    {
        
        if (jsonValue) {
            NSArray *data = [jsonValue objectForKey:@"data"];
            remain = [[jsonValue objectForKey:@"remain"] intValue];
         
            if (!isAddMore) {
                [dataSource removeAllObjects];
            }
            for (NSDictionary *dic in data)
            {
                GRCommentObj *commentObj = [GRCommentObj objForDic:dic];
                [dataSource addObject:commentObj];
            }
//            gameObj.commtentCount = remain + dataSource.count;
//            filishBolck(gameObj.commtentCount);
            [tableView reloadData];
        }
        isLoading = NO;
        isAddMore = NO;
        [tableView.pullToRefreshController didFinishRefresh];
    }
}
//获取信息失败
-(void)ERApiDidFail:(id)api  error:(NSString *)errorStr
{
    if(api == subitApi)
    {
        commentHud.mode = MBProgressHUDModeText;
        commentHud.labelText =errorStr;
        [commentHud hide:YES afterDelay:1];
    }
    else if(api == grApi)
    {
        
        isLoading = NO;
        isAddMore = NO;
        [tableView.pullToRefreshController didFinishRefresh];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    [scrollView.pullToRefreshController setUserdraging];
}

//=========table
- (int)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    line.hidden=(dataSource.count==0);
    int count = dataSource.count;
    if(dataSource.count>0)
    {
        count = dataSource.count+1;
    }
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row >=dataSource.count)return 40;
    GRCommentObj *obj =[dataSource objectAtIndex:indexPath.row];
    
    CGFloat height = [GRCommentListCell heigthForOb:obj];
    
    
    return height+30;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row <dataSource.count)
    {
        static NSString *cellIdentifier = @"commentListCell";
        GRCommentListCell *cell = [_tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            cell = [[[GRCommentListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        GRCommentObj *obj =[dataSource objectAtIndex:indexPath.row];
        [cell setDisplayObj:obj withHigthLigthId:@""];
        return cell;
        
    }
    else{
        static NSString *cellIdentifier = @"addMore";
        UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if(cell == nil)
        {
            
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIImageView *commentLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"comment_show_replies"]];
            commentLine.backgroundColor = [UIColor clearColor];
            commentLine.frame = CGRectMake(30, 5, 250, 29);
            [cell insertSubview:commentLine atIndex:0];
            [commentLine release];
        }
        if(isAddMore == NO)
        {
            if(remain == 0 )
            {
                cell.textLabel.text = @"已加载完所有评论";
            }
            else{
                cell.textLabel.text = [NSString stringWithFormat:@"还剩%i条 点击加载更多",remain];
            }
            //            cell.textLabel.text = @"点击添加更多";
            cell.textLabel.textAlignment = UITextAlignmentCenter;
            cell.textLabel.font = [UIFont systemFontOfSize:15];
            cell.textLabel.textColor = [UIColor colorWithRed:54/255.0 green:59/255.0 blue:64/255.0 alpha:1];//[UIColor colorWithRed:83/255.0 green:69/255.0 blue:51/255.0 alpha:1];
            
        }
        cell.textLabel.textAlignment = UITextAlignmentCenter;
        return cell;
        
    }

   
    
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row <dataSource.count)
    {
    }
    else{
        if(remain==0)return;
        
        UITableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
        cell.textLabel.text = @"加载中...";
        [self addMore];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
