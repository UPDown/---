//
//  ERBookDetailedViewController.h
//  EReader
//
//  Created by helfy  on 13-12-1.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIMenuCatalog.h"

@interface ERBookDetailedViewController : UIViewController
{
    void(^refDataFinish)(NSDictionary *dic);

}
-(void)setrefDataFinsihBlock:(void (^)(NSDictionary *dic))refDataFinish;
- (void)loadDetaiData:(NSDictionary *)detailInfo withCateID:(int)_cateID withCatalogType:(CatalogType)catalogType;
@end
