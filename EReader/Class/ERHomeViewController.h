//
//  ERHomeViewController.h
//  EReader
//
//  Created by Hudajiang on 13-11-20.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
#import "EGORefreshTableFooterView.h"

@class MIMenuCatalog;
@interface ERHomeViewController :UIViewController <EGORefreshTableDelegate>
{
    //EGOHeader
    EGORefreshTableHeaderView *_refreshHeaderView;
    //EGOFoot
    EGORefreshTableFooterView *_refreshFooterView;
    //
    BOOL _reloading;
}

- (void)refreshData:(NSString *)urlString;
-(void)changeCatalog:(MIMenuCatalog *)catalog;
@end
