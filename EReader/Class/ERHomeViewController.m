//
//  ERHomeViewController.m
//  EReader
//
//  Created by Hudajiang on 13-11-20.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import "ERHomeViewController.h"
#import "SBJson.h"
#import "UIImageView+WebCache.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerBarButtonItem.h"
#import "ASIHTTPRequest.h"
#import "MBProgressHUD.h"
#import "MIMenuCatalog.h"
#import "FileManager.h"
#import <CommonCrypto/CommonDigest.h>
#import "ERBookDetailedViewController.h"
#import "MIImageBGView.h"
#import "MIADViewer.h"
#import "MobClick.h"
@implementation NSString (MD5)

- (NSString *)md5Encrypt {
    const char *original_str = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(original_str, strlen(original_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < 16; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
}
@end
#pragma mark - -BookCell
@interface BookCell : UITableViewCell

@property (nonatomic,retain)UIImageView *iconImageView;
@property (nonatomic,retain)UILabel     *titleLabel;
@property (nonatomic,retain)UILabel     *summaryLabel;
@property (nonatomic,retain)UILabel     *stateLabel;
@end


@implementation BookCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Custom initialization
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 120)];
        bgView.backgroundColor = [UIColor whiteColor];
        [self addSubview:bgView];
        [bgView release];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 100)];
        imageView.backgroundColor = [UIColor clearColor];
        imageView.center = CGPointMake(50.0, 60.0);
        [imageView setImage:[UIImage imageNamed:@"book_cover.png"]];
        self.iconImageView = imageView;
        [self addSubview:self.iconImageView];
        [imageView release];
        
        
        UILabel *m_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 10, 150, 30)];
        m_titleLabel.font = [UIFont systemFontOfSize:14.0];
        m_titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel = m_titleLabel;
        [self addSubview:self.titleLabel];
        [m_titleLabel release];
        
        
        UILabel *m_sumLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 50, 210, 60)];
        m_sumLabel.backgroundColor = [UIColor clearColor];
        m_sumLabel.font = [UIFont systemFontOfSize:10.0];
        m_sumLabel.textColor = [UIColor lightGrayColor];
        m_sumLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        m_sumLabel.numberOfLines = 4;
        self.summaryLabel = m_sumLabel;
        [self addSubview:self.summaryLabel];
        [m_sumLabel release];
        
        UILabel *m_stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(270, 15, 40, 20)];
        m_stateLabel.backgroundColor = [UIColor clearColor];
        m_stateLabel.font = [UIFont systemFontOfSize:12.0];
        m_stateLabel.textColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1];
        self.stateLabel = m_stateLabel;
        [self addSubview:self.stateLabel];
        [m_stateLabel release];
        
      
    }
    return self;
}

- (void)dealloc
{
    self.iconImageView = nil;
    self.titleLabel = nil;
    self.summaryLabel = nil;
    [super dealloc];
}

@end


#pragma mark - -ERHomeViewController
@interface ERHomeViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *bookListArray;
    UITableView *bookTableView;
    
    int             cateID;
    ASIHTTPRequest *request;
    
    MIMenuCatalog *curCatalog;
    
    int             currentPage;
    int             currentSearchPage;
    
    EGORefreshPos   currentRefreshPos;
    MIADViewer *adView;
}
@property (retain, nonatomic) ASIHTTPRequest *request;

- (void)getRequestFailed:(ASIHTTPRequest *)theRequest;
- (void)getRequestComplete:(ASIHTTPRequest *)theRequest;
@end

@implementation ERHomeViewController
@synthesize request;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        bookListArray = [[NSMutableArray alloc] init];
        cateID = 0;
        currentPage =1;
        currentSearchPage = 1;
        currentRefreshPos = EGORefreshHeader;
        adView = [MIADViewer shareADView];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.mm_drawerController setLeftDrawerViewController:(UIViewController *)SharedAppDelegate.menuViewController];
    [adView setDelegate:(id)self];
    [self.view addSubview:adView];
    if(adView.hidden == NO)
    {
        [self adLoadFilish:adView];
    }
    
}
-(void)adLoadFilish:(MIADViewer*)view
{
    view.frame = CGRectMake(0, self.view.frame.size.height-view.frame.size.height, view.frame.size.width, view.frame.size.height);
    [self.view addSubview:view];

    CGRect frame = self.view.bounds;
    float height = frame.size.height - self.navigationController.navigationBar.frame.size.height;
    
    if (IsIOS7) {
        frame.origin.y =self.navigationController.navigationBar.frame.size.height;
        height = height-20;
        frame.origin.y = frame.origin.y + 20;
    }
    else{
        height = height + self.navigationController.navigationBar.frame.size.height;
    }
    
    frame.size.height =height - view.frame.size.height;
    bookTableView.frame = frame;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    MIImageBGView *backImage = [[[MIImageBGView alloc] initWithFrame:self.view.bounds] autorelease];
    [self.view addSubview:backImage];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    
    
    CGRect frame = self.view.bounds;
    float height = frame.size.height - self.navigationController.navigationBar.frame.size.height;
 
    
    if (IsIOS7) {
       frame.origin.y =self.navigationController.navigationBar.frame.size.height;
        height = height-20;
        frame.origin.y = frame.origin.y + 20;
    }
    else{
        UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] init];
        backButtonItem.tintColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.3];
        self.navigationItem.backBarButtonItem = backButtonItem;
        [backButtonItem release];
    }
   
    frame.size.height  = height;
 
    bookTableView = [[UITableView alloc] initWithFrame:frame];
    [self.view addSubview:bookTableView];
    bookTableView.delegate = self;
    bookTableView.dataSource = self;
    bookTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    bookTableView.backgroundColor = [UIColor clearColor];

    [bookTableView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew  context:NULL];
    
    [self createHeaderView];
    [self setFooterView];
    
    MIMenuCatalog *catalog = [[[MIMenuCatalog alloc] init] autorelease];
    catalog.title = @"首页";
    catalog.type = MIMenuCatalogHome;
    [self changeCatalog:catalog];
}

- (void)shouldRefreshData
{
    [bookTableView setContentOffset:CGPointMake(0, -(REFRESH_HEADER_REGION_HEIGHT+10)) animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
      [bookTableView removeObserver:self forKeyPath:@"contentSize"];
    [request setDelegate:nil];
    [request cancel];
    [request release];
    [bookListArray release];
    [super dealloc];
}

//api
- (void)refreshData:(NSString *)urlString{
    
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
//    [bookListArray removeAllObjects];

    
    
	[request setDelegate:nil];
	[request cancel];
    
    [self setRequest:[ASIHTTPRequest requestWithURL:[NSURL URLWithString:urlString]]];
    [request setTimeOutSeconds:20];
    [request setDelegate:self];
    [request setDidFailSelector:@selector(getRequestFailed:)];
    [request setDidFinishSelector:@selector(getRequestComplete:)];
    [request startAsynchronous];
}

-(void)changeCatalog:(MIMenuCatalog *)catalog
{
    _reloading =NO;
   	[bookTableView setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
	[_refreshHeaderView setState:EGOOPullRefreshNormal];
    [_refreshFooterView setState:EGOOPullRefreshNormal];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(shouldRefreshData) object:nil];
    [self performSelector:@selector(shouldRefreshData) withObject:nil afterDelay:0.2];
    
    currentPage = 1;
    currentSearchPage = 1;
    ERAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate.drawerViewController setCenterViewController:delegate.drawerViewController.centerViewController withFullCloseAnimation:YES completion:^(BOOL finished) {
        
    }];
     
    self.title = catalog.title;
    if(catalog.type == curCatalog.type
       && [catalog.title isEqualToString:curCatalog.title])
    {
        return;
    }
    ObjRelease(curCatalog);
    curCatalog = [catalog retain];
    
    [bookListArray removeAllObjects];
//    [bookTableView triggerPullToRefresh];
    
    
    cateID = [catalog.categoryID intValue];
    

    
    if(catalog.type == MIMenuCatalogCatalogList)
    {
        NSString *urlString = [NSString stringWithFormat:@"http://m.baidu.com/book/data/cate?pn=%i&tj=book_cates_1_%@_%@&cateid=%@",currentPage,catalog.categoryID,catalog.categoryID,catalog.categoryID];
        [self refreshData:urlString];
    }
    else if (catalog.type == MIMenuCatalogRank)
    {
//        http://m.baidu.com/book/data/rank?pn=3
        NSString *urlString = [NSString stringWithFormat:@"http://m.baidu.com/book/data/rank?pn=%i",currentPage];
        [self refreshData:urlString];
    }
    else if (catalog.type == MIMenuCatalogHome)
    {
        NSString *urlString = [NSString stringWithFormat:@"http://m.baidu.com/book/data/recommend?pn=%i",currentPage];
        [self refreshData:urlString];
    }
    else if (catalog.type == MIMenuCatalogSearch)
    {
        
         NSString *urlString = [NSString stringWithFormat:@"http://m.baidu.com/pu=sz@1320_1001,usm@0/s?word=%@&st=11n041&tn=xse&tj=xsd_normal_b2&pn=%i",[curCatalog.title stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],currentSearchPage];
        [self refreshData:urlString];
    }
    
    //TODO
    [self readPinsFromDisk];
    [bookTableView reloadData];
    
}

- (void)savePinsToDisk{
    
    if(bookListArray.count){
//        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//        [dic setObject:dataSource forKey:@"data"];
//        [dic setObject:[NSNumber numberWithInt:remain] forKey:@"remain"];
//        
        NSString *jsons = [bookListArray JSONRepresentation];
        
        [FileManager saveJsonStr:jsons name:[self.title md5Encrypt]];
    }
    
}

- (void)readPinsFromDisk{
    
    NSString *jsons = [FileManager jsonStrWithName:[self.title md5Encrypt]];
    [bookListArray removeAllObjects];
    
    NSArray *jsonValue = [jsons JSONValue];
    if (jsonValue && jsonValue.count)
        [bookListArray addObjectsFromArray:jsonValue];
    
//    NSArray *data = [jsonValue objectForKey:@"data"];
//    remain = [[jsonValue objectForKey:@"remain"] integerValue];
//    
//    for (NSDictionary *objDic in data) {
//        GRGameObj *gameObj = [GRGameObj objForDic:objDic];
//        [dataSource addObject:gameObj];
//    }
    
//
}



-(void)fitterCatalogAndTitle
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    
    if([version floatValue]>[[MobClick getConfigParams:@"appVersion"] floatValue])
    {
 
    NSString *catalogFitterStr = [MobClick getConfigParams:@"catalogfitter111"];
    NSArray *titlefitter = [[MobClick getConfigParams:@"titlefitter"] componentsSeparatedByString:@","];
    NSMutableArray *removeArray = [NSMutableArray array];
    for (NSDictionary *dic in bookListArray) {
        NSString *category =[dic objectForKey:@"category"];
        NSString *title=[dic objectForKey:@"title"];
        if([catalogFitterStr rangeOfString:category].location != NSNotFound)
        {
            [removeArray addObject:dic];
            continue;
        }
        
        for (NSString *fitter in titlefitter) {
            if([title rangeOfString:fitter].location != NSNotFound)
            {
                [removeArray addObject:dic];
                break;
            }
        }
    }
    
    [bookListArray removeObjectsInArray:removeArray];
    }
}

#pragma mark  -ASIHTTPRequest
- (void)getRequestFailed:(ASIHTTPRequest *)theRequest
{
//     [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)getRequestComplete:(ASIHTTPRequest *)theRequest
{
    NSString *resultStr = [request responseString];

    NSDictionary *rootDic = [resultStr JSONValue];
//    NSLog(@"Response: %@", rootDic);
    if (rootDic && rootDic.count) {
        
            switch (curCatalog.type) {
                case MIMenuCatalogCatalogList:
                {
  /*                  author = "\U9c7c\U4eba\U4e8c\U4ee3";
                    category = "\U90fd\U5e02";
                    coverImage = "http://timg01.baidu-img.cn/timg?pa&size=@tpl_size@&sec=1371246007&di=268281d1bb02b42968b0ea77b87c7642&ref=http%3A%2F%2Fwww%2Eqidian%2Ecom%2Fbook%2F1931432%2Easpx&src=http%3A%2F%2Fimage%2Ecmfu%2Ecom%2Fbooks%2F1931432%2F1931432%2Ejpg";
                    cursrc = "";
                    curtitle = "";
                    gid = 2833112486;
                    hasNew = "";
                    lastChapter =                 {
                        href = "http://www.yxgsk.com/files/article/html/49/49707/8924001.html";
                        index = 3227;
                        rank = "";
                        text = "\U7b2c3215\U7ae0 \U5218\U5929\U5389\U7684\U6094\U8fc7";
                    };
                    listurl = "http://www.yxgsk.com/files/article/html/49/49707/index.html";
                    reason = "";
                    "rec_con" = "";
                    "rec_type" = "";
                    status = "\U8fde\U8f7d";
                    summary = "\U3011 \U4e00\U4e2a\U5927\U5c71\U91cc\U8d70\U51fa\U6765\U7684\U7edd\U4e16\U9ad8\U624b\Uff0c\U4e00\U5757\U80fd\U9884\U77e5\U672a\U6765\U7684\U795e\U79d8\U7389\U4f69\U2026\U2026\U6797\U9038\U662f\U4e00\U540d\U666e\U901a\U7684\U9ad8\U4e09\U5b66\U751f\Uff0c\U4e0d\U8fc7\Uff0c\U4ed6\U8fd8\U6709\U8eab\U8d1f\U53e6\U5916\U4e00\U4e2a\U91cd\U4efb\Uff0c\U90a3\U5c31\U662f\U6ce1\U6821\U82b1\Uff01\U800c\U4e14\U8fd8\U662f\U5949\U6821\U82b1\U8001\U7238\U4e4b\U547d\Uff01\U867d\U7136\U6797\U9038\U5f88\U4e0d\U60f3\U8ddf\U8fd9\U4f4d\U96be\U4f3a\U5019\U7684\U5927\U5c0f\U59d0\U6253\U4ea4\U9053\Uff0c\U4f46\U662f\U957f\U8f88\U4e4b\U547d\U96be\U8fdd\U6297\Uff0c\U4ed6\U4e0d\U5f97\U4e0d\U5343\U91cc\U8fe2\U8fe2\U7684\U8f6c\U5b66\U5230\U4e86\U677e\U5c71\U5e02\U7b2c\U4e00\U9ad8\U4e2d\Uff0c\U7ed9\U5927\U5c0f\U59d0\U978d\U524d\U9a6c\U540e\U7684\U5f53\U8ddf\U73ed\U2026\U2026\U4e8e\U662f\Uff0c\U53f2\U4e0a\U6700\U725b\U903c\U7684\U8ddf\U73ed\U51fa\U73b0\U4e86\U2014\U2014\U5927\U5c0f\U59d0\U7684\U8d34\U8eab\U9ad8\U624b\Uff01\U770b\U8fd9\U4f4d\U8ddf\U73ed\U5982\U4f55\U53d1\U5bb6\U81f4\U5bcc\U5077\U5c0f\U59d0\Uff0c\U5f00\U59cb\U4ed6\U5949\U65e8\U6ce1\U599e\U725bX\U95ea\U95ea\U7684\U4eba\U751f\U2026\U2026\U672c\U4e66\U6709\U70b9\U513f\U7eaf\U2026\U2026\U4e5f\U6709\U70b9\U513f\U5c0f\U66a7\U6627\U2026\U2026";
                    title = "\U6821\U82b1\U7684\U8d34\U8eab\U9ad8\U624b";
                },
     */
                    if (![[rootDic objectForKey:@"errno"] boolValue]) {
                        NSDictionary *resultDic = [rootDic objectForKey:@"result"];
                        if (resultDic && resultDic.count) {
                            NSArray *aray = [resultDic objectForKey:@"cate"];
                            
                            if (_reloading && currentRefreshPos == EGORefreshFooter) {
                                [bookListArray addObjectsFromArray:aray];
                            }
                            else{
                                [bookListArray setArray:aray];
                            }
                        }
                    }
                }
                    break;
                case MIMenuCatalogHome:
                {
       /*         {
                    author = "\U98ce\U51cc\U5929\U4e0b";
                    category = "\U7384\U5e7b";
                    coverImage = "http://timg01.baidu-img.cn/timg?pa&size=@tpl_size@&sec=1371246179&di=9b814b29eba72959ae3098bfad87efad&ref=http%3A%2F%2Fwww%2Eqidian%2Ecom%2Fbook%2F1952707%2Easpx&src=http%3A%2F%2Fimage%2Ecmfu%2Ecom%2Fbooks%2F1952707%2F1952707%2Ejpg";
                    cursrc = "";
                    curtitle = "";
                    gid = 1982980514;
                    hasNew = 0;
                    lastChapter =                 {
                        href = "";
                        index = "";
                        rank = "";
                        text = "";
                    };
                    listurl = "http://www.aiyun.com/html/13/13271/index.htm";
                    reason = "\U7c7b\U4f3c\U300a\U5927\U4e3b\U5bb0\U300b";
                    "rec_con" = "\U5927\U4e3b\U5bb0";
                    "rec_type" = 0;
                    status = "\U8fde\U8f7d";
                    summary = "\U4e00\U7b11\U98ce\U96f7\U9707\Uff0c\U4e00\U6012\U6ca7\U6d77\U5bd2\Uff1b\U4e00\U624b\U7834\U82cd\U7a79\Uff0c\U4e00\U5251\U821e\U957f\U5929\Uff01\U4e00\U4eba\U4e00\U5251\Uff0c\U50b2\U4e16\U4e5d\U91cd\U5929\Uff01(\U672c\U7ad9\U90d1\U91cd\U63d0\U9192\Uff1a\U672c\U6545\U4e8b\U7eaf\U5c5e\U865a\U6784\Uff0c\U5982\U6709\U96f7\U540c\Uff0c\U7eaf\U5c5e\U5de7\U5408\Uff0c\U5207\U52ff\U6a21\U4eff\U3002)";
                    title = "\U50b2\U4e16\U4e5d\U91cd\U5929";
                },
                    */
                    if (![[rootDic objectForKey:@"errno"] boolValue]) {
                        NSDictionary *resultDic = [rootDic objectForKey:@"result"];
                        if (resultDic && resultDic.count) {
                            NSArray *aray = [resultDic objectForKey:@"recommend"];
                            if (_reloading && currentRefreshPos == EGORefreshFooter) {
                                [bookListArray addObjectsFromArray:aray];
                            }
                            else{
                                [bookListArray setArray:aray];
                            }
                        }
                    }
                }
                    break;
                case MIMenuCatalogRank:
                {
        /*        {
                    author = "\U6211\U5403\U897f\U7ea2\U67ff";
                    category = "\U4ed9\U4fa0";
                    coverImage = "http://timg01.baidu-img.cn/timg?pa&quality=100&size=@tpl_size@&fsize=@tpl_fsize@&sec=1379347122&di=45d6a971a77654e3f28463210e57dd67&ref=http%3A%2F%2Fwww%2Eqidian%2Ecom%2FBook%2F2502372%2Easpx&src=http%3A%2F%2Fimage%2Ecmfu%2Ecom%2Fbooks%2F2502372%2F2502372%2Ejpg";
                    cursrc = "";
                    curtitle = "";
                    gid = 3961103225;
                    hasNew = 0;
                    lastChapter =                 {
                        href = "http://www.yxgsk.com/files/article/html/54/54411/8945554.html";
                        index = 677;
                        rank = "";
                        text = "\U7b2c\U4e8c\U5341\U4e00\U5377 \U7b2c\U5341\U56db\U7ae0 \U672c\U5c0a";
                    };
                    listurl = "http://www.yxgsk.com/files/article/html/54/54411/index.html";
                    reason = "693582\U4eba\U5728\U8bfb";
                    "rec_con" = 693582;
                    "rec_type" = 1;
                    status = "\U8fde\U8f7d";
                    summary = "\U83bd\U8352\U7eaa\U5728\U7ebf\U9605\U8bfb,\U4f5c\U8005:\U6211\U5403\U897f\U7ea2\U67ff...";
                    title = "\U83bd\U8352\U7eaa";
                },
         */           
                    if (![[rootDic objectForKey:@"errno"] boolValue]) {
                        NSDictionary *resultDic = [rootDic objectForKey:@"result"];
                        if (resultDic && resultDic.count) {
                            NSArray *aray = [resultDic objectForKey:@"rank"];
                            if (_reloading && currentRefreshPos == EGORefreshFooter) {
                                [bookListArray addObjectsFromArray:aray];
                            }
                            else{
                                [bookListArray setArray:aray];
                            }
                        }
                    }
                }
                    break;
                case MIMenuCatalogSearch:
                {
                    if (![[rootDic objectForKey:@"errno"] boolValue]) {
                        NSDictionary *resultDic = [rootDic objectForKey:@"result"];
                        if (resultDic && resultDic.count) {
                            NSArray *aray = [resultDic objectForKey:@"search"];
                            if (_reloading && currentRefreshPos == EGORefreshFooter) {
                                [bookListArray addObjectsFromArray:aray];
                            }
                            else{
                                [bookListArray setArray:aray];
                            }
                        }
                    }
                }
                    break;
                default:
                    break;
            }
    }
    [self fitterCatalogAndTitle];
    [self savePinsToDisk];
    if (bookListArray && bookListArray.count) {
        bookTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    else{
        bookTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    [bookTableView reloadData];
    
    [self finishedLoadData];
    
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark -Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}


#pragma mark -Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return bookListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    BookCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[BookCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    // Configure the cell...
    NSDictionary *dic = [bookListArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = [dic objectForKey:@"title"];
    cell.summaryLabel.text = [dic objectForKey:@"summary"];
    NSString *urlStr = [[dic objectForKey:@"coverImage"] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
    [cell.iconImageView cancelCurrentImageLoad];
    [cell.iconImageView setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:@"book_cover.png"] options:SDWebImageRetryFailed |
     SDWebImageProgressiveDownload andResize:CGSizeMake(80, 100)];
    cell.stateLabel.text = [dic objectForKey:@"status"];
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark -Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.mm_drawerController setLeftDrawerViewController:nil];

    ERBookDetailedViewController *detailViewController = [[ERBookDetailedViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:detailViewController animated:YES];
  
    [detailViewController loadDetaiData:[bookListArray objectAtIndex:indexPath.row] withCateID:cateID withCatalogType:curCatalog.type];
    [detailViewController setrefDataFinsihBlock:^(NSDictionary *dic) {
    [bookListArray replaceObjectAtIndex:indexPath.row withObject:dic];
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}];
    [detailViewController release];
    
}


#pragma mark
#pragma methods for creating and removing the header view

-(void)createHeaderView{
    if (_refreshHeaderView && [_refreshHeaderView superview]) {
        [_refreshHeaderView removeFromSuperview];
    }
	_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:
                          CGRectMake(0.0f, 0.0f - self.view.bounds.size.height,
                                     self.view.frame.size.width, self.view.bounds.size.height)];
    _refreshHeaderView.delegate = self;
    
	[bookTableView addSubview:_refreshHeaderView];
    
    [_refreshHeaderView refreshLastUpdatedDate];
}

-(void)finishedLoadData{
	
    [self finishReloadingData];
}

#pragma mark -
#pragma mark method that should be called when the refreshing is finished
- (void)finishReloadingData{
	
	//  model should call this when its done loading
	_reloading = NO;
    
	if (_refreshHeaderView) {
        [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:bookTableView];
    }
    
    if (_refreshFooterView) {
        [_refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:bookTableView];
    }
    
    // overide, the actula reloading tableView operation and reseting position operation is done in the subclass
}

-(void)setFooterView{
	//    UIEdgeInsets test = self.aoView.contentInset;
    // if the footerView is nil, then create it, reset the position of the footer
    CGFloat height = MAX(bookTableView.contentSize.height, bookTableView.frame.size.height);
    if (_refreshFooterView && [_refreshFooterView superview])
	{
        // reset position
        _refreshFooterView.frame = CGRectMake(0.0f,
                                              height,
                                              bookTableView.frame.size.width,
                                              self.view.bounds.size.height);
    }else
	{
        // create the footerView
        _refreshFooterView = [[EGORefreshTableFooterView alloc] initWithFrame:
                              CGRectMake(0.0f, height,
                                         bookTableView.frame.size.width, self.view.bounds.size.height)];
        _refreshFooterView.delegate = self;
        [bookTableView addSubview:_refreshFooterView];
    }
    
    if (_refreshFooterView)
	{
        [_refreshFooterView refreshLastUpdatedDate];
    }
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    CGSize newSize = [[change objectForKey:@"new"] CGSizeValue];
    float footerY = newSize.height;
    
    
    footerY = footerY > ((UIView *)object).frame.size.height?footerY:((UIView *)object).frame.size.height;
    
    CGRect rect = _refreshFooterView.frame;
    rect.origin.y =footerY;
    _refreshFooterView.frame = rect;
}

-(void)removeFooterView
{
    if (_refreshFooterView && [_refreshFooterView superview])
	{
        [_refreshFooterView removeFromSuperview];
    }
    _refreshFooterView = nil;
}

//===============
//刷新delegate
#pragma mark -
#pragma mark data reloading methods that must be overide by the subclass

-(void)beginToReloadData:(EGORefreshPos)aRefreshPos{
	
	//  should be calling your tableviews data source model to reload
	_reloading = YES;
    
    if (aRefreshPos == EGORefreshHeader)
	{
        currentRefreshPos =EGORefreshHeader;
        // pull down to refresh data
        [self performSelector:@selector(refreshView) withObject:nil afterDelay:2.0];
    }else if(aRefreshPos == EGORefreshFooter)
	{
        currentRefreshPos =EGORefreshFooter;
        // pull up to load more data
        [self performSelector:@selector(getNextPageView) withObject:nil afterDelay:2.0];
    }
	
	// overide, the actual loading data operation is done in the subclass
}

//刷新调用的方法
-(void)refreshView
{
	NSLog(@"刷新完成");
    [self finishedLoadData];
	
}
//加载调用的方法
-(void)getNextPageView
{
    
    if(curCatalog.type == MIMenuCatalogCatalogList)
    {
        currentPage++;
        NSString *urlString = [NSString stringWithFormat:@"http://m.baidu.com/book/data/cate?pn=%i&tj=book_cates_1_%@_%@&cateid=%@",currentPage,curCatalog.categoryID,curCatalog.categoryID,curCatalog.categoryID];
        [self refreshData:urlString];
    }
    else if (curCatalog.type == MIMenuCatalogRank)
    {
        currentPage++;
        NSString *urlString = [NSString stringWithFormat:@"http://m.baidu.com/book/data/rank?pn=%i",currentPage];
        [self refreshData:urlString];
    }
    else if (curCatalog.type == MIMenuCatalogHome)
    {
        
        currentPage++;
        NSString *urlString = [NSString stringWithFormat:@"http://m.baidu.com/book/data/recommend?pn=%i",currentPage];
        [self refreshData:urlString];
    }
    else if (curCatalog.type == MIMenuCatalogSearch)
    {
        currentSearchPage = currentSearchPage +20;
        NSString *urlString = [NSString stringWithFormat:@"http://m.baidu.com/pu=sz@1320_1001,usm@0/s?word=%@&st=11n041&tn=xse&tj=xsd_normal_b2&pn=%i",[curCatalog.title stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],currentSearchPage];
        [self refreshData:urlString];
    }
    
    //TODO

//    [bookTableView reloadData];
//    [self removeFooterView];
//    [self finishedLoadData];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
	if (_refreshHeaderView)
	{
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    }
	
	if (_refreshFooterView)
	{
        [_refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	if (_refreshHeaderView)
	{
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
	
	if (_refreshFooterView)
	{
        [_refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
    }
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    [self scrollViewDidEndDragging:scrollView willDecelerate:YES];
    
}

#pragma mark -
#pragma mark EGORefreshTableDelegate Methods

- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos
{
	
	[self beginToReloadData:aRefreshPos];
	
}

- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}


// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view
{
	
	return [NSDate date]; // should return date data source was last changed
	
}

@end
