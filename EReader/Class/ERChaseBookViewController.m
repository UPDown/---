//
//  ERChaseBookViewController.m
//  EReader
//
//  Created by Hudajiang on 14-1-8.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import "ERChaseBookViewController.h"
#import "FileManager.h"
#import "ERWebViewController.h"
#import "ERDataBase.h"
#import "ERBookDetailedViewController.h"
@interface ERChaseBookViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView     *listView;
    NSMutableArray  *bookListArray;
}
@end

@implementation ERChaseBookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        bookListArray = [[NSMutableArray alloc] initWithArray:[[ERDataBase shareDBControl] queryFavBooks]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    self.view.backgroundColor = [UIColor whiteColor];;
    self.view.layer.cornerRadius = 6;
    self.view.layer.masksToBounds = YES;
    self.title = @"我的收藏";

    UIBarButtonItem *lefItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"返 回", nil) style:UIBarButtonItemStylePlain target:self action:@selector(backView)];

    self.navigationItem.leftBarButtonItem = lefItem;

    CGRect frame = self.view.bounds;
    float height = frame.size.height - self.navigationController.navigationBar.frame.size.height;
    
    if (IsIOS7) {
        self.automaticallyAdjustsScrollViewInsets = NO;
//        self.navigationController.navigationBar.barTintColor = [UIColor blueColor];
//        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//        self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
        
        frame.origin.y =self.navigationController.navigationBar.frame.size.height;
        height = height-20;
        frame.origin.y = frame.origin.y + 20;
    }
    else{
        lefItem.tintColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.9];
    }
    
    frame.size.height  = height;
    
    listView = [[UITableView alloc] initWithFrame:frame];
    [self.view addSubview:listView];
    listView.delegate = self;
    listView.dataSource = self;
    listView.backgroundColor = [UIColor clearColor];
    
    if (bookListArray && bookListArray.count) {
        listView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    else{
        listView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    ObjRelease(listView);
    [super dealloc];
}

- (void)backView
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark -Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return bookListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    // Configure the cell...
    NSDictionary *dic = [bookListArray objectAtIndex:indexPath.row];

    cell.textLabel.text = [dic objectForKey:@"title"];
    cell.detailTextLabel.text = [dic objectForKey:@"author"];
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:10.0];
    
    return cell;
}


 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
     // Return NO if you do not want the specified item to be editable.
     return YES;
 }



 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
     if (editingStyle == UITableViewCellEditingStyleDelete) {
         // Delete the row from the data source
         NSMutableDictionary *dic = [bookListArray objectAtIndex:indexPath.row];
         
         [dic setObject:[NSNumber numberWithFloat:NO] forKey:@"fav"];
         if (dic) {
             [[ERDataBase shareDBControl]updateBookWithDic:dic];
         }
         
         [bookListArray removeObjectAtIndex:indexPath.row];
         
         [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
     }
     else if (editingStyle == UITableViewCellEditingStyleInsert) {
         // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
 }


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark -Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//
//    NSDictionary *dic = [bookListArray objectAtIndex:indexPath.row];
//    
//    ERWebViewController   *bookWebViewController = [[ERWebViewController alloc] initWithNibName:nil bundle:nil];
//    [self presentViewController:bookWebViewController animated:YES completion:^{
//        //        [bookWebViewController loadWebView:[dic objectForKey:@"url"] withBookName:[dic objectForKey:@"bookName"] withInfo:[dic objectForKey:@"infos"]];
//        
////        [bookWebViewController loadInfo:[dic objectForKey:@"infos"] withCateID:[[dic objectForKey:@"cateID"] intValue]];
//    }];
//    [bookWebViewController release];
    ERBookDetailedViewController *detailViewController = [[ERBookDetailedViewController alloc] initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:detailViewController animated:YES];
    
    [detailViewController loadDetaiData:[bookListArray objectAtIndex:indexPath.row] withCateID:0 withCatalogType:MIMenuCatalogHome];
    
    [detailViewController release];
}

@end
