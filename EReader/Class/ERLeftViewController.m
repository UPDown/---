//
//  ERLeftViewController.m
//  EReader
//
//  Created by Hudajiang on 13-11-20.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import "ERLeftViewController.h"
#import "SBJson.h"
#import "ERHomeViewController.h"
#import "ASIHTTPRequest.h"
#import "UIViewController+MMDrawerController.h"
#import "ERUserObj.h"
#import "UIImageView+WebCache.h"
#import "ERUserLoginViewController.h"
//#import "ERFavoriteViewController.h"

@interface ERLeftViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray  *listArray;
    
    UIImageView *headIconView;
    UILabel     *nameLabel;
    UILabel     *emailLabel;
    UILabel     *nullTipLabel;
    
    UITableView *_tableView;
    
    ASIHTTPRequest *request;
}
@property (retain, nonatomic) ASIHTTPRequest *request;

- (void)getCategorysFailed:(ASIHTTPRequest *)theRequest;
- (void)getCategorysComplete:(ASIHTTPRequest *)theRequest;
@end

@implementation ERLeftViewController
@synthesize request;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserHeadView) name:kUserLoginFinish object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserHeadView) name:kUserLogOut object:nil];
        
        listArray = [[NSMutableArray alloc] init];
        
        NSString *urlString = [NSString stringWithFormat:@"http://m.baidu.com/book/data/cates"];
        
        [self setRequest:[ASIHTTPRequest requestWithURL:[NSURL URLWithString:urlString]]];
        [request setTimeOutSeconds:20];
        [request setDelegate:self];
        [request setDidFailSelector:@selector(getCategorysFailed:)];
        [request setDidFinishSelector:@selector(getCategorysComplete:)];
        [request startAsynchronous];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // 个人
//    UIImageView *headerBGView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60)];
//    [self.view addSubview:headerBGView];
//    [headerBGView setBackgroundColor:[UIColor redColor]];
 
    UIButton *headBGButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0,  self.view.bounds.size.width, 60)];
    headBGButton.backgroundColor = [UIColor redColor];
    [headBGButton addTarget:self action:@selector(headButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:headBGButton];

    
    headIconView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 50, 50)];
    headIconView.userInteractionEnabled = NO;
    headIconView.backgroundColor = [UIColor blueColor];
    [headBGButton addSubview:headIconView];

    
    nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 5, 180, 20)];
    nameLabel.userInteractionEnabled = NO;
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.font = [UIFont boldSystemFontOfSize:18.0];
    [headBGButton addSubview:nameLabel];

    
    emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 30, 180, 20)];
    emailLabel.userInteractionEnabled = NO;
    emailLabel.backgroundColor = [UIColor clearColor];
    emailLabel.textColor = [UIColor whiteColor];
    emailLabel.font = [UIFont boldSystemFontOfSize:14.0];
    [headBGButton addSubview:emailLabel];
    
    
    nullTipLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 5, 180, 50)];
    nullTipLabel.userInteractionEnabled = NO;
    nullTipLabel.hidden = NO;
    nullTipLabel.backgroundColor = [UIColor redColor];
    nullTipLabel.textColor = [UIColor whiteColor];
    nullTipLabel.font = [UIFont boldSystemFontOfSize:16.0];
    nullTipLabel.text = @"未登录";
    [headBGButton addSubview:nullTipLabel];
    
    // 收藏
    UIImageView *favoritesBGView = [[UIImageView alloc] initWithFrame:CGRectMake(0,60,  self.view.bounds.size.width, 40)];
    favoritesBGView.userInteractionEnabled = YES;
    favoritesBGView.backgroundColor = [UIColor greenColor];
    [self.view addSubview:favoritesBGView];
    
    UIButton *favoriteButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 45, 30)];
    [favoriteButton setTitle:@"收藏" forState:UIControlStateNormal];
    [favoriteButton addTarget:self action:@selector(favoriteAction:) forControlEvents:UIControlEventTouchUpInside];
    [favoritesBGView addSubview:favoriteButton];
    [favoriteButton release];
    
    [favoritesBGView release];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 100,self.view.bounds.size.width, self.view.bounds.size.height-100)];
    [self.view insertSubview:_tableView belowSubview:headBGButton];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.showsHorizontalScrollIndicator = NO;
    
   [headBGButton release];
    
    [self updateUserHeadView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUserLoginFinish object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUserLogOut object:nil];
    
    [request setDelegate:nil];
    [request cancel];
	[request release];
    [listArray release];
    [headIconView release];
    [nameLabel release];
    [nullTipLabel release];
    [emailLabel release];
    [_tableView release];
    [super dealloc];
}

#pragma mark - Action
- (void)headButtonAction:(UIButton *)sender
{
    ERCurrentUserObj *userObj = [ERCurrentUserObj currentUserInfo];
    if (userObj.userLogined) {
        
        UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"注销" message:@"是否注销当前帐号" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alter show];
        [alter release];
    }
    else{
        UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
        
        ERUserLoginViewController *login =[[ERUserLoginViewController alloc] init];
        
        UINavigationController *flip = [[UINavigationController alloc] initWithRootViewController:login];
        flip.navigationBarHidden = YES;
        [rootViewController presentModalViewController:flip animated:YES];
        [flip release];
        [login release];
    }

}

- (void)favoriteAction:(UIButton *)sender
{
    UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    
//    ERFavoriteViewController *favorite =[[ERFavoriteViewController alloc] init];
//    
//    UINavigationController *flip = [[UINavigationController alloc] initWithRootViewController:favorite];
//    flip.navigationBarHidden = YES;
//    [rootViewController presentModalViewController:flip animated:YES];
//    [flip release];
//    [favorite release];
}

#pragma mark  - ASIHTTPRequest
- (void)getCategorysFailed:(ASIHTTPRequest *)theRequest
{

}

- (void)getCategorysComplete:(ASIHTTPRequest *)theRequest
{
    NSString *resultStr = [request responseString];
    
    NSDictionary *rootDic = [resultStr JSONValue];
    //            NSLog(@"Response: %@", result);
    if (rootDic && rootDic.count) {
        NSDictionary *resultDic = [rootDic objectForKey:@"result"];
        if (resultDic && resultDic.count) {
            NSArray *aray = [resultDic objectForKey:@"cates"];
            [listArray addObjectsFromArray:aray];
        }
    }
    [_tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"类别";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    // Configure the cell...
    NSDictionary *dic = [listArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [dic objectForKey:@"catename"];
    
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.mm_drawerController
     setCenterViewController:self.mm_drawerController.centerViewController
     withCloseAnimation:YES
     completion:^(BOOL finish) {
         
         ERHomeViewController* cc = (ERHomeViewController*)SharedAppDelegate.homeViewController;
         cc.navigationItem.title = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
         
         if ([cc respondsToSelector:@selector(refreshData:)]) {
             NSDictionary *dic = [listArray objectAtIndex:indexPath.row];
             [cc refreshData:[dic objectForKey:@"cateid"]];
         }

     }];

}

#pragma mark - - 外部调用
- (void)updateUserHeadView
{
    ERCurrentUserObj *userObj = [ERCurrentUserObj currentUserInfo];
    if (userObj.userLogined) {
        //登录
        nullTipLabel.hidden = YES;
        nameLabel.text = userObj.userName;
        emailLabel.text = userObj.email;
//        [headIconView setImageWithURL:<#(NSURL *)#> placeholderImage:<#(UIImage *)#>]
    }
    else{
        //未登录
        nullTipLabel.hidden = NO;
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex) {
         [[ERCurrentUserObj currentUserInfo] userLogOut];
    }
}

@end
