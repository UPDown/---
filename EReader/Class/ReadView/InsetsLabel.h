//
//  InsetsLabel.h
//  EReader
//
//  Created by Hudajiang on 14-2-13.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
@interface InsetsLabel : UIView
{
    CTFrameRef frameRef;
    NSString *showText;
}
@property(nonatomic) UIEdgeInsets insets;

- (id)initWithFrame:(CGRect)frame andInsets:(UIEdgeInsets) insets;
- (id)initWithInsets:(UIEdgeInsets) insets;

-(void)setShowText:(NSString *)text;
-(void)refDisplay;
-(CGRect)drawTextFrame;
@end
