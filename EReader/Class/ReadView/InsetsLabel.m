//
//  InsetsLabel.m
//  EReader
//
//  Created by Hudajiang on 14-2-13.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import "InsetsLabel.h"
#import "ERBook.h"
@implementation InsetsLabel
@synthesize insets = _insets;

- (id)initWithFrame:(CGRect)frame andInsets:(UIEdgeInsets)insets {
    self = [super initWithFrame:frame];
    if(self) {
        self.insets = insets;
    }
    return self;
}

- (id)initWithInsets:(UIEdgeInsets)insets {
    self = [super init];
    if(self) {
        self.insets = insets;
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.layer.geometryFlipped = YES;
    
    return self;
}
-(void)dealloc
{
        ObjRelease(showText);
    if(frameRef)
    {
        //release
        CFRelease(frameRef);
        frameRef=NULL;
    }
    [super dealloc];
}
//
//- (void)drawTextInRect:(CGRect)rect {
//    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.insets)];
//}
-(void)setShowText:(NSString *)text{
    
    ObjRelease(showText);
    showText = [text retain];
    //要绘制的文本
    if(showText)
    {
        [self creatframesetterRefWithText:showText];
    }
    
}
-(void)refDisplay
{

    if(showText)
    {
        [self creatframesetterRefWithText:showText];
    }
}

-(CGRect)drawTextFrame
{

    return CGRectInset(self.bounds, 15, 25);
}
-(void)creatframesetterRefWithText:(NSString *)text
{
    NSMutableAttributedString* attString = [ERBook creatAttributedStringWithText:text];
    
    if(frameRef)
    {
        //release
        CFRelease(frameRef);
        frameRef=NULL;
    }
    CTFramesetterRef framesetterRef = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attString);
    
    CGMutablePathRef path = CGPathCreateMutable(); //1
    CGPathAddRect(path, NULL, [self drawTextFrame] );
    frameRef = CTFramesetterCreateFrame(framesetterRef,
                                        CFRangeMake(0, [attString length]), path, NULL);
    
    CFRelease(framesetterRef);
    CGPathRelease(path);
    [self setNeedsDisplay];
}
- (void)drawRect:(CGRect)rect
{
    
    //绘图上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //修正坐标系
    //    CGAffineTransform textTran = CGAffineTransformIdentity;
    //    textTran = CGAffineTransformMakeTranslation(0.0, self.bounds.size.height);
    //    textTran = CGAffineTransformScale(textTran, 1.0, -1.0);
    //    CGContextConcatCTM(context, textTran);
    CTFrameDraw(frameRef, context);
    
}


@end
