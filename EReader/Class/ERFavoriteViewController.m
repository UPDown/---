//
//  ERFavoriteViewController.m
//  EReader
//
//  Created by Hudajiang on 13-11-28.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import "ERFavoriteViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ERWebViewController.h"
#import "FileManager.h"
#import "ERDataBase.h"
@interface ERFavoriteViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView     *listTableView;
    NSMutableArray  *listArray;
    
    UILabel *titleLabel ;
    
    BOOL        isLocalBookMark;
}
@end

@implementation ERFavoriteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        isLocalBookMark = YES;
        listArray = [[NSMutableArray alloc] initWithArray:[FileManager readFavoritesFile]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.view.backgroundColor = [UIColor whiteColor];
    self.view.layer.cornerRadius = 6;
    self.view.layer.masksToBounds = YES;
    self.title = @"书签";
    
    UIBarButtonItem *lefItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"返 回", nil) style:UIBarButtonItemStylePlain target:self action:@selector(backToHome)];
    self.navigationItem.leftBarButtonItem = lefItem;

    CGRect frame = self.view.bounds;
    float height = frame.size.height - self.navigationController.navigationBar.frame.size.height;
    
    
    if (IsIOS7) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        frame.origin.y =self.navigationController.navigationBar.frame.size.height;
        height = height-20;
        frame.origin.y = frame.origin.y + 20;
    }
    else{
        lefItem.tintColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.9];
    }
    
    frame.size.height  = height;
    
    listTableView = [[UITableView alloc] initWithFrame:frame];
    listTableView.delegate = self;
    listTableView.dataSource = self;
    listTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:listTableView];

    if (listArray && listArray.count) {
        listTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    else{
        listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [listArray release];
    [listTableView release];
    [super dealloc];
}

- (void)editAction:(UIButton*)sender
{
    BOOL isSelect = sender.selected;
    
    if (isSelect) {
        isLocalBookMark = NO;
        titleLabel.text = NSLocalizedString(@"网络书签", nil) ;
    }
    else{
        isLocalBookMark = YES;
        titleLabel.text = NSLocalizedString(@"本地书签", nil) ;
    }
    
    sender.selected = !isSelect;
    
    
    [listTableView reloadData];
    
    
//    if (listTableView.editing) 
//        [listTableView setEditing:NO animated:YES];
//    else
//        [listTableView setEditing:YES animated:YES];

}

-(void)backToHome
{
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

- (void)accessoryButtonAction:(UIButton *)button
{
    if (isLocalBookMark) {
        if (button.tag >= 10) {
            int index = button.tag - 10;
            
            NSDictionary *dic = [listArray objectAtIndex:index];
        }
    }
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
//        UIButton *button = [ UIButton buttonWithType:UIButtonTypeCustom ];
//        CGRect frame = CGRectMake( 0.0 , 0.0 ,50 ,30 );
//        button. frame = frame;
//        //    [button setBackgroundImage:[UIImage imageNamed:@"save.png"] forState:UIControlStateNormal ];
////        [button setTitle:@"上传"  forState:UIControlStateNormal ];
//        button.titleLabel.textColor = [UIColor blackColor];
//        button. backgroundColor = [UIColor redColor ];
//        [button addTarget:self action:@selector(accessoryButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//        cell.accessoryView = button;
    }
    // Configure the cell...
    NSDictionary *dic = [listArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [dic objectForKey:@"bookName"];
//    if (isLocalBookMark) {
//        //本地
        cell.detailTextLabel.text = [dic objectForKey:@"pageName"];
//        UIButton *accessoryButton = (UIButton *)cell.accessoryView;
//        accessoryButton.tag = 10 + indexPath.row;
//        accessoryButton.hidden = NO;
//        [accessoryButton setTitle:@"上传"  forState:UIControlStateNormal ];
//    }
//    else{
//        //网络
//        cell.detailTextLabel.text = @"";
//        UIButton *accessoryButton = (UIButton *)cell.accessoryView;
//        accessoryButton.hidden = YES;
//        [accessoryButton setTitle:@""  forState:UIControlStateNormal ];
//    }
    
    return cell;
}


 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
     // Return NO if you do not want the specified item to be editable.
     return YES;
 }
 


 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
     if (editingStyle == UITableViewCellEditingStyleDelete) {
     // Delete the row from the data source
         
         NSMutableDictionary *dic = [listArray objectAtIndex:indexPath.row];
         
         if (dic) {
             [FileManager deleteFavorite:dic];
         }
         
         [listArray removeObjectAtIndex:indexPath.row];
         
         [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
     }
//     else if (editingStyle == UITableViewCellEditingStyleInsert) {
//     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//     }
 }
 

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dic = [listArray objectAtIndex:indexPath.row];
    
    ERWebViewController   *bookWebViewController = [[ERWebViewController alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:bookWebViewController animated:YES completion:^{
       
    
        
        [bookWebViewController loadWebView:[dic objectForKey:@"url"] withBookName:[dic objectForKey:@"bookName"] withInfo:[dic objectForKey:@"infos"]];
        
//        [bookWebViewController loadInfo:[dic objectForKey:@"infos"] withCateID:[[dic objectForKey:@"cateID"] intValue]];
    }];
    [bookWebViewController release];
    
}

@end
