//
//  ERReadViewController.m
//  EReader
//
//  Created by Hudajiang on 14-1-17.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import "ERReadViewController.h"
#import "NSObject+SBJson.h"
#import "InsetsLabel.h"
#import "ERSettingView.h"
#import <QuartzCore/QuartzCore.h>
#import "ERBook.h"
#import "ChromeProgressBar.h"
#import "FBPageScrollView.h"

#define kPageStartTag   10000

@interface ERReadViewController ()<UITableViewDataSource,UITableViewDelegate,ERSettingViewDelegate>
{
    NSUInteger      pageIndex;
    NSUInteger      totalPage;
    
    //目录模式
//    UITextView      *contextTextView;
    FBPageScrollView *pageScrollview;
    NSString        *contextStr;
    UIButton        *upButton;
    UIButton        *downButton;
    
    //其他模式
    UIScrollView    *pageScrollView;
    BOOL            isPageParseOver;
    
    
    // 工具栏
    UIView          *toolBar;
    BOOL            isToolHidden;
    UILabel         *readProessLabel;
    UISlider        *readProessView;
    UILabel         *_uiProessLabel;
    
    // 目录
    UIView          *directoryView;
    UITableView     *directoryTableView;
    NSMutableArray  *listArray;
    BOOL            isDirectoryViewOpen;
    UILabel         *progressLabel;

    //夜间模式
    BOOL            isNight;
    BOOL            isNightInDirectory;
    ERSettingView   *settingView;

    //by zym
    ERBook *erBook;
    ChromeProgressBar *chromeBar;

    UILabel *titleLabel;
    
    
    //日模式下的背景颜色
    UIColor     *dayReadBgColor;
}
@end

@implementation ERReadViewController
-(id)initWithBookObj:(EROfflineBookObj *)readBookObj
{
    self = [super initWithNibName:nil bundle:nil];
    
    if (self) {
        // Custom initialization
           bookObj = [readBookObj retain];
        
//        dayReadBgColor = [[UIColor alloc] init];
        
        pageIndex = 1;
        totalPage = 0;
        isToolHidden = NO;
        isDirectoryViewOpen = NO;
        isNight = NO;
        isPageParseOver = NO;
        
        currentParsingMode = ReadParsingModeOther;
        
        //阅读纪录
        NSString *path =  [bookObj bookReadRecordPath];
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
            NSDictionary *recordDic = [[NSDictionary alloc] initWithContentsOfFile:path];
            if (recordDic && recordDic.count) {
                id tempDic =[recordDic objectForKey:@"Read_Record_index"];
                if (tempDic) {
                    pageIndex = [((NSNumber*)tempDic) intValue];
                }
            }
            [recordDic release];
        }
        
//        NSString *catalogStr = [NSString stringWithContentsOfFile:[readBookObj bookCatalogPath] encoding:NSUTF8StringEncoding error:nil];
        contextStr = [[NSString alloc] initWithContentsOfFile:[readBookObj bookPath] encoding:NSUTF8StringEncoding error:nil];
        
        NSString *parten = @"第.{1,7}[章节回].*";
        
        NSRegularExpression *reg = [NSRegularExpression regularExpressionWithPattern:parten options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSArray* match = [reg matchesInString:contextStr options:NSMatchingReportCompletion range:NSMakeRange(0, [contextStr length])];
        
        NSMutableArray *matchArray = [[NSMutableArray alloc] init];
        
        if (match.count != 0)
        {
            for (NSTextCheckingResult *matc in match)
            {
                NSRange range = [matc range];
//                NSLog(@"%@",[contextStr substringWithRange:range]);
                [matchArray addObject:[contextStr substringWithRange:range]];
            }
        }
        
        listArray = [[NSMutableArray alloc] initWithArray:matchArray];
//        listArray = [[NSMutableArray alloc] initWithArray:[catalogStr JSONValue]];
        if (listArray && listArray.count) {
            currentParsingMode = ReadParsingModeDirectory;
            
        }
        
        [matchArray release];
    }
 
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor =[UIColor colorWithRed:250/255.0 green:250/255.0  blue:250/255.0  alpha:1];
    
    //toolBar
    toolBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 44+(IsIOS7?20:0))];
    [self.view addSubview:toolBar];
    toolBar.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
    
    
    [self loadViewForMode];
}
-(void)loadViewForMode
{

    [toolBar.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 44+(IsIOS7?20:0)-1, self.view.bounds.size.width, 1.0f)];
    lineView.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.8];
    [toolBar addSubview:lineView];
    [lineView release];
    
    
    UIButton *item1 = [[UIButton alloc] initWithFrame:CGRectMake(0, (IsIOS7?20:0), 60, 44)];
    [item1 setTitleColor:[UIColor colorWithRed:51.0f/255.0f green:153.0f/255.0f blue:255.0f/255.0f alpha:1] forState:UIControlStateNormal];
    [item1 setTitle:@"返回" forState:UIControlStateNormal];
    [item1 addTarget:self action:@selector(doBackView) forControlEvents:UIControlEventTouchUpInside];
    [toolBar addSubview:item1];
    [item1 release];
    
    
    titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, (IsIOS7?20:0), 150, 44)] autorelease];
    [toolBar addSubview:titleLabel];
    titleLabel.text = bookObj.title;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    
    if (currentParsingMode == ReadParsingModeDirectory) {
        titleLabel.frame = CGRectMake(60, titleLabel.frame.origin.y, 140, 44);
        UIButton *item2 = [[UIButton alloc] initWithFrame:CGRectMake(200, (IsIOS7?20:0), 50, 44)];
        [item2 setTitleColor:[UIColor colorWithRed:51.0f/255.0f green:153.0f/255.0f blue:255.0f/255.0f alpha:1] forState:UIControlStateNormal];//[UIColor colorWithRed:3/255.0 green:3/255.0 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
        [item2 setTitle:@"选项" forState:UIControlStateNormal];
        [item2 addTarget:self action:@selector(doSetting) forControlEvents:UIControlEventTouchUpInside];
        [toolBar addSubview:item2];
        [item2 release];
        
        
        UIButton *item3 = [[UIButton alloc] initWithFrame:CGRectMake(260, (IsIOS7?20:0), 50, 44)];
        [item3 setTitleColor:[UIColor colorWithRed:51.0f/255.0f green:153.0f/255.0f blue:255.0f/255.0f alpha:1] forState:UIControlStateNormal];
        [item3 setTitle:@"目录" forState:UIControlStateNormal];
        [item3 addTarget:self action:@selector(doDirectory) forControlEvents:UIControlEventTouchUpInside];
        [toolBar addSubview:item3];
        [item3 release];
    }
    else{
        titleLabel.frame = CGRectMake(60, titleLabel.frame.origin.y, 200, 44);
        UIButton *item2 = [[UIButton alloc] initWithFrame:CGRectMake(260, (IsIOS7?20:0), 50, 44)];
        [item2 setTitleColor:[UIColor colorWithRed:51.0f/255.0f green:153.0f/255.0f blue:255.0f/255.0f alpha:1] forState:UIControlStateNormal];
        [item2 setTitle:@"选项" forState:UIControlStateNormal];
        [item2 addTarget:self action:@selector(doSetting) forControlEvents:UIControlEventTouchUpInside];
        [toolBar addSubview:item2];
        [item2 release];
    }
//    [contextTextView removeFromSuperview];
    [directoryView removeFromSuperview];
    [directoryTableView removeFromSuperview];
   
     ObjRelease(directoryTableView);
     ObjRelease(directoryView);
//    ObjRelease(contextTextView);
    
//    [contextTextView removeFromSuperview];
//    ObjRelease(contextTextView);
    ObjRelease(erBook);
    
    switch (currentParsingMode) {
        case ReadParsingModeDirectory:
        {
            [self performSelector:@selector(doToolBar) withObject:nil afterDelay:3.0];
            
//            contextTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, IsIOS7?20:0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-(IsIOS7?20:0))];
//            contextTextView.delegate = (id)self;
//            contextTextView.editable = NO;
//            
//            if (IsIOS7) {
//                contextTextView.textContainerInset =UIEdgeInsetsMake(0, 10, 0, 10);
//            }
//            else{
//                contextTextView.frame = CGRectInset(contextTextView.frame, 10, 0);
//            }
//            
//            contextTextView.delegate = (id)self;
//            contextTextView.editable = NO;
//            contextTextView.backgroundColor = [UIColor clearColor];
//            
//            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"font"]) {
//                float fontSize =[[[NSUserDefaults standardUserDefaults] objectForKey:@"font"] floatValue];
//                contextTextView.font = [UIFont systemFontOfSize:fontSize];
//            }
//            else{
//                contextTextView.font = FONT;
//            }
//            
//            [self.view insertSubview:contextTextView belowSubview:toolBar];
            
            //上下章节按钮
            upButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 60)];
            upButton.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
            [upButton setTitle:@"上一章" forState:UIControlStateNormal];
            upButton.center = CGPointMake(CGRectGetWidth(upButton.bounds)*0.5-1, CGRectGetHeight(self.view.bounds)*0.5);
            [self.view addSubview:upButton];
            upButton.layer.borderWidth =1;
            upButton.layer.borderColor =[UIColor grayColor].CGColor;
            [upButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [upButton addTarget:self action:@selector(preAction:) forControlEvents:UIControlEventTouchUpInside];
            [upButton release];
            
            downButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 60)];
            downButton.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
            [downButton setTitle:@"下一章" forState:UIControlStateNormal];
            downButton.layer.borderWidth =1;
            downButton.layer.borderColor =[UIColor grayColor].CGColor;
            [downButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            downButton.center = CGPointMake(CGRectGetWidth(self.view.bounds) - CGRectGetWidth(upButton.bounds)*0.5+1, CGRectGetHeight(self.view.bounds)*0.5);
            [self.view addSubview:downButton];
            [downButton addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
            [downButton release];
            
//            contextTextView.decelerationRate = 0.1;
            
            progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, CGRectGetHeight(self.view.frame) - 20, 290, 20)];
            progressLabel.font = [UIFont systemFontOfSize:13.0];
            progressLabel.backgroundColor = [UIColor clearColor];
            progressLabel.textAlignment = NSTextAlignmentRight;
            [self.view insertSubview:progressLabel belowSubview:toolBar];
            
            [self loadNextContentWithPage:pageIndex];


            
//            UITapGestureRecognizer *tapDirectory = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToolAction:)];
//            [self.view addGestureRecognizer:tapDirectory];
//            [tapDirectory release];
//            
            // 目录
            directoryView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
            directoryView.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
            [self.view addSubview:directoryView];
            
            UIView *dirLineView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 44+(IsIOS7?20:0)-1, self.view.bounds.size.width, 1.0f)];
            dirLineView.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.8];
            [directoryView addSubview:dirLineView];
            [dirLineView release];
            
            UIButton *directoryCloseBtn = [[UIButton alloc] initWithFrame:CGRectMake(5, (IsIOS7?20:0), 60, 44)];
            [directoryCloseBtn setTitleColor:[UIColor colorWithRed:51.0f/255.0f green:153.0f/255.0f blue:255.0f/255.0f alpha:1] forState:UIControlStateNormal];
            [directoryView addSubview:directoryCloseBtn];
            [directoryCloseBtn setTitle:@"关闭" forState:UIControlStateNormal];
            [directoryCloseBtn addTarget:self action:@selector(doDirectory) forControlEvents:UIControlEventTouchUpInside];
            [directoryCloseBtn release];
            
            
            UIButton *directoryChangeMode = [[UIButton alloc] initWithFrame:CGRectMake(230, (IsIOS7?20:0), 80, 44)];
            [directoryChangeMode setTitleColor:[UIColor colorWithRed:51.0f/255.0f green:153.0f/255.0f blue:255.0f/255.0f alpha:1] forState:UIControlStateNormal];
            [directoryView addSubview:directoryChangeMode];
            [directoryChangeMode setTitle:@"目录错误" forState:UIControlStateNormal];
            [directoryChangeMode addTarget:self action:@selector(directoryChangeMode) forControlEvents:UIControlEventTouchUpInside];
            [directoryChangeMode release];
            
            
            directoryTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 44+(IsIOS7?20:0), CGRectGetWidth(directoryView.frame), CGRectGetHeight(directoryView.frame)-44-(IsIOS7?20:0))];
            [directoryView addSubview:directoryTableView];
            directoryTableView.delegate = self;
            directoryTableView.dataSource = self;
            
        }
            break;
        case ReadParsingModeOther:
        {
            
            pageScrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
            pageScrollView.showsHorizontalScrollIndicator = NO;
            pageScrollView.showsVerticalScrollIndicator = NO;
            pageScrollView.delegate = self;
            pageScrollView.pagingEnabled = YES;
            pageScrollView.backgroundColor = [UIColor whiteColor];
            [self.view insertSubview:pageScrollView belowSubview:toolBar];
            
            UITapGestureRecognizer *tapDirectory = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToolAction:)];
            [pageScrollView addGestureRecognizer:tapDirectory];
            [tapDirectory release];
            
            
            chromeBar = [[[ChromeProgressBar alloc] initWithFrame:CGRectMake(0.0f, 44+(IsIOS7?20:0)-2, self.view.bounds.size.width, 2.0f)] autorelease];
            [toolBar addSubview:chromeBar];
            
            
            erBook = [[ERBook alloc] init];
            erBook.delegate = (id)self;
            
            
            [erBook parserBookWithBookObj:bookObj withBounds:CGRectInset(pageScrollView.bounds, 15, 25) withfinishBlock:^{
                //解析完成，加载进度条
                isPageParseOver = YES;
                [self performSelector:@selector(doToolBar) withObject:nil afterDelay:1.0];
                
                readProessLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetHeight(self.view.frame)-20, 310, 20)];
                [self.view addSubview:readProessLabel];
                readProessLabel.hidden = YES;
                readProessLabel.text = @"";
                if (isNight) {
                    readProessLabel.textColor = [UIColor whiteColor];
                }
                else{
                    readProessLabel.textColor = [UIColor blackColor];
                }
                readProessLabel.textAlignment = NSTextAlignmentRight;
                readProessLabel.font = [UIFont systemFontOfSize:12.0];
                readProessLabel.backgroundColor = [UIColor clearColor];
                
                
                readProessView = [[UISlider alloc] initWithFrame:CGRectMake(10, IsIOS7?(CGRectGetHeight(self.view.frame)-40):(CGRectGetHeight(self.view.frame)-45), CGRectGetWidth(self.view.frame)-20, 20)];
                
                [self.view addSubview:readProessView];
                readProessView.hidden = YES;
                [readProessView addTarget:self action:@selector(readProessAction:) forControlEvents:UIControlEventTouchUpInside];
                [readProessView addTarget:self action:@selector(readProessValueChanged:) forControlEvents:UIControlEventValueChanged];
                readProessView.backgroundColor = [UIColor clearColor];
                readProessView.value = 0.00;
                
                //设置label的属性
                _uiProessLabel = [[UILabel alloc]initWithFrame:CGRectMake(readProessView.frame.origin.x, readProessView.frame.origin.y-10, 80, 20)];
                [_uiProessLabel setTextAlignment:NSTextAlignmentCenter];
                _uiProessLabel.font = [UIFont systemFontOfSize:14.0];
                _uiProessLabel.textColor = [UIColor whiteColor];
                [_uiProessLabel setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.9]];
                //表示一开始为隐藏，不显示label，如果不用下面这句一开始进去是不显示label
                [_uiProessLabel setAlpha:0.f];
                
                [self.view addSubview:_uiProessLabel];
                
                
                totalPage = erBook.pageRanges.count;
                
                readProessLabel.hidden = NO;
                readProessLabel.text = [NSString stringWithFormat:@"第%i/%i页",pageIndex,totalPage ];
                [readProessView setMinimumValue:1.0];
                [readProessView setMaximumValue:totalPage];
                readProessView.hidden = toolBar.hidden;
                readProessView.value = pageIndex;
                [pageScrollView setContentSize:CGSizeMake(erBook.pageRanges.count * pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
                [pageScrollView setContentOffset:CGPointMake((pageIndex-1)*CGRectGetWidth(pageScrollView.bounds), 0)];
                [self firstLoadPageWithIndex:pageIndex-1];
                
            }];
            
            
            double delayInSeconds = 0.1;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                if(pageIndex < erBook.pageRanges.count)
                {
                    [pageScrollView setContentSize:CGSizeMake(erBook.pageRanges.count * pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
                    [pageScrollView setContentOffset:CGPointMake((pageIndex-1)*CGRectGetWidth(pageScrollView.bounds), 0)];
                    [self firstLoadPageWithIndex:pageIndex-1];
                }
            });
            
            
        }
            break;
        default:
            break;
    }
    
    settingView = [[[[NSBundle mainBundle] loadNibNamed:@"settingView" owner:nil options:nil] lastObject] retain];
    settingView.settingDelegate = self;
    [settingView readCache];
    if (currentParsingMode == ReadParsingModeDirectory) {
        settingView.fontSegment.hidden = NO;
    }
    else{
        settingView.fontSegment.hidden = YES;
        settingView.frame = CGRectMake(settingView.frame.origin.x, settingView.frame.origin.y, settingView.frame.size.width, settingView.frame.size.width - 80);
    }
    settingView.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    settingView.hidden = YES;

}
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
//{
//    if ([[[[touch view] superview] class] isSubclassOfClass:[UITableViewCell class]]) {
//        return NO;
//    }
//    return YES;
//}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex) {
        currentParsingMode = ReadParsingModeOther;
        ObjRelease(listArray);
        [[NSFileManager defaultManager] removeItemAtPath:[bookObj bookCatalogPath] error:nil];
        
        if (upButton)
        {
            [upButton removeFromSuperview];
        }
        ObjRelease(upButton);
        if (downButton)
        {
            [downButton removeFromSuperview];
        }
        ObjRelease(downButton);

        [self loadViewForMode];
    }
}
-(void)directoryChangeMode
{
    UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"确定目录和内容无法匹配" message:@"确定后将不能使用目录功能" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    [alert release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    if (isNight) {
        return UIStatusBarStyleLightContent;
    }
    else{
        return UIStatusBarStyleDefault;
    }
}
- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)dealloc
{
    ObjRelease(dayReadBgColor);
    
    ObjRelease(pageScrollView);
    settingView.settingDelegate = nil;
    ObjRelease(settingView);

    ObjRelease(bookObj);
  
    ObjRelease(erBook);
    
    ObjRelease(readProessLabel);
    ObjRelease(readProessView);
    ObjRelease(toolBar);

    ObjRelease(contextStr);
    ObjRelease(directoryView);
    ObjRelease(directoryTableView);
    ObjRelease(listArray);

    [super dealloc];
}

#pragma mark -设置选项 Action
- (void)colorAction:(id)sender
{
    isNight = NO;
    UIButton *btn = (UIButton *)sender;
    ObjRelease(dayReadBgColor);
    dayReadBgColor = [btn.backgroundColor retain];
    if (currentParsingMode ==ReadParsingModeDirectory) {
        self.view.backgroundColor = btn.backgroundColor;
        pageScrollview.contentView.textColor = [UIColor blackColor];
        progressLabel.textColor = [UIColor blackColor];
    }
    else{
        
        for (UIView *temp in pageScrollView.subviews) {
            if ([[temp class] isSubclassOfClass:[InsetsLabel class]]) {
                InsetsLabel *label = (InsetsLabel *)temp;
                [label refDisplay];
            }
        }
        pageScrollView.backgroundColor = btn.backgroundColor;
        readProessLabel.textColor = [UIColor blackColor];

    }


    
    toolBar.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
    if (IsIOS7) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)fontAction:(id)sender
{
    UISegmentedControl *segment = (UISegmentedControl *)sender;
    

    UIFont *font =FONT;
    switch (segment.selectedSegmentIndex) {
        case 0:
        {
            font =[UIFont systemFontOfSize:16];
            
        }
            break;
        case 1:
        {
                font =FONT;
        }
            break;
        case 2:
        {
             font =[UIFont systemFontOfSize:20];

        }
            break;
            
        default:
            break;
    }
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithFloat:font.pointSize] forKey:@"font"];
    if (currentParsingMode == ReadParsingModeDirectory)
    {
    
        [pageScrollview setContentFont:font];

        
        [pageScrollview reloadData];
        
//        [pageScrollview setContentSize:CGSizeMake(CGRectGetWidth(pageScrollview.frame), CGRectGetHeight(pageScrollview.pageView.frame))];
//        [UIFont systemFontOfSize:20];
    }
    else
    {
    }


}

- (void)nightAction:(id)sender
{
    UISegmentedControl *segment = (UISegmentedControl *)sender;
    switch (segment.selectedSegmentIndex) {
        case 0:
        {
            //日
            isNight = NO;
            if (currentParsingMode == ReadParsingModeDirectory) {
                if (dayReadBgColor) {
                    self.view.backgroundColor =dayReadBgColor;
                }
                else{
                    self.view.backgroundColor =[UIColor colorWithRed:250/255.0 green:250/255.0  blue:250/255.0  alpha:1];
                }
                
                pageScrollview.contentView.textColor = [UIColor blackColor];
                progressLabel.textColor = [UIColor blackColor];
                
                upButton.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
                [upButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                upButton.layer.borderColor = [UIColor grayColor].CGColor;
                
                downButton.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
                downButton.layer.borderColor = [UIColor grayColor].CGColor;
                [downButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            else{
                for (UIView *temp in pageScrollView.subviews) {
                    if ([[temp class] isSubclassOfClass:[InsetsLabel class]]) {
                        InsetsLabel *label = (InsetsLabel *)temp;
                        [label refDisplay];
                    }
                }
                pageScrollView.backgroundColor = [UIColor colorWithRed:250/255.0 green:250/255.0  blue:250/255.0  alpha:1];
                readProessLabel.textColor = [UIColor blackColor];
            }
            
            toolBar.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];

            if (IsIOS7) {
                [self setNeedsStatusBarAppearanceUpdate];
            }
            
        }
            break;
        case 1:
        {
            //夜
            isNight = YES;
            if (currentParsingMode == ReadParsingModeDirectory) {
                self.view.backgroundColor = [UIColor colorWithRed:50/255.0 green:50/255.0  blue:50/255.0  alpha:1];
                pageScrollview.contentView.textColor = [UIColor whiteColor];
                progressLabel.textColor = [UIColor whiteColor];
                
                upButton.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1];
                [upButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                upButton.layer.borderColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.8].CGColor;
                downButton.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1];
                [downButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                downButton.layer.borderColor =  [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.8].CGColor;
            }
            else{
                for (UIView *temp in pageScrollView.subviews) {
                    if ([[temp class] isSubclassOfClass:[InsetsLabel class]]) {
                        InsetsLabel *label = (InsetsLabel *)temp;
                        [label refDisplay];
                    }
                }
                pageScrollView.backgroundColor = [UIColor colorWithRed:50/255.0 green:50/255.0  blue:50/255.0  alpha:1];
                readProessLabel.textColor = [UIColor whiteColor];
            }
            
            toolBar.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
            if (IsIOS7) {
                [self setNeedsStatusBarAppearanceUpdate];
            }
 
        }
            break;
            
        default:
            break;
    }
    
}

- (void)lightAction:(id)sender
{
    UISlider *slider = (UISlider *)sender;
    [[UIScreen mainScreen] setBrightness:slider.value];
}


-(void)ERBookPageParser:(int)currentParserIndex percentage:(float) percentage
{
    totalPage = currentParserIndex+1;
    chromeBar.progress =percentage;
    
    [pageScrollView setContentSize:CGSizeMake(totalPage * pageScrollView.frame.size.width, pageScrollView.frame.size.height)];

    if(currentParserIndex==pageIndex-1)
    {
        [pageScrollView setContentOffset:CGPointMake((pageIndex-1)*CGRectGetWidth(pageScrollView.bounds), 0)];
        [self firstLoadPageWithIndex:pageIndex-1];
    }
}

#pragma mark - 通用touchAction
-(void)stopanimate
{
    [UIView animateWithDuration:0.5
                     animations:^{
                         [_uiProessLabel setAlpha:0.f];
                     }
                     completion:^(BOOL finished){
                         // 动画结束时的处理
                     }];
}

-(void)readProessValueChanged:(id)sender
{
    UIImageView *imageView = IsIOS7?[readProessView.subviews objectAtIndex:1]:[readProessView.subviews objectAtIndex:2];
    CGRect theRect = [self.view convertRect:imageView.frame fromView:imageView.superview];
    [_uiProessLabel setFrame:CGRectMake(theRect.origin.x-22, theRect.origin.y-38, _uiProessLabel.frame.size.width, _uiProessLabel.frame.size.height)];
    
    NSInteger v = readProessView.value+0.5;
    //label的显示数字。为滑动条移动后的位置的value
    [_uiProessLabel setText:[NSString stringWithFormat:@"%d",v]];
    
    //动画效果
    [_uiProessLabel setAlpha:1.f];
}

- (void)readProessAction:(UISlider *)slider
{
    NSUInteger page = slider.value;
    
    if (page <= totalPage) {
        pageIndex = page;
        
        [pageScrollView setContentOffset:CGPointMake((pageIndex-1)*CGRectGetWidth(pageScrollView.bounds), 0)];
        
        [self firstLoadPageWithIndex:pageIndex-1];
        
        readProessLabel.text = [NSString stringWithFormat:@"第%i/%i页",pageIndex,totalPage ];
        
        
        [self stopanimate];
        
        
        NSString *path =  [bookObj bookReadRecordPath];
        NSMutableDictionary *recordDic = [[NSMutableDictionary alloc] init];
        [recordDic setObject:[NSNumber numberWithInt:pageIndex] forKey:@"Read_Record_index"];
        [recordDic setObject:bookObj.title forKey:@"Read_Record_name"];
        [recordDic setObject:bookObj.gId forKey:@"Read_Record_gid"];
        [recordDic writeToFile:path atomically:YES];
        [recordDic release];
    }
}

//设置
- (void)doSetting
{
    UIView *settingBGView = [[UIView alloc] initWithFrame:self.view.bounds];
    settingBGView.userInteractionEnabled = YES;
    settingBGView.backgroundColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.3];
    settingBGView.tag = 110;
    [self.view addSubview:settingBGView];
    [self.view bringSubviewToFront:settingBGView];

    if (!settingView) {
    
        settingView = [[[[NSBundle mainBundle] loadNibNamed:@"settingView" owner:nil options:nil] lastObject] retain];
        settingView.settingDelegate = self;
        if (currentParsingMode == ReadParsingModeDirectory) {
            settingView.fontSegment.hidden = NO;
        }
        else{
            settingView.fontSegment.hidden = YES;
        }
        settingView.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    }
    [settingBGView addSubview:settingView];
    settingView.hidden = NO;
    settingView.alpha = 0;
    settingView.transform = CGAffineTransformMakeScale(0.8, 0.8);
    
    [UIView animateWithDuration:0.25 animations:^{
        
        settingView.transform = CGAffineTransformMakeScale(1.1, 1.1);
         settingView.alpha = 1;
       
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            settingView.transform = CGAffineTransformIdentity;
           
        }];
    }];
    
    [settingBGView release];
}

//目录
- (void)doDirectory
{
    if (isDirectoryViewOpen) {
        isNight = isNightInDirectory;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.4];
        directoryView.frame = CGRectMake(CGRectGetWidth(directoryView.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(directoryView.frame));
        [UIView commitAnimations];
        if (IsIOS7) {
            [self setNeedsStatusBarAppearanceUpdate];
        }
    }
    else{
        isNightInDirectory = isNight;
        isNight = NO;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.4];
        directoryView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(directoryView.frame));
        [directoryTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:pageIndex-1 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        [directoryTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:pageIndex-1 inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:NO];
        [UIView commitAnimations];
        if (IsIOS7) {
            [self setNeedsStatusBarAppearanceUpdate];
        }
    }
    isDirectoryViewOpen = !isDirectoryViewOpen;
}

 
//返回
- (void)doBackView
{
    NSString *path =  [bookObj bookReadRecordPath];
    NSMutableDictionary *recordDic = [[NSMutableDictionary alloc] init];
    [recordDic setObject:[NSNumber numberWithInt:pageIndex] forKey:@"Read_Record_index"];
    [recordDic setObject:bookObj.title forKey:@"Read_Record_name"];
    [recordDic setObject:bookObj.gId forKey:@"Read_Record_gid"];
    [recordDic writeToFile:path atomically:YES];
    [recordDic release];
    [self dismissViewControllerAnimated:YES completion:Nil];
    
    
      [erBook parserCancel];
}



//中间工具栏
-(void)doToolBar
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    isToolHidden = !isToolHidden;
    
    if (isToolHidden) {
        //隐藏
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.25];
        toolBar.frame =CGRectMake(0, -(44+(IsIOS7?20:0)), CGRectGetWidth(self.view.frame), (44+(IsIOS7?20:0)));
        
        if (currentParsingMode == ReadParsingModeDirectory) {
            upButton.center = CGPointMake(-CGRectGetWidth(upButton.bounds)*0.5-1, CGRectGetHeight(self.view.bounds)*0.5);
            downButton.center = CGPointMake(CGRectGetWidth(self.view.bounds) + CGRectGetWidth(upButton.bounds)*0.5+1, CGRectGetHeight(self.view.bounds)*0.5);
        }
        else
        {
            if (isPageParseOver) {
                readProessView.hidden = YES;
            }
            else
            {
                readProessView.hidden = YES;
            }
        }
        [UIView commitAnimations];
    }
    else{
        //显示
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.25];
        toolBar.frame =CGRectMake(0, 0, CGRectGetWidth(self.view.frame), (44+(IsIOS7?20:0)));
        if (currentParsingMode == ReadParsingModeDirectory) {
            upButton.center = CGPointMake(CGRectGetWidth(upButton.bounds)*0.5-1, CGRectGetHeight(self.view.bounds)*0.5);
            downButton.center = CGPointMake(CGRectGetWidth(self.view.bounds) - CGRectGetWidth(upButton.bounds)*0.5+1, CGRectGetHeight(self.view.bounds)*0.5);
        }
        else
        {
            if (isPageParseOver) {
                readProessView.hidden = NO;
            }
            else{
                readProessView.hidden = YES;
            }
            
        }
        [UIView commitAnimations];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    switch (currentParsingMode) {
        case ReadParsingModeDirectory:
        {
            UIView *tempView = [[touches anyObject] view];
            if (tempView.tag ==110){

                [UIView animateWithDuration:0.3 animations:^{
                    settingView.alpha =0;
                    settingView.transform = CGAffineTransformMakeScale(0.8, 0.8);
                } completion:^(BOOL finished) {
                    settingView.hidden = YES;
                    settingView.transform = CGAffineTransformIdentity;
                    [tempView removeFromSuperview];
                }];
            }
            else if (![[tempView class] isSubclassOfClass:[ERSettingView class]]){
                //中间
                [self doToolBar];
            }

        }
            break;
        case ReadParsingModeOther:
        {
            UITouch * currentTouch =[touches anyObject];
            CGPoint currentPoint = [currentTouch locationInView:self.view];
            float pointX = currentPoint.x;
            
            UIView *tempView = [[touches anyObject] view];
            
            if (tempView.tag ==110){
                
                [UIView animateWithDuration:0.3 animations:^{
                    settingView.alpha =0;
                    settingView.transform = CGAffineTransformMakeScale(0.8, 0.8);
                } completion:^(BOOL finished) {
                    settingView.hidden = YES;
                    settingView.transform = CGAffineTransformIdentity;
                    [tempView removeFromSuperview];
                }];
            }
            else if ([[tempView class] isSubclassOfClass:[ERSettingView class]]){
                //中间
//                [self doToolBar];
            }
            else if (pointX>=0 && pointX < CGRectGetWidth(self.view.frame)/3) {
                //左
//                [self doPre];
            }
            else if (pointX<=CGRectGetWidth(self.view.frame) && pointX > CGRectGetWidth(self.view.frame)/3*2){
                // 右
//                [self doNext];
            }
            else{
                //中间
                [self doToolBar];
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - ReadParsingModeDirectory
- (void)tapToolAction:(UITapGestureRecognizer *)tap
{
    if (tap.state == UIGestureRecognizerStateEnded) {
        
        if ([[tap.view class] isSubclassOfClass:[UITextView class]] || [[tap.view class] isSubclassOfClass:[UIScrollView class]]) {
            //中间
            [self doToolBar];
        }
    

    }
    


}


- (void)preAction:(UIButton *)sender
{
    [self loadPrexContentWithPage:pageIndex-1];
    isToolHidden = NO;
    [self doToolBar];
}

- (void)nextAction:(UIButton *)sender
{
    [self loadNextContentWithPage:pageIndex+1];
    isToolHidden = NO;
    [self doToolBar];
}

- (void)loadChapterPageView:(NSInteger)_pageIndex withTitle:(NSString *)titleStr withContent:(NSString *)pageContentStr
{
//    if(_pageIndex<0 || _pageIndex>=totalPage)return;


    if (pageScrollview == nil) {
        pageScrollview = [[[NSBundle mainBundle] loadNibNamed:@"FBPageScrollView" owner:self options:nil] lastObject];
        pageScrollview.frame = CGRectMake(0, IsIOS7?20:0, CGRectGetWidth(pageScrollview.frame), CGRectGetHeight(pageScrollview.frame)-(IsIOS7?20:0)*2.0);
        pageScrollview.delegate = self;
        pageScrollview.pageDelegate = (id)self;
       [self.view insertSubview:pageScrollview belowSubview:progressLabel];

    }
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:titleStr,@"title",
                         [NSNumber numberWithInteger:_pageIndex+1],@"pageIndex",
                         [NSNumber numberWithInteger:listArray.count],@"totalPage",
                         nil];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"font"]) {
        float fontSize =[[[NSUserDefaults standardUserDefaults] objectForKey:@"font"] floatValue];
        [pageScrollview setContentFont:[UIFont systemFontOfSize:fontSize]];
    }
    else{
        [pageScrollview setContentFont:FONT];
    }
    
    
    [pageScrollview bindData:dic Content:pageContentStr];
    
    progressLabel.text = [NSString stringWithFormat:@"%i/%i",_pageIndex,listArray.count];
    
//    [pageScrollview setContentSize:CGSizeMake(CGRectGetWidth(pageScrollview.frame), CGRectGetHeight(pageScrollview.pageView.frame))];
    [pageScrollview setContentOffset:CGPointMake(0, 0)];
    if (settingView) {
        [settingView readCache];
    }
    
}

// 下章节
- (void)loadNextContentWithPage:(int)index
{
    if (listArray && listArray.count) {
        
        if ((index-1) <listArray.count) {
            pageIndex = index;
            
//            NSDictionary *currentDic = [listArray objectAtIndex:index-1];
//            NSString *currentTitleKey = [currentDic objectForKey:@"text"];
             NSString *currentTitleKey = [listArray objectAtIndex:index-1];
//            titleLabel.text = currentTitleKey;
            NSRange range = [contextStr rangeOfString:currentTitleKey];
            if (range.location != NSNotFound) {
                if (index == listArray.count) {
                    //最后一章
                    NSString *sectionStr = [contextStr substringFromIndex:range.location];
//                    contextTextView.text = sectionStr;
                    
                    [self loadChapterPageView:pageIndex withTitle:currentTitleKey withContent:sectionStr];
                }
                else{
//                    NSDictionary *nextDic = [listArray objectAtIndex:index];
//                    NSString *nextTitleKey = [nextDic objectForKey:@"text"];

                    NSString *nextTitleKey = [listArray objectAtIndex:index];
                    
                    NSRange searchRange;
                    searchRange.location =range.location;
                    searchRange.length = contextStr.length - range.location;
                    
                    NSRange nextRange = [contextStr rangeOfString:nextTitleKey options:NSCaseInsensitiveSearch range:searchRange];
                    
                    NSRange newRange;
                    newRange.location = range.location;
                    newRange.length = nextRange.location-range.location;
                    NSString *sectionStr = [contextStr substringWithRange:newRange];
//                    contextTextView.text = sectionStr;
                    [self loadChapterPageView:pageIndex withTitle:currentTitleKey withContent:sectionStr];
                }
            }
            else{
//                contextTextView.text = currentTitleKey;
            }
           
//            contextTextView.contentOffset = CGPointMake(0, 0);
            
            NSString *path =  [bookObj bookReadRecordPath];
            NSMutableDictionary *recordDic = [[NSMutableDictionary alloc] init];
            [recordDic setObject:[NSNumber numberWithInt:index] forKey:@"Read_Record_index"];
            [recordDic setObject:bookObj.title forKey:@"Read_Record_name"];
            [recordDic setObject:bookObj.gId forKey:@"Read_Record_gid"];
            [recordDic writeToFile:path atomically:YES];
            [recordDic release];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"已经到底了!" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertView show];
            [alertView release];
        }
    }
}

//上章节
- (void)loadPrexContentWithPage:(int)index
{
    if (listArray && listArray.count) {
        
        if ((index-1) >= 0) {
            pageIndex = index;
            
//            NSDictionary *currentDic = [listArray objectAtIndex:index-1];
//            NSString *currentTitleKey = [currentDic objectForKey:@"text"];
            
            NSString *currentTitleKey = [listArray objectAtIndex:index-1];
            titleLabel.text = currentTitleKey;
            NSRange range = [contextStr rangeOfString:currentTitleKey];
            if (range.location != NSNotFound) {
                if (index == listArray.count) {
                    //最后一章
                    NSString *sectionStr = [contextStr substringFromIndex:range.location];
//                    contextTextView.text = sectionStr;
                    [self loadChapterPageView:pageIndex withTitle:currentTitleKey withContent:sectionStr];
                }
                else{
//                    NSDictionary *nextDic = [listArray objectAtIndex:index];
//                    NSString *nextTitleKey = [nextDic objectForKey:@"text"];
                    
                    NSString *nextTitleKey = [listArray objectAtIndex:index];
                    
                    NSRange searchRange;
                    searchRange.location =range.location;
                    searchRange.length = contextStr.length - range.location;
                    
                    NSRange nextRange = [contextStr rangeOfString:nextTitleKey options:NSCaseInsensitiveSearch range:searchRange];
                    
                    
                    NSRange newRange;
                    newRange.location = range.location;
                    newRange.length = nextRange.location-range.location;
                    NSString *sectionStr = [contextStr substringWithRange:newRange];
//                    contextTextView.text = sectionStr;
                    [self loadChapterPageView:pageIndex withTitle:currentTitleKey withContent:sectionStr];
                }
            }
            else{
//                contextTextView.text = currentTitleKey;
            }

            NSString *path = [bookObj bookReadRecordPath];
            NSMutableDictionary *recordDic = [[NSMutableDictionary alloc] init];
            [recordDic setObject:[NSNumber numberWithInt:index] forKey:@"Read_Record_index"];
            [recordDic setObject:bookObj.title forKey:@"Read_Record_name"];
            [recordDic setObject:bookObj.gId forKey:@"Read_Record_gid"];
            [recordDic writeToFile:path atomically:YES];
            [recordDic release];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"已经到顶了!" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertView show];
            [alertView release];
        }
    }
    
}



#pragma mark - ReadParsingModeOther
#pragma mark -touch


#pragma mark -KDBookDelegate


#pragma mark - 工具栏
#pragma mark -Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.font = [UIFont systemFontOfSize:12.0];
        cell.selectionStyle =UITableViewCellSelectionStyleGray;
    }
    // Configure the cell...
    
    if (listArray && listArray.count) {
//        NSDictionary *dic = [listArray objectAtIndex:indexPath.row];
//        cell.textLabel.text = [dic objectForKey:@"text"];
        cell.textLabel.text = [listArray objectAtIndex:indexPath.row];
    }
    
    return cell;
}

#pragma mark -Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self loadNextContentWithPage:indexPath.row+1];
    [self doDirectory];
}

#pragma mark -Scroll view delegate
- (void)firstLoadPageWithIndex:(int)_pageIndex
{
    for (UIView *temp in pageScrollView.subviews) {
        if ([[temp class] isSubclassOfClass:[InsetsLabel class]]) {
            [temp removeFromSuperview];
        }
    }
    [self loadPageWithIndex:_pageIndex-1];
    [self loadPageWithIndex:_pageIndex];
    [self loadPageWithIndex:_pageIndex+1];
    [self removePageViewWithIndex:_pageIndex-2];
    [self removePageViewWithIndex:_pageIndex+2];
}


-(void)removePageViewWithIndex:(int)_pageIndex
{
    if(_pageIndex<0 || _pageIndex>=totalPage)return;
    int tag = kPageStartTag +_pageIndex;
    InsetsLabel *pageLabel = (InsetsLabel *)[pageScrollView viewWithTag:tag];
    if(pageLabel)
    {
        [pageLabel removeFromSuperview];
    }
}

-(void)loadPageWithIndex:(int)_pageIndex
{
    if(_pageIndex<0 || _pageIndex>=totalPage)return;
    int tag = kPageStartTag +_pageIndex;
    InsetsLabel *pageLabel = (InsetsLabel *)[pageScrollView viewWithTag:tag];
    if(pageLabel == nil)
    {
        pageLabel = [[InsetsLabel alloc] initWithFrame:CGRectMake(_pageIndex*pageScrollView.frame.size.width , 0,pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
        pageLabel.tag = tag;
        pageLabel.backgroundColor = [UIColor clearColor];
        [pageScrollView addSubview:pageLabel];
        
        [erBook getTextWithIndex:_pageIndex withfinishBlock:^(NSString *string) {
            [pageLabel setShowText:string];
            
        }];
        
        [pageLabel refDisplay];
        [pageLabel release];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (currentParsingMode == ReadParsingModeOther) {
        int new_pageIndex = floorf(scrollView.contentOffset.x/scrollView.frame.size.width+0.5);
        if(pageIndex != (new_pageIndex+1))
        {
            pageIndex = new_pageIndex+1;
            [self loadPageWithIndex:new_pageIndex-1];
            [self loadPageWithIndex:new_pageIndex];
            [self loadPageWithIndex:new_pageIndex+1];
            [self removePageViewWithIndex:new_pageIndex-2];
            [self removePageViewWithIndex:new_pageIndex+2];
        }
    }
    else{
        isToolHidden = NO;
        [self doToolBar];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (currentParsingMode == ReadParsingModeDirectory) {
//        if (scrollView.contentOffset.y >= scrollView.contentSize.height-CGRectGetHeight(contextTextView.bounds)) {
//            isToolHidden = YES;
//            [self doToolBar];
//        }
    }
    else{
        int new_pageIndex = floorf(scrollView.contentOffset.x/scrollView.frame.size.width+0.5);
        
        pageIndex = new_pageIndex+1;
        readProessLabel.text =  [NSString stringWithFormat:@"第%i/%i页",pageIndex,totalPage ];

        readProessView.value = pageIndex;
        
        
        
        NSString *path =  [bookObj bookReadRecordPath];
        NSMutableDictionary *recordDic = [[NSMutableDictionary alloc] init];
        [recordDic setObject:[NSNumber numberWithInt:pageIndex] forKey:@"Read_Record_index"];
        [recordDic setObject:bookObj.title forKey:@"Read_Record_name"];
        [recordDic setObject:bookObj.gId forKey:@"Read_Record_gid"];
        [recordDic writeToFile:path atomically:YES];
        [recordDic release];
    }
    
}


@end
