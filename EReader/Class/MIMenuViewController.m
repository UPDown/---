//
//  MIMenuViewController.m
//  MIViewer
//
//  Created by zym on 13-9-22.
//  Copyright (c) 2013年 MIViewer. All rights reserved.
//

#import "MIMenuViewController.h"
//#import "MIADViewer.h"
#import "MIImageBGView.h"
#import "MIMeunTableCell.h"
#import "ERAppDelegate.h"
#import "SDImageCache.h"
#import "ERHomeViewController.h"
#import "MBProgressHUD.h"
#import "SBJson.h"
#import "ERUserObj.h"
#import "ERUserLoginViewController.h"
#import "ERMeunCellObj.h"
#import "MIMenuCatalog.h"
#import "FileManager.h"
#import <CommonCrypto/CommonDigest.h>
#import "ASIHTTPRequest.h"
#import "NSString+conver.h"
#import "ERFavoriteViewController.h"
#import "ERChaseBookViewController.h"
#import "EROfflineManagerViewController.h"

@implementation NSString (MD5)

- (NSString *)md5Encrypt {
    const char *original_str = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(original_str, strlen(original_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < 16; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
}
@end
@interface MIMenuViewController ()
{   
    UITableView *listTableView;
//    NSMutableArray *catalogArray;
    
    UIView *tableHeadView;
    NSString *chooseCatalog;
    
    UISearchBar *searchBar;
    
    MBProgressHUD *hud;
    
    ERCurrentUserObj *currentUsr;
    
    NSMutableArray *menuCellObjArray;
    
    NSMutableArray *catalogArray;
    NSMutableArray *channleButtonArray;
    
    ASIHTTPRequest *request;
    
    UIView *cateView;
    ERMeunCellObj *cateObj;
  
}
@property (retain, nonatomic) ASIHTTPRequest *request;

- (void)getCategorysFailed:(ASIHTTPRequest *)theRequest;
- (void)getCategorysComplete:(ASIHTTPRequest *)theRequest;
@end

@implementation MIMenuViewController
@synthesize request;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
      
        catalogArray = [[NSMutableArray alloc] init];
        menuCellObjArray = [[NSMutableArray alloc] init];
        currentUsr = [ERCurrentUserObj currentUserInfo];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(userStatusDidChange) name:kUserLoginFinish object:nil];
           [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(userStatusDidChange) name:kUserLogOut object:nil];
        
        
        
        NSString *urlString = [NSString stringWithFormat:@"http://m.baidu.com/book/data/cates"];
        
        [self setRequest:[ASIHTTPRequest requestWithURL:[NSURL URLWithString:urlString]]];
        [request setTimeOutSeconds:20];
        [request setDelegate:self];
        [request setDidFailSelector:@selector(getCategorysFailed:)];
        [request setDidFinishSelector:@selector(getCategorysComplete:)];
        [request startAsynchronous];
    }
    return self;
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    ObjRelease(menuCellObjArray);
    ObjRelease(listTableView);
    ObjRelease(tableHeadView);
    [super dealloc];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
//    MIImageBGView *backImage = [[MIImageBGView alloc] initWithFrame:self.view.bounds];
//    [backImage setImage:[UIImage imageNamed:@"comment_tableview_bg.png"]];
//    [self.view addSubview:backImage];
//    [backImage release];
    self.view.backgroundColor = kBgColor;
    CGRect frame = self.view.bounds;
    
    float height = frame.size.height;
    if (IsIOS7) {
        height = height-20;
        frame.origin.y = frame.origin.y + 20;
        
        UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
        [self.view addSubview:whiteView];
        whiteView.backgroundColor = [UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1];
        [whiteView release];
    }
    frame.size.height  = height;
 
    listTableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    listTableView.backgroundColor = [UIColor clearColor];
    listTableView.backgroundView = nil;
    listTableView.dataSource = (id)self;
    listTableView.delegate = (id)self;
    listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:listTableView];
    [listTableView setScrollsToTop:NO];
    

    
//    //使用缓存
    NSString *catalogJson = [FileManager jsonStrWithName:[@"catalogList" md5Encrypt]];
    NSArray *onlineCatalogArray = [catalogJson JSONValue];
    for (NSString *str in onlineCatalogArray) {
        MIMenuCatalog *catalog = [[[MIMenuCatalog alloc] init] autorelease];
        catalog.title =str;
        [catalogArray addObject:catalog];
    }
    
    
    
    [self addSearchView];
    [self reSetMuenCellObj];
    [self loadCatalogArray];
    
    UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(10, 0, 200, 150)] autorelease];
    
    UITextView *textView = [[[UITextView alloc] initWithFrame:CGRectMake(0, 30, 260, 100)] autorelease];
    textView.editable=NO;
    textView.scrollEnabled =NO;
    textView.text =kAboutInfo;
    textView.textColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.8];
    textView.backgroundColor = [UIColor clearColor];
    textView.font = [UIFont systemFontOfSize:10];
    textView.dataDetectorTypes = UIDataDetectorTypeAll;
    [view addSubview:textView];
    
    listTableView.tableFooterView = view;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onlineConfigCallBack) name:UMOnlineConfigDidFinishedNotification object:nil];
}
-(void)onlineConfigCallBack
{
    [listTableView reloadData];
}
-(void)fitterCatalog
{
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    
    if([version floatValue]>[[MobClick getConfigParams:@"appVersion"] floatValue])
    {
        
        NSString *catalogFitterStr = [MobClick getConfigParams:@"catalogfitter111"];

        NSMutableArray *removeArray = [NSMutableArray array];
        for ( MIMenuCatalog *catalog  in catalogArray) {
            NSString *category =catalog.title;
            if([catalogFitterStr rangeOfString:category].location != NSNotFound)
            {
                [removeArray addObject:catalog];
                continue;
            }
            
        }
        [catalogArray removeObjectsInArray:removeArray];
    }
}
#pragma mark  - ASIHTTPRequest
- (void)getCategorysFailed:(ASIHTTPRequest *)theRequest
{
    
}

- (void)getCategorysComplete:(ASIHTTPRequest *)theRequest
{
    NSString *resultStr = [request responseString];
    NSDictionary *rootDic = [resultStr JSONValue];
    
    NSMutableArray *newArray =[NSMutableArray array];
    if (rootDic && rootDic.count) {
        NSDictionary *resultDic = [rootDic objectForKey:@"result"];
        if (resultDic && resultDic.count) {
            NSArray *aray = [resultDic objectForKey:@"cates"];
            for (NSDictionary *cata in aray) {
                MIMenuCatalog *catalog = [[[MIMenuCatalog alloc] init] autorelease];
                catalog.title =[cata objectForKey:@"catename"];
                catalog.type = MIMenuCatalogCatalogList;
                catalog.categoryID = [NSString_conver NULLStrChangeToStr:[cata objectForKey:@"cateid"]];
                [newArray addObject:catalog];
            }
        }
    }
    
    if(newArray.count >0)
    {
        
        [FileManager saveJsonStr:[newArray JSONRepresentation] name:@"catalogList"];
        [catalogArray removeAllObjects];
        [catalogArray addObjectsFromArray:newArray];
    }
    
    
    [self fitterCatalog];
    [self loadCatalogArray];
    [listTableView reloadData];
}

- (void)getSearchFailed:(ASIHTTPRequest *)theRequest
{
    
}

- (void)getSearchComplete:(ASIHTTPRequest *)theRequest
{
    NSString *resultStr = [request responseString];
    NSDictionary *rootDic = [resultStr JSONValue];
    
    NSMutableArray *newArray =[NSMutableArray array];
    if (rootDic && rootDic.count) {
        NSDictionary *resultDic = [rootDic objectForKey:@"result"];
        if (resultDic && resultDic.count) {
            NSArray *aray = [resultDic objectForKey:@"search"];
        
            for (NSDictionary *cata in aray) {
                MIMenuCatalog *catalog = [[[MIMenuCatalog alloc] init] autorelease];
                catalog.title =[cata objectForKey:@"title"];
                catalog.type = MIMenuCatalogSearch;
                catalog.categoryID = [NSString_conver NULLStrChangeToStr:[cata objectForKey:@"cateid"]];
                [newArray addObject:catalog];
            }
        }
    }
}


#pragma mark - - 
-(void)loadCatalogArray
{
    if(channleButtonArray ==nil){
        channleButtonArray = [[NSMutableArray alloc] init];
    }
    for (UIButton *subButton in channleButtonArray) {
        [subButton removeFromSuperview];
    }
    [channleButtonArray removeAllObjects];;
    if(cateView == nil)
    {
        cateView = [[UIView alloc] init];
        cateView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    //
    }
    CGFloat width = 260.0f;
    
    CGFloat x = 20;
    CGFloat y = 20;
    
    
    
    NSDictionary *catagoryList = [[NSUserDefaults standardUserDefaults] objectForKey:@"PreViewCatagory"];
    
    NSString *curChannel = catagoryList?[catagoryList objectForKey:@"name"]:@"";
    
    for (int i=0;i<catalogArray.count;i++) {
        
        MIMenuCatalog *catalog =[catalogArray objectAtIndex:i];
        
        NSString *name =catalog.title;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        CGSize size = [name sizeWithFont:[UIFont boldSystemFontOfSize:14.0f]];
        
        if (x + size.width > width) {
            x = 20;
            y=y+45;
        }
        
        btn.frame =CGRectMake(x, y, size.width + 8.0f, 27.0f);
        [btn setTitle:name forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        btn.selected = [curChannel isEqualToString:name] ?YES:NO;
        [btn addTarget:self action:@selector(changeCatalog:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [btn setBackgroundImage:[[UIImage imageNamed:@"menu-section-button.png"] stretchableImageWithLeftCapWidth:6.0f topCapHeight:13.0f] forState:UIControlStateNormal];
        [btn setBackgroundImage:[[UIImage imageNamed:@"menu-section-button-pressed.png"] stretchableImageWithLeftCapWidth:6.0f topCapHeight:13.0f] forState:UIControlStateSelected];
        
        btn.tag = i;
        [cateView addSubview:btn];
        x += size.width + 20.0f;
        [channleButtonArray addObject:btn];
        
    }
    y = y+50;
    cateView.frame = CGRectMake(0, 0, 320, y);

}
- (void)changeCatalog:(UIButton *)btn{
    
    if (btn.selected) {
        return;
    }
    for (UIButton *subButton in channleButtonArray) {
        subButton.selected = NO;
    }
    btn.selected = YES;
    
    
    int index = btn.tag;
    MIMenuCatalog *catalog = [catalogArray objectAtIndex:index];
    ERAppDelegate*delegate = (ERAppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate.homeViewController changeCatalog:catalog];
}

-(void)reSetMuenCellObj
{
    [menuCellObjArray removeAllObjects];
    
//    if(currentUsr.userLogined ==NO)
//    {
        [menuCellObjArray addObject:[ERMeunCellObj cellObjWith:@"账号管理" level:0 action:@selector(userLogIn)]];
//    }
//    else{
//        ERMeunCellObj *obj=[ERMeunCellObj cellObjWith:currentUsr.userName level:0 action:@selector(userLogOutAsk)];
//        [menuCellObjArray addObject:obj];
        
        //            obj=[GRMeunCellObj cellObjWith:@"帐号管理" level:1 action:@selector(mangerInfo)];
        //            obj.image = [UIImage imageNamed:@"menu_unsel_author"];
        //            [menuCellObjArray addObject:obj];
        
        //            obj=[ERMeunCellObj cellObjWith:@"我的发布" level:1 action:@selector(myPost)];
        //            obj.image = [UIImage imageNamed:@"menu_unsel_author"];
        //           [menuCellObjArray addObject:obj];
        //
        //            obj=[ERMeunCellObj cellObjWith:@"我的收藏" level:1 action:@selector(myFav)];
        //              obj.image = [UIImage imageNamed:@"menu_unsel_favorites"];
        //            [menuCellObjArray addObject:obj];
//    }
    
    [menuCellObjArray addObject:[ERMeunCellObj cellObjWith:@"首页" level:0 action:@selector(gotoHome)]];
    [menuCellObjArray addObject:[ERMeunCellObj cellObjWith:@"排行" level:0 action:@selector(bookrank)]];
    [menuCellObjArray addObject:[ERMeunCellObj cellObjWith:@"分类" level:0 action:@selector(bookCate)]];
    ObjRelease(cateObj);
    cateObj= [[ERMeunCellObj cellObjWith:@"" level:1 action:nil] retain];
    cateObj.isCatalog = YES;
//    [menuCellObjArray addObject:cateObj];
    [menuCellObjArray addObject:[ERMeunCellObj cellObjWith:@"我的收藏" level:0 action:@selector(chaseBook)]];
    [menuCellObjArray addObject:[ERMeunCellObj cellObjWith:@"我的书签" level:0 action:@selector(bookMark)]];
    [menuCellObjArray addObject:[ERMeunCellObj cellObjWith:@"离线管理" level:0 action:@selector(offlineFile)]];
    
//    [menuCellObjArray addObject:[ERMeunCellObj cellObjWith:@"阅读历史" level:0 action:@selector(historyRead)]];
    
 

    [menuCellObjArray addObject:[ERMeunCellObj cellObjWith:@"清理缓存" level:0 action:@selector(clernCache)]];
//    [menuCellObjArray addObject:[GRMeunCellObj cellObjWith:@"给我好评吧～" level:0 action:@selector(comment)]];
//    [menuCellObjArray addObject:[ERMeunCellObj cellObjWith:@"用户反馈" level:0 action:@selector(umFeedBack)]];
//    [menuCellObjArray addObject:[ERMeunCellObj cellObjWith:@"关于" level:0 action:@selector(about)]];
//    [menuCellObjArray addObject:[ERMeunCellObj cellObjWith:@"精品推荐应用" level:0 action:@selector(addWall)]];
    
    
    if(listTableView.visibleCells.count>0)
    {
        [listTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    else{
        [listTableView reloadData];
    }
}
-(void)addSearchView
{

    UIImageView *searchView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    searchView.userInteractionEnabled = YES;
    UIImage *tempImage = [UIImage imageNamed:@"menu_unsel_bg.png"];
    UIImage *unselImage = [tempImage stretchableImageWithLeftCapWidth:tempImage.size.width/2 topCapHeight:tempImage.size.height/2];
    searchView.image = unselImage;
    
    tableHeadView = [[UIView alloc] init];
    tableHeadView.backgroundColor = [UIColor clearColor];
    [tableHeadView addSubview:searchView];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //        [btn setTitle:@"搜索" forState:UIControlStateNormal];
 [btn  addTarget:self action:@selector(showSearchBar) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(0, 0, 320, 50);
   [searchView addSubview:btn];
    [searchView release];
    
    
    UILabel *tempLable = [[[UILabel alloc] initWithFrame:CGRectMake(20, 0, 200, 50)] autorelease];
    tempLable.font = [UIFont boldSystemFontOfSize:16];
    tempLable.textAlignment = NSTextAlignmentLeft;
    tempLable.textColor = [UIColor colorWithRed:85/255.0 green:85/255.0 blue:85/255.0 alpha:1];
    tempLable.backgroundColor = [UIColor clearColor];
    tempLable.text =@"搜书";
    [btn addSubview: tempLable];
    
    
    UIImageView *serachImage =[[UIImageView alloc] initWithFrame:CGRectMake(220, 13, 35, 25)];
    serachImage.image =[UIImage imageNamed:@"ButtonPreview.png"];
    [searchView addSubview:serachImage];
    [serachImage release];
    tableHeadView.frame = CGRectMake(0, 0, 320, 50);
    listTableView.tableHeaderView = tableHeadView;

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showSearchBar{
    
    if (searchBar == nil) {
        searchBar = [[UISearchBar alloc] init];
        //        searchBar.frame = CGRectMake(0, 31, 320, 44);
        searchBar.frame = CGRectMake(0, -44, 270, 44);
        searchBar.alpha = 1;
        searchBar.delegate = (id)self;
        searchBar.placeholder = @"找我想看";
        searchBar.barStyle = UIBarStyleDefault;
        searchBar.backgroundColor = [UIColor clearColor];
        [self.view addSubview:searchBar];
    }
    
    UIView *bgView = [self.view viewWithTag:32320];
    if (nil == bgView) {
        bgView = [[UIView alloc] init];
        bgView.alpha = 0;
        bgView.tag = 32320;
        bgView.frame = self.view.bounds;
        [bgView setBackgroundColor:[UIColor blackColor]];
        [self.view addSubview:bgView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showSearchBar)];
        [bgView addGestureRecognizer:tap];
        
        [tap  release];
        [bgView release];
        [self.view bringSubviewToFront:searchBar];
    }
    
    BOOL show = bgView.alpha==0?YES:NO;
    
    if (show) {
        searchBar.alpha = 1;
    }
    
    [(ERAppDelegate *)[[UIApplication sharedApplication] delegate] setIsSearching:bgView.alpha==0?YES:NO];
    show?[searchBar becomeFirstResponder]:[searchBar resignFirstResponder];
    show?searchBar.text = nil:searchBar.text;
    [UIView animateWithDuration:0.3 animations:^{
        //        searchBar.alpha = searchBar.alpha==0?1:0;
        searchBar.frame = show?CGRectMake(0, (IsIOS7?20:0), 270, 44):CGRectMake(0, -44, 270, 44);
        listTableView.frame = show?CGRectMake(0, 44+(IsIOS7?20:0), 320, listTableView.frame.size.height):CGRectMake(0, (IsIOS7?20:0), 320, listTableView.frame.size.height);
        bgView.alpha = show?0.4:0;
        
    } completion:^(BOOL finished) {
        
        if (!show) {
            searchBar.alpha = 0;
        }
        
    }];
}


- (void)search:(NSString *)str{
    
    //TODO 有条件搜索
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"kChangeCatalogNotifacation" object:[NSString stringWithFormat:@"search/?q=%@||%@",str,str]];
//    MIMenuCatalog *catalog = [[[MIMenuCatalog alloc] init] autorelease];
//    catalog.catalogName = str;
//    catalog.urlPath = [NSString stringWithFormat:@"search/?q=%@",str];
//    GRAppDelegate*delegate = (GRAppDelegate *)[[UIApplication sharedApplication] delegate];
//    [delegate.homeViewController changeCatalog:catalog];
    
    
    
//    ERMeunCellObj *catalog = [[[ERMeunCellObj alloc] init] autorelease];
//    catalog.title = str;//@"搜索结果";
//    catalog.sraechText =str;
//    catalog.type = MIMenuCatalogSraech;
//    ERAppDelegate*delegate = (ERAppDelegate *)[[UIApplication sharedApplication] delegate];
//    [delegate.homeViewController changeCatalog:catalog];
    
    
    MIMenuCatalog *catalog = [[[MIMenuCatalog alloc] init] autorelease];
    catalog.title = str;
    catalog.type = MIMenuCatalogSearch;
    ERAppDelegate*delegate = (ERAppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate.homeViewController changeCatalog:catalog];
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar{
    
    if (_searchBar.text && [_searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@""]) {
        
        [self search:_searchBar.text];
    }
    [self showSearchBar];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self showSearchBar];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ERMeunCellObj *cellObj = [menuCellObjArray objectAtIndex:indexPath.row];
    if(cellObj.isCatalog)
    {
        if (cateView == nil) {
            [self loadCatalogArray];
        }
        return cateView.frame.size.height;
        
    }
    return 50;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return menuCellObjArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  ERMeunCellObj *cellObj = [menuCellObjArray objectAtIndex:indexPath.row];
    if(cellObj.isCatalog)
    {
        
        static NSString *CellIdentifier = @"cateCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        [cell addSubview:cateView];
        return cell;
    }
    else
    {
    static NSString *CellIdentifier = @"Cell";
        
    MIMeunTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[MIMeunTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    // Configure the cell...
 
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    ERMeunCellObj *cellObj = [menuCellObjArray objectAtIndex:indexPath.row];
    cell.title.text = cellObj.title;
    cell.level = cellObj.level;
    cell.cellImageView.image = cellObj.image;
          return cell;
    }
  
    return nil;
}
-(void)clernImage
{
    //TODO.提示
    [[SDImageCache sharedImageCache] cleanDisk];
    [[SDImageCache sharedImageCache] clearDisk];
    [[SDImageCache sharedImageCache] clearMemory];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"SDImageCacheClearCache" object:nil];
    [self performSelectorOnMainThread:@selector(clernFinish) withObject:nil waitUntilDone:YES];
}
-(void)clernFinish
{
    hud.labelText = @"清理完成";
    hud.mode = MBProgressHUDModeText;
    [hud hide:YES afterDelay:1];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ERMeunCellObj *cellObj = [menuCellObjArray objectAtIndex:indexPath.row];
    if(cellObj.isCatalog)return;
    [self performSelector:cellObj.action];
}


#pragma mark - -cell SEL
// 离线
- (void)offlineFile
{
    [MobClick event:@"offlineAction"];
    
    UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    
    EROfflineManagerViewController *offlineVC =[[EROfflineManagerViewController alloc] init];
    
    UINavigationController *flip = [[UINavigationController alloc] initWithRootViewController:offlineVC];
    if (!IsIOS7) {
        flip.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], UITextAttributeTextColor,[UIFont systemFontOfSize:18.0],UITextAttributeFont,nil];
        
        flip.navigationBar.tintColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:0.9];
    }
    [rootViewController presentModalViewController:flip animated:YES];
    [flip release];
    [offlineVC release];
}

-(void)gotoHome
{
    [MobClick event:@"homeAction"];
    
    MIMenuCatalog *catalog = [[[MIMenuCatalog alloc] init] autorelease];
    catalog.title = @"首页";
    catalog.type = MIMenuCatalogHome;
    ERAppDelegate*delegate = (ERAppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate.homeViewController changeCatalog:catalog];
}
-(void)bookrank
{
    [MobClick event:@"rankAction"];
    
    MIMenuCatalog *catalog = [[[MIMenuCatalog alloc] init] autorelease];
    catalog.title = @"排行";
    catalog.type = MIMenuCatalogRank;
    ERAppDelegate*delegate = (ERAppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate.homeViewController changeCatalog:catalog];
}
-(void)bookCate
{
    [MobClick event:@"cateAction"];
    
    if([menuCellObjArray containsObject:cateObj])
    {
        [menuCellObjArray removeObject:cateObj];
        [listTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:4 inSection:0]] withRowAnimation:UITableViewRowAnimationMiddle];
        
    }
    else{
       [menuCellObjArray insertObject:cateObj atIndex:4];
        [listTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:4 inSection:0]] withRowAnimation:UITableViewRowAnimationMiddle];
        
  }

//    [UIView beginAnimations:nil context:nil];
//    [listTableView reloadData];
//    [UIView commitAnimations];
}

//收藏
- (void)chaseBook
{
    [MobClick event:@"chaseAction"];
    
    UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    
    ERChaseBookViewController *chase =[[ERChaseBookViewController alloc] init];
    
    UINavigationController *flip = [[UINavigationController alloc] initWithRootViewController:chase];
    if (!IsIOS7) {
        flip.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], UITextAttributeTextColor,[UIFont systemFontOfSize:18.0],UITextAttributeFont,nil];
        
        flip.navigationBar.tintColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:0.9];
    }
    [rootViewController presentModalViewController:flip animated:YES];

    [flip release];
    [chase release];

}

// 书签
-(void)bookMark
{
    [MobClick event:@"bookMarkAction"];
    
    UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    
    ERFavoriteViewController *favorite =[[ERFavoriteViewController alloc] init];
    
    UINavigationController *flip = [[UINavigationController alloc] initWithRootViewController:favorite];
    if (!IsIOS7) {
        flip.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], UITextAttributeTextColor,[UIFont systemFontOfSize:18.0],UITextAttributeFont,nil];
        
        flip.navigationBar.tintColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:0.9];
    }
    [rootViewController presentModalViewController:flip animated:YES];
    [flip release];
    [favorite release];
}

-(void)historyRead
{
    
}


-(void)userLogOutAsk
{
    UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"是否注销？" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alter show];
    [alter release];
}

-(void)userLogIn
{
    UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;

     ERUserLoginViewController *login =[[[ERUserLoginViewController alloc] init] autorelease];
    
    UINavigationController *flip = [[[UINavigationController alloc] initWithRootViewController:login] autorelease];
    if (!IsIOS7) {
        flip.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], UITextAttributeTextColor,[UIFont systemFontOfSize:18.0],UITextAttributeFont,nil];
        
        flip.navigationBar.tintColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:0.9];
    }
    [rootViewController presentModalViewController:flip animated:YES];
   
}

-(void)mangerInfo
{
    
}

-(void)myPost
{
//    ERMeunCellObj *catalog = [[[ERMeunCellObj alloc] init] autorelease];
//    catalog.title = @"我的发布";
//    catalog.type = MIMenuCatalogMyPost;
//    GRAppDelegate*delegate = (GRAppDelegate *)[[UIApplication sharedApplication] delegate];
//    [delegate.homeViewController changeCatalog:catalog];
}

-(void)myFav
{
//    MIMenuCatalog *catalog = [[[MIMenuCatalog alloc] init] autorelease];
//    catalog.title = @"我的收藏";
//    catalog.type = MIMenuCatalogMyFav;
//    GRAppDelegate*delegate = (GRAppDelegate *)[[UIApplication sharedApplication] delegate];
//    [delegate.homeViewController changeCatalog:catalog];
}

-(void)clernCache
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"清理缓存" message:@"是否清理缓存？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
    alertView.tag = 101;
    [alertView show];
    [alertView release];
}

-(void)comment
{
 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=880549221"]];
}

-(void)about
{
//    UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
//    GRAboutViewController *aboutViewCont =[[GRAboutViewController alloc] init];
//    [rootViewController presentModalViewController:aboutViewCont animated:YES];
//    [aboutViewCont release];
//  
}

-(void)umFeedBack
{
//    UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
//    [UMFeedback setLogEnabled:YES];
//    [UMFeedback checkWithAppkey:UMENG_APPKEY];
//    UMFeedback* _umFeedback = [UMFeedback sharedInstance];
//    [_umFeedback setAppkey:UMENG_APPKEY delegate:(id)self];
//    [UMFeedback showFeedback:rootViewController withAppkey:UMENG_APPKEY];
}

-(void)addWall
{
//    UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
//    MIWallViewController *wallViewCont =[[MIWallViewController alloc] init];
//    [rootViewController presentModalViewController:wallViewCont animated:YES];
//    [wallViewCont release];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 101) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES ];
            hud.labelText = @"清理中...";
            hud.mode = MBProgressHUDModeIndeterminate;
            [self performSelectorInBackground:@selector(clernImage) withObject:nil];
        }
    }
    else{
        if(buttonIndex==0)
        {
            
        }
        else{
            [[ERCurrentUserObj currentUserInfo] userLogOut];
        }
    }
}

////==
-(void)userStatusDidChange
{
    [self reSetMuenCellObj];
}

@end
