//
//  ERReadViewController.h
//  EReader
//
//  Created by Hudajiang on 14-1-17.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EROfflineBookObj.h"
typedef enum  {
    ReadParsingModeDirectory = 0,
    ReadParsingModeOther
}ReadParsingMode;


@interface ERReadViewController : UIViewController
{
    ReadParsingMode     currentParsingMode;
    EROfflineBookObj *bookObj;
}
-(id)initWithBookObj:(EROfflineBookObj *)readBookObj;
@end
