//
//  ERSettingView.m
//  EReader
//
//  Created by Hudajiang on 14-2-18.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import "ERSettingView.h"

@implementation ERSettingView
@synthesize settingDelegate;
@synthesize fontSegment;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

    }
    return self;
}
- (void)awakeFromNib
{
    selectColorTag = 1;
    
    lightSlider.value = [UIScreen mainScreen].brightness;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changebrugthness:) name:UIScreenBrightnessDidChangeNotification object:nil];
    
    
    if (!IsIOS7) {
        lightSlider.minimumTrackTintColor = [UIColor colorWithRed:37/255.0 green:124/255.0 blue:252/255.0 alpha:1.0];
        lightSlider.maximumTrackTintColor = [UIColor colorWithRed:187/255.0 green:187/255.0 blue:187/255.0 alpha:1.0];
        lightSlider.thumbTintColor = [UIColor whiteColor];
        
        fontSegment.tintColor = [UIColor colorWithRed:37/255.0 green:124/255.0 blue:252/255.0 alpha:1.0];
        fontSegment.segmentedControlStyle = UISegmentedControlStyleBordered;
        
        nightSegment.tintColor = [UIColor colorWithRed:37/255.0 green:124/255.0 blue:252/255.0 alpha:1.0];
        nightSegment.segmentedControlStyle = UISegmentedControlStyleBordered;
    }

    colorBtn1.tag =1;
    colorBtn2.tag =2;
    colorBtn3.tag =3;
    colorBtn4.tag =4;
     colorBtn5.tag =5;
    
    CGColorRef borderColor =[UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.5].CGColor;
    colorBtn1.layer.borderColor =borderColor;
    colorBtn2.layer.borderColor = borderColor;
    colorBtn3.layer.borderColor = borderColor;
    colorBtn4.layer.borderColor =borderColor;
    colorBtn5.layer.borderColor = borderColor;
    
    colorBtn1.layer.borderWidth = 0;
    colorBtn2.layer.borderWidth = 0;
    colorBtn3.layer.borderWidth = 0;
    colorBtn4.layer.borderWidth = 0;
    colorBtn5.layer.borderWidth = 0;
    

    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 5;
    self.layer.shadowColor =borderColor;
    self.layer.shadowOffset = CGSizeMake(0, 0);

}
-(void)changebrugthness:(NSNotification *)notify
{
    lightSlider.value = [UIScreen mainScreen].brightness;
}


-(void)readCache
{
    //读取缓存设置。。。
    int mode = [[NSUserDefaults standardUserDefaults] integerForKey:@"mode"];
    switch (mode) {
        case -1:
        {
            int colorIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"colorIndex"];
            switch (colorIndex) {
                case 1:
                    [self colorAction:colorBtn1];
                    break;
                case 2:
                    [self colorAction:colorBtn2];
                    break;
                case 3:
                    [self colorAction:colorBtn3];
                    break;
                case 4:
                    [self colorAction:colorBtn4];
                    break;
                case 5:
                    [self colorAction:colorBtn5];
                    break;
                default:
                    break;
            }
            break;
        }
        case 0:
        {
            
            nightSegment.selectedSegmentIndex = 0;
            
            int colorIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"colorIndex"];
            switch (colorIndex) {
                case 1:
                    [self colorAction:colorBtn1];
                    break;
                case 2:
                    [self colorAction:colorBtn2];
                    break;
                case 3:
                    [self colorAction:colorBtn3];
                    break;
                case 4:
                    [self colorAction:colorBtn4];
                    break;
                case 5:
                    [self colorAction:colorBtn5];
                    break;
                default:
                    break;
                }
            
                [self nightAction:nightSegment];
            }
            break;
        case 1:
            nightSegment.selectedSegmentIndex = 1;
            [self nightAction:nightSegment];
            break;
        default:
            break;
    }
    
    
    int fontIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"fontIndex"];
    fontSegment.selectedSegmentIndex =fontIndex;
    

}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    colorBtn1 = nil;
    colorBtn2 = nil;
    colorBtn3 = nil;
    colorBtn4 = nil;
    colorBtn5 = nil;

    nightSegment = nil;
    lightSlider = nil;
    self.fontSegment =nil;
    self.settingDelegate = nil;
    [super dealloc];
}

#pragma mark -设置选项 Action
//设置选项
- (IBAction)colorAction:(UIButton *)sender
{
     [[NSUserDefaults standardUserDefaults] setInteger:-1 forKey:@"mode"];
    [[NSUserDefaults standardUserDefaults] setInteger:sender.tag forKey:@"colorIndex"];
    [[NSUserDefaults standardUserDefaults] synchronize];
 
    selectColorTag = sender.tag;
    colorBtn1.layer.borderWidth = 0;
    colorBtn2.layer.borderWidth = 0;
    colorBtn3.layer.borderWidth = 0;
    colorBtn4.layer.borderWidth = 0;
    colorBtn5.layer.borderWidth = 0;
    
    nightSegment.selectedSegmentIndex = 0;
    
    sender.layer.borderWidth = 2;
    if (settingDelegate) {
        if ([settingDelegate respondsToSelector:@selector(colorAction:)]) {
            [settingDelegate colorAction:sender];
        }
    }
}

- (IBAction)fontAction:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setInteger:fontSegment.selectedSegmentIndex forKey:@"fontIndex"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    if (settingDelegate) {
        if ([settingDelegate respondsToSelector:@selector(fontAction:)]) {
            [settingDelegate fontAction:sender];
        }
    }
}

- (IBAction)nightAction:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setInteger:nightSegment.selectedSegmentIndex forKey:@"mode"];
    
    if (nightSegment.selectedSegmentIndex==0) {

        [[NSUserDefaults standardUserDefaults] setInteger:selectColorTag forKey:@"colorIndex"];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setInteger:nightSegment.selectedSegmentIndex forKey:@"colorIndex"];
    }

    [[NSUserDefaults standardUserDefaults] synchronize];
    if (settingDelegate) {
        if ([settingDelegate respondsToSelector:@selector(nightAction:)]) {
            [settingDelegate nightAction:sender];
        }
    }
}

- (IBAction)lightAction:(id)sender
{
    if (settingDelegate) {
        if ([settingDelegate respondsToSelector:@selector(lightAction:)]) {
            [settingDelegate lightAction:sender];
        }
    }
}


@end
