//
//  ERPageChangeView.h
//  EReader
//
//  Created by Hudajiang on 14-1-14.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ERPageChangeViewDelegate <NSObject>
- (void)SelectPageAndCloseView;
- (void)selectPage:(int)index;
@end

@interface ERPageChangeView : UIView<UIPickerViewDataSource,UIPickerViewDelegate>

@property (retain, nonatomic) IBOutlet UIView *pickerBgView;
@property (nonatomic,assign)id  pageDelegate;
@property (nonatomic,assign)int selectNum;
@property (nonatomic,assign)int totalNum;
@property (retain, nonatomic) IBOutlet UIPickerView *pickerView;

- (IBAction)FinishAction:(id)sender;
- (void)loadDataWithNumCount:(int)num;
@end
