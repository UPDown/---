//
//  MIMeunTableCell.h
//  MIViewer
//
//  Created by zym on 13-9-22.
//  Copyright (c) 2013年 MIViewer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIMeunTableCell : UITableViewCell
{
    UISwitch *switchView;
    UIImageView *bgView;
    
    UIImage *selImage;
    UIImage *unselImage;
    
  
}
//@property (nonatomic,retain) UIImage *selImage;
//@property (nonatomic,retain) UIImage *unselImage;
@property (nonatomic,assign) int level;
@property (nonatomic,retain) UILabel *title;
@property (nonatomic,readonly)  UIImageView *cellImageView;
@end
