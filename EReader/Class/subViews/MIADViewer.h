//
//  MIADViewer.h
//  MIViewer
//
//  Created by zym on 13-9-22.
//  Copyright (c) 2013年 MIViewer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GuomobAdSDK.h"

#define kGguomobAdKey @"slgca6xvh091330"

@protocol MIADViewerDelegate <NSObject>
-(void)adLoadFilish:(id)view;
-(void)loadAdError:(NSString *)error;
@end


@interface MIADViewer : UIView
{
    GuomobAdSDK *guomobBannerAD;
}
@property (nonatomic,assign) id<MIADViewerDelegate>delegate;
+(MIADViewer *)shareADView;
@end
