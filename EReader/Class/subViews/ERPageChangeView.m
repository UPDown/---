//
//  ERPageChangeView.m
//  EReader
//
//  Created by Hudajiang on 14-1-14.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import "ERPageChangeView.h"

@implementation ERPageChangeView
@synthesize pageDelegate;
@synthesize selectNum,totalNum;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        totalNum= 0;
        selectNum = 0;
    }
    return self;
}

- (void)dealloc {
    [_pickerView release];
    [_pickerBgView release];
    [super dealloc];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
     CGPoint point= [touch locationInView:self];
    if(CGRectContainsPoint(_pickerView.frame, point))
    {
    [self dismiss];
    }
}

-(void)dismiss
{
    CGRect rect =self.pickerBgView.frame;
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                         self.pickerBgView.frame=CGRectOffset(rect, 0, self.pickerBgView.frame.size.height);
                         self.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0];
                     } completion:^(BOOL finished) {
                         if (self.pageDelegate) {
                             [self.pageDelegate SelectPageAndCloseView];
                         }
                     }];
}
- (void)loadDataWithNumCount:(int)num
{
    CGRect rect =self.pickerBgView.frame;
self.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0];
    self.pickerBgView.frame=CGRectOffset(rect, 0, self.pickerBgView.frame.size.height);
    [UIView animateWithDuration:0.25f
                         animations:^{
                             [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                             self.pickerBgView.frame=rect;
                             self.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.6];
                         } completion:^(BOOL finished) {

                         }];

     
    self.totalNum= num;
    [self.pickerView selectRow:self.selectNum inComponent:0 animated:YES];
    [self.pickerView reloadAllComponents];
}

- (IBAction)FinishAction:(id)sender {
    [self dismiss];
}

#pragma mark -UIPickerViewDelegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.totalNum;
}
-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [NSString stringWithFormat: @"第%i页",row+1];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.selectNum = row;
    if (self.pageDelegate) {
        [self.pageDelegate selectPage:self.selectNum];
    }
}
@end
