//
//  FBPageScrollView.h
//  FirmamentBook
//
//  Created by HDJ on 14-7-4.
//  Copyright (c) 2014年 HDJ. All rights reserved.
//

#import <UIKit/UIKit.h>

#define FONT    [UIFont systemFontOfSize:18]

@protocol FBPageScrollViewDelegate <NSObject>

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

@end

@interface FBPageScrollView : UIScrollView

@property (assign, nonatomic)id<FBPageScrollViewDelegate> pageDelegate;

@property (strong, nonatomic) IBOutlet UITextView *contentView;

- (void)setContentFont:(UIFont *)font;
- (void)bindData:(id)data Content:(NSString *)contentStr;
- (void)reloadData;
@end
