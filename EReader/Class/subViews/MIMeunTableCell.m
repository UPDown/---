//
//  MIMeunTableCell.m
//  MIViewer
//
//  Created by zym on 13-9-22.
//  Copyright (c) 2013年 MIViewer. All rights reserved.
//

#import "MIMeunTableCell.h"

@implementation MIMeunTableCell
@synthesize title,level=_level,cellImageView=_cellImageView;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.backgroundColor = [UIColor whiteColor];
        
        UIImage *tempImage = [UIImage imageNamed:@"menu_unsel_bg.png"];
         unselImage = [[tempImage stretchableImageWithLeftCapWidth:tempImage.size.width/2 topCapHeight:tempImage.size.height/2] retain];
        
        tempImage = [UIImage imageNamed:@"menu_sel_bg.png"];
         selImage = [[tempImage stretchableImageWithLeftCapWidth:tempImage.size.width/2 topCapHeight:tempImage.size.height/2] retain];
        
        bgView= [[UIImageView alloc] initWithImage:unselImage];
        [self addSubview:bgView];
//        self.selectedBackgroundView = [[[UIImageView alloc] initWithImage:selImage] autorelease];
        
//        icon = [[[UIImageView alloc] initWithFrame:CGRectMake(15, 16, 18, 18)] autorelease];
//        icon.backgroundColor = [UIColor clearColor];
//        [self addSubview:icon];
        
        self.title = [[[UILabel alloc] initWithFrame:CGRectMake(20, 0, 200, 50)] autorelease];
        self.title.font = [UIFont boldSystemFontOfSize:16];
        self.title.textAlignment = NSTextAlignmentLeft;
        self.title.textColor = [UIColor colorWithRed:85/255.0 green:85/255.0 blue:85/255.0 alpha:1];
        self.title.backgroundColor = [UIColor clearColor];
        [self addSubview: self.title];
        
        _cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 16, 18, 18)];
        [self addSubview:_cellImageView];
    }
    return self;
}
-(void)dealloc
{
    self.title = nil;
    ObjRelease(bgView);
    ObjRelease(unselImage);
    ObjRelease(selImage);
    [super dealloc];
}
-(void)setLevel:(int)level
{
    _level = level;
    self.title.frame = CGRectMake(20+level*30, 0, 200, 50);
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
//    if(selected)
//    {
//        if (switchView == nil) {
//            bgView.image = selImage;
//        }
//    }
//    else{
//      bgView.image = unselImage;
//    }
    // Configure the view for the selected state
}
-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    if(highlighted)
    {
        if (switchView == nil) {
               bgView.image = selImage;
        }
    }
    else{
            bgView.image = unselImage;
    }
}

@end
