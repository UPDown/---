//
//  ERCatalogTableCell.m
//  EReader
//
//  Created by helfy  on 14-1-12.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import "ERCatalogTableCell.h"

@implementation ERCatalogTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
