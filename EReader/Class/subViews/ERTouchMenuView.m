//
//  ERTouchMenuView.m
//  EReader
//
//  Created by HDJ on 13-12-1.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import "ERTouchMenuView.h"
#import <QuartzCore/QuartzCore.h>

#define aniTime  0.3f

@implementation ERTouchMenuView
@synthesize touchMenuDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        currentPoint = CGPointZero;
        
        //tab bar view  始终居中显示
        tabBarView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2-100, self.frame.size.height/2-100, 200 , 200)] ;
        
        //view 设置半透明 圆角样式
        tabBarView.layer.cornerRadius = 10;//设置圆角的大小
        tabBarView.layer.backgroundColor = [[UIColor blackColor] CGColor];
        tabBarView.alpha = 0.8f;//设置透明
        tabBarView.layer.masksToBounds = YES;
        tabBarView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        
        [self addSubview:tabBarView];
        
        //循环设置tabbar上的button
        NSArray *imgNames = [[NSArray alloc]initWithObjects:@"favIcon.png",@"button_close.png", nil];
        NSArray *tabTitle = [[NSArray alloc]initWithObjects:@"收藏",@"返回 ", nil];
        
//        for (int i=0; i<4; i++) {
//            CGRect rect;
//            rect.size.width = 60;
//            rect.size.height = 60;
//            switch (i) {
//                case 0:
//                    rect.origin.x = 100-30;
//                    rect.origin.y = 40-30;
//                    break;
//                case 1:
//                    rect.origin.x = 160-30;
//                    rect.origin.y = 100-30;
//                    break;
//                case 2:
//                    rect.origin.x = 100-30;
//                    rect.origin.y = 160-30;
//                    break;
//                case 3:
//                    rect.origin.x = 40-30;
//                    rect.origin.y = 100-30;
//                    break;
//            }
        for (int i = 0; i < 2; i++) {
            CGRect rect;
            rect.size.width = 60;
            rect.size.height = 60;
            switch (i) {
                case 0:
                    rect.origin.x = 100-60-10;
                    rect.origin.y = 100-30;
                    break;
                case 1:
                    rect.origin.x = 100+10;
                    rect.origin.y = 100-30;
                    break;
            }
            
            //设置每个tabView
            UIButton *tabButton = [[UIButton alloc] initWithFrame:rect];
            tabButton.backgroundColor = [UIColor clearColor];
            [tabButton setImage:[UIImage imageNamed:[imgNames objectAtIndex:i]] forState:UIControlStateNormal];
            [tabButton setTitle:[tabTitle objectAtIndex:i] forState:UIControlStateNormal];
            tabButton.titleLabel.font = [UIFont systemFontOfSize:12];
            tabButton.titleLabel.textAlignment = UITextAlignmentCenter;
            [tabButton setTitleEdgeInsets:UIEdgeInsetsMake(35, -30, 0, 0)];
            [tabButton setImageEdgeInsets:UIEdgeInsetsMake(0, 12, 20, 12)];
            [tabButton setTag:i];
            [tabButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [tabBarView addSubview:tabButton];
            
//            //设置tabView的图标
//            UIButton *tabButton = [UIButton buttonWithType:UIButtonTypeCustom];
//            tabButton.frame = CGRectMake(15, 0, 30, 30);
//            [tabButton setBackgroundImage:[UIImage imageNamed:[imgNames objectAtIndex:i]] forState:UIControlStateNormal];
//            [tabButton setTag:i];
//            [tabButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
//            [tabView addSubview:tabButton];
//            
//            //设置标题
//            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 35, 60, 15)];
//            titleLabel.font = [UIFont systemFontOfSize:12];
//            titleLabel.textAlignment = UITextAlignmentCenter;
//            titleLabel.textColor = [UIColor whiteColor];
//            titleLabel.backgroundColor = [UIColor clearColor];
//            titleLabel.text = [tabTitle objectAtIndex:i];
//            [tabView addSubview:titleLabel];
//            [titleLabel release];
            
            [tabButton release];
        }
        [imgNames release];
        [tabTitle release];
    }
    return self;
}

- (void)dealloc
{
    self.touchMenuDelegate = nil;
    [tabBarView release];
    [super dealloc];
}


- (void)showView:(CGPoint)centerPoint
{
    currentPoint = centerPoint;
    
    self.hidden = NO;
    tabBarView.hidden = YES;
    tabBarView.center = centerPoint;
    tabBarView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:aniTime];
    tabBarView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    tabBarView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    NSLog(@"%f",self.frame.size.height/2);
    tabBarView.hidden = NO;
    [UIView commitAnimations];
    
}

- (void)hiddenView:(CGPoint)centerPoint
{
    tabBarView.hidden = NO;
    tabBarView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    tabBarView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:aniTime];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(hiddenSelfView)];
    tabBarView.center = centerPoint;
    tabBarView.transform = CGAffineTransformMakeScale(0.01, 0.01);

    [UIView commitAnimations];
}

- (void)hiddenSelfView
{
    tabBarView.hidden = YES;
    self.hidden = YES;
}

- (void)buttonClicked:(UIButton *)sender
{
    int index = [sender tag];
    if (self.touchMenuDelegate) {
        [self hiddenView:currentPoint];
        if ([self.touchMenuDelegate respondsToSelector:@selector(touchMenuClick:)]) {
            double delayInSeconds = aniTime;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                 [self.touchMenuDelegate touchMenuClick:index];
            });
           
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.touchMenuDelegate) {
        [self hiddenView:currentPoint];
        if ([self.touchMenuDelegate respondsToSelector:@selector(touchMenuCanceledClick)]) {
            double delayInSeconds = aniTime;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                 [self.touchMenuDelegate touchMenuCanceledClick];
            });
        }
    }
}
@end
