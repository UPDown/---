//
//  GRCommentListCell.m
//  GameRaiders
//
//  Created by helfy  on 13-10-13.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import "GRCommentListCell.h"
#import "NSString+conver.h"
#import <QuartzCore/QuartzCore.h>
@implementation GRCommentListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    
        
        contentBgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 5, 280, 89)];
        contentBgView.image = [[UIImage imageNamed:@"comment_bubble"] stretchableImageWithLeftCapWidth:30 topCapHeight:30];
        contentBgView.backgroundColor = [UIColor clearColor];
        contentBgView.layer.masksToBounds = YES;
        [self addSubview: contentBgView];
        
        // Initialization code
        autorLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 290, 15)];
        autorLabel.textColor = [UIColor colorWithRed:121/255.0 green:122/255.0 blue:124/255.0 alpha:1];
        autorLabel.backgroundColor = [UIColor clearColor];
        autorLabel.font = [UIFont systemFontOfSize:14];
        [contentBgView addSubview:autorLabel];
        
        commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 250, 20)];
        commentLabel.backgroundColor = [UIColor clearColor];
        commentLabel.font = [UIFont systemFontOfSize:15];
        commentLabel.numberOfLines = 0;
        commentLabel.textColor = [UIColor colorWithRed:54/255.0 green:59/255.0 blue:64/255.0 alpha:1];
        commentLabel.lineBreakMode = UILineBreakModeWordWrap;

        [contentBgView addSubview:commentLabel];
        
        timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 40, 15)];
        timeLabel.textColor = [UIColor colorWithRed:121/255.0 green:122/255.0 blue:124/255.0 alpha:1];;
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.font = [UIFont systemFontOfSize:14];
        timeLabel.textAlignment =NSTextAlignmentRight;
        [contentBgView addSubview:timeLabel];
  
    }
    return self;
}
-(void)setDisplayObj:(GRCommentObj *)newObj  withHigthLigthId:(NSString *)higthLigthId
{
    if([newObj.userInfo.userId intValue] == [higthLigthId intValue])
    {
        autorLabel.textColor = [UIColor colorWithRed:200/255.0 green:59/255.0 blue:43/255.0 alpha:1];//[UIColor colorWithRed:193/255.0 green:166/255.0 blue:142/255.0 alpha:1];
    }
    else
    {
        autorLabel.textColor = [UIColor grayColor];
    }
    autorLabel.text = newObj.userInfo.userName;
    CGRect frame=  contentBgView.frame;
    commentLabel.text =newObj.commentContent;
    timeLabel.text = [NSString_conver dateWithString:newObj.creatDate];
   
    CGSize size = [commentLabel.text sizeWithFont:commentLabel.font constrainedToSize:CGSizeMake(commentLabel.frame.size.width, 9999) lineBreakMode:UILineBreakModeWordWrap];
    commentLabel.frame = CGRectMake(15,10, commentLabel.frame.size.width, size.height);
    frame.size.height = size.height+55;
    autorLabel.frame = CGRectMake(5, frame.size.height -30, frame.size.width-120, 30);
    timeLabel .frame = CGRectMake(frame.size.width-110, frame.size.height -30, 100, 30);
    contentBgView.frame = frame;
//    [self startAnimation];
}
-(void)startAnimation
{
//    CGRect rect = contentBgView.frame;
//    CGRect rect1 = contentBgView.frame;
//    rect1.origin.x=-rect1.size.width;
//    contentBgView.frame = rect1;
    contentBgView.alpha =0;
    [UIView beginAnimations:nil context:nil];
//    contentBgView.frame = rect;
   contentBgView.alpha =1;
    [UIView commitAnimations];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+ (UILabel*)textLabelWithFrame:(CGRect)rect
{
	UILabel *label = [[UILabel alloc] initWithFrame:rect];
	return [label autorelease];
}
+(NSString *)rtcString:(NSString *)textString
{
    return textString;
}

+(float)heigthForOb:(GRCommentObj *)newObj
{
    float cellHeigth = 40;
    UILabel* contentText =[[self class] textLabelWithFrame:CGRectMake(0, 0, 250, 20)];
    contentText.font = [UIFont systemFontOfSize:15];
        contentText.lineBreakMode = UILineBreakModeWordWrap;
    contentText.text = [[self class] rtcString:newObj.commentContent];

    CGSize size = [contentText.text sizeWithFont:contentText.font constrainedToSize:CGSizeMake(contentText.frame.size.width, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    cellHeigth = cellHeigth+((size.height));

    return cellHeigth;
}
@end
