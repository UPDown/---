//
//  MIADViewer.m
//  MIViewer
//
//  Created by zym on 13-9-22.
//  Copyright (c) 2013年 MIViewer. All rights reserved.
//

#import "MIADViewer.h"
//#import "MobClick.h"
static MIADViewer *shareADView = nil;
@implementation MIADViewer
@synthesize delegate;
#define kAdViewPortraitRect CGRectMake(0, 0, 320, 50)
+(MIADViewer *)shareADView
{
    if(shareADView == nil)
    {
        shareADView= [[MIADViewer alloc] initWithFrame:CGRectZero];
    }
    return shareADView;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.hidden = YES;
        //根据友盟返回参数加载广告
        
        int adtype = 0;
        switch (adtype) {
            case 0:
                //guomob
                [self loadGoumobAd];
                break;
            case 1:
                //百度
                [self loadBaiduAd];
                break;
            case 2:
                //随机
                if(arc4random()%2==1)
                {
                   [self loadBaiduAd];
                }
                else{
                   [self loadGoumobAd];
                }
             
                break;
                
            default:
                break;
        }
    }
    return self;
}
-(void)dealloc
{

    ObjRelease(guomobBannerAD);
    [super dealloc];
}
#pragma baiduAD

-(void)loadBaiduAd
{
//    [baiDuAdView removeFromSuperview];
//    ObjRelease(baiDuAdView);
//    self.frame =kAdViewPortraitRect;
//    baiDuAdView = [[BaiduMobAdView alloc] init];
//    //sharedAdView.AdUnitTag = @"myAdPlaceId1";
//    //此处为广告位id，可以不进行设置，如需设置，在百度移动联盟上设置广告位id，然后将得到的id填写到此处。
//    baiDuAdView.AdType = BaiduMobAdViewTypeBanner;
//    baiDuAdView.frame = self.bounds;
//    baiDuAdView.delegate = self;
//    [self addSubview:baiDuAdView];
//    [baiDuAdView start];
}
//- (NSString *)publisherId
//{
//        return kBaiduAdKey;
////    return  @"debug"; //@"d6f2f0fc";
//}
//
//- (NSString*) appSpec
//{
//    //注意：该计费名为测试用途，不会产生计费，请测试广告展示无误以后，替换为您的应用计费名，然后提交AppStore.
//    return kBaiduAdKey;
////    return @"debug";//d6f2f0fc
//}
//
//-(void) willDisplayAd:(BaiduMobAdView*) adview
//{
//    //在广告即将展示时，产生一个动画，把广告条加载到视图中
//    baiDuAdView.hidden = NO;
////    CGRect f = sharedAdView.frame;
////    f.origin.x = -320;
////    sharedAdView.frame = f;
////    [UIView beginAnimations:nil context:nil];
////    f.origin.x = 0;
////    sharedAdView.frame = f;
////    [UIView commitAnimations];
//    self.hidden = NO;
//    if(self.delegate && [self.delegate respondsToSelector:@selector(adLoadFilish:)])
//    {
//        [self.delegate adLoadFilish:self];
//    }
//    
//}

//-(void) failedDisplayAd:(BaiduMobFailReason) reason;
//{
//    NSLog(@"delegate: failedDisplayAd %d", reason);
//    //加载 其他的
//       [self loadGoumobAd];
//}




#pragma guomob
-(void)loadGoumobAd
{
    [guomobBannerAD removeFromSuperview];
    ObjRelease(guomobBannerAD);
    
    self.frame =kAdViewPortraitRect;
    guomobBannerAD=[GuomobAdSDK initWithAppId:kGguomobAdKey delegate:self];
    [guomobBannerAD loadAd:YES];
    [self addSubview:guomobBannerAD];
}
- (void)loadBannerAdSuccess:(BOOL)success
{
    if(success)
    {
    self.hidden = NO;
        if(self.delegate && [self.delegate respondsToSelector:@selector(adLoadFilish:)])
        {
            [self.delegate adLoadFilish:self];
        }
    }
//    else{
//        [self loadBaiduAd];
//    }
}
- (void)BannerConnectionDidFailWithError:(NSString *)error
{
     self.hidden = YES;
    self.frame = CGRectZero;
    if(self.delegate && [self.delegate respondsToSelector:@selector(loadAdError:)])
    {
        [self.delegate loadAdError:error];
    }
}
@end
