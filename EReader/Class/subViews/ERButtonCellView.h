//
//  ERButtonCellView.h
//  EReader
//
//  Created by helfy  on 13-12-2.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ERButtonCellView : UIButton
@property (nonatomic,retain) UILabel *titleLabel;
@property (nonatomic,retain) UIImageView *iconView;
@end
