//
//  FBPageScrollView.m
//  FirmamentBook
//
//  Created by HDJ on 14-7-4.
//  Copyright (c) 2014年 HDJ. All rights reserved.
//

#import "FBPageScrollView.h"
#import <CoreText/CoreText.h>

@implementation FBPageScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.pageDelegate) {
        if ([self.pageDelegate respondsToSelector:@selector(touchesEnded:withEvent:)]) {
            [self.pageDelegate touchesEnded:touches withEvent:event];
        }
    }
}


- (void)awakeFromNib
{
    self.contentView.textColor = [UIColor blackColor];
    self.contentView.font = FONT;
    self.contentView.textAlignment = NSTextAlignmentRight;
}


- (void)bindData:(id)data Content:(NSString *)contentStr
{
    
    NSString *titleStr = [data objectForKey:@"title"];
//    int pageIndex = [[data objectForKey:@"pageIndex"] intValue];
//    int totalPage = [[data objectForKey:@"totalPage"] intValue];
    
//    self.chapterNameLabel.text = titleStr;
//    self.progressLabel.text = [NSString stringWithFormat:@"%i/%i",pageIndex-1,totalPage];
    
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:contentStr];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:8];
    [paragraphStyle setParagraphSpacing:10];
    [paragraphStyle setAlignment:NSTextAlignmentLeft];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    
    NSDictionary * attributes = @{NSFontAttributeName : self.contentView.font,
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
   
    
    [attributedString addAttributes:attributes range:NSMakeRange(0, [contentStr length])];
    [self.contentView setAttributedText:attributedString];
    
    CGSize size = [self.contentView sizeThatFits:CGSizeMake(CGRectGetWidth(self.contentView.frame), MAXFLOAT)];
    
    float itemHeight = size.height;
    
    NSLog(@"%@=%f",titleStr,itemHeight);
    
    self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, itemHeight);
    
    [self setContentSize:CGSizeMake(self.frame.size.width, CGRectGetHeight(self.contentView.frame))];

}

- (void)setContentFont:(UIFont *)font
{
    self.contentView.font = font;
}

- (void)reloadData
{
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:self.contentView.text];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:8];
    [paragraphStyle setParagraphSpacing:10];
    [paragraphStyle setAlignment:NSTextAlignmentLeft];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    
    NSDictionary * attributes = @{NSFontAttributeName : self.contentView.font,
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
    [attributedString addAttributes:attributes range:NSMakeRange(0, [self.contentView.text length])];
    [self.contentView setAttributedText:attributedString];
    
    CGSize size = [self.contentView sizeThatFits:CGSizeMake(CGRectGetWidth(self.contentView.frame), MAXFLOAT)];
    
    float itemHeight = size.height;
        
    self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, itemHeight);
    
    [self setContentSize:CGSizeMake(self.frame.size.width, CGRectGetHeight(self.contentView.frame))];
}

@end