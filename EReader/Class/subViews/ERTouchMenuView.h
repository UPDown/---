//
//  ERTouchMenuView.h
//  EReader
//
//  Created by HDJ on 13-12-1.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ERTouchMenuViewDelegate <NSObject>

- (void)touchMenuClick:(int)index;
- (void)touchMenuCanceledClick;
@end

@interface ERTouchMenuView : UIView
{
    UIView  *tabBarView;
    CGPoint currentPoint;
}
@property (nonatomic,assign)id<ERTouchMenuViewDelegate> touchMenuDelegate;

- (void)showView:(CGPoint)centerPoint;
- (void)hiddenView:(CGPoint)centerPoint;
@end
