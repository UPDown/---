//
//  GRCommentListCell.h
//  GameRaiders
//
//  Created by helfy  on 13-10-13.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRCommentObj.h"
@interface GRCommentListCell : UITableViewCell
{
    UIView *line;
    UIImageView *bgImageView;
    UIImageView *contentBgView;
    
    UILabel *autorLabel;
    UILabel *commentLabel;
    UILabel *timeLabel;
}
-(void)setDisplayObj:(GRCommentObj *)newObj withHigthLigthId:(NSString *)higthLigthId;

+(float)heigthForOb:(GRCommentObj *)newObj;
@end
