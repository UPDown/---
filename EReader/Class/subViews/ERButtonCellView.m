//
//  ERButtonCellView.m
//  EReader
//
//  Created by helfy  on 13-12-2.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import "ERButtonCellView.h"

@implementation ERButtonCellView
@synthesize titleLabel;
@synthesize iconView;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
 
        
        self.iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, self.frame.size.height-20,  self.frame.size.height-20)] autorelease];
        [self addSubview:self.iconView];
        
        self.titleLabel =[[[UILabel alloc] initWithFrame:CGRectMake( self.frame.size.height, 1, self.frame.size.width-self.frame.size.height-10,  self.frame.size.height-2)] autorelease];
        self.titleLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont systemFontOfSize:15];
        [self addSubview:self.titleLabel];
    }
    return self;
}
-(void)dealloc
{
    self.iconView= nil;
    self.titleLabel = nil;
    [super dealloc];
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    self.titleLabel.frame =CGRectMake( self.frame.size.height, 1, self.frame.size.width-self.frame.size.height-10,  self.frame.size.height-2);
    self.iconView.frame=CGRectMake(5, 5, self.frame.size.height-10,  self.frame.size.height-10);
}
-(void)setHighlighted:(BOOL)highlighted
{
    if(highlighted)
    {
        self.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.1];
        self.titleLabel.textColor = [UIColor whiteColor];
         self.iconView.frame=CGRectMake(7, 7, self.frame.size.height-14,  self.frame.size.height-14);
    }
    else{
        self.backgroundColor =[UIColor whiteColor];
         self.titleLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
         self.iconView.frame=CGRectMake(10, 10, self.frame.size.height-20,  self.frame.size.height-20);
    }
    [super setHighlighted:highlighted];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
