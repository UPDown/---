//
//  ERSettingView.h
//  EReader
//
//  Created by Hudajiang on 14-2-18.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ERSettingViewDelegate <NSObject>

- (void)colorAction:(id)sender;
- (void)fontAction:(id)sender;
- (void)nightAction:(id)sender;
- (void)lightAction:(id)sender;

@end

@interface ERSettingView : UIView
{
    //设置选项
    IBOutlet            UIButton*    colorBtn1;
    IBOutlet            UIButton*    colorBtn2;
    IBOutlet            UIButton*    colorBtn3;
    IBOutlet            UIButton*    colorBtn4;
    IBOutlet            UIButton*    colorBtn5;
    
    IBOutlet            UISegmentedControl*     fontSegment;
    IBOutlet            UISegmentedControl*     nightSegment;
    
    IBOutlet            UISlider*               lightSlider;
    
    int                 selectColorTag;
}

@property (nonatomic,assign)id<ERSettingViewDelegate>   settingDelegate;
@property (nonatomic,retain) IBOutlet            UISegmentedControl*     fontSegment;
//设置选项
- (IBAction)colorAction:(id)sender;
- (IBAction)fontAction:(id)sender;
- (IBAction)nightAction:(id)sender;
- (IBAction)lightAction:(id)sender;
-(void)readCache;
@end
