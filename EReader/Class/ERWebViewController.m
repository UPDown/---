//
//  ERWebViewController.m
//  EReader
//
//  Created by Hudajiang on 13-11-27.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import "ERWebViewController.h"

#import "MBProgressHUD.h"
#import "FileManager.h"

#import "MIImageBGView.h"
#import "QBKOverlayMenuView.h"

#import "GRCommentViewController.h"

#import <Comment/Comment.h>
#import "ERDataBase.h"
#import "IMTWebView.h"
#import "ChromeProgressBar.h"
#import "ERAppDelegate.h"
#import "ERHomeViewController.h"
#import "MIMenuCatalog.h"
#import "MIADViewer.h"
@interface ERWebViewController ()<UIWebViewDelegate,QBKOverlayMenuViewDelegate>
{
    IMTWebView *contentWebView;
    
//    int cateID;
    
    
    
    UIView *topBarView;
    ChromeProgressBar *chromeBar;
    UILabel *titleLabel;
    UIButton *dismissButton;
    UIButton *stopLoadButton;
    
    UIView *bottomBarView;
    UIButton *goBackButton;    // 后退
    UIButton *goForwardButton;  //前进
    UIButton *favButton;
    UIButton *bookMarkButton;
    UIButton *commentButton;
    
    
    float contentOffset;
    
    BOOL dragging;
    
    NSURL *loadUrl;
}
@property (nonatomic,retain) NSString    *currentBookName;
@property (nonatomic,retain) NSMutableDictionary   *currentInfo;

@end

@implementation ERWebViewController
@synthesize currentBookName;
@synthesize currentInfo;
@synthesize currentCatalogType;
@synthesize downBlock;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        cateID = 0;

    }
    return self;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    NSString *urlStr = [contentWebView.request.URL absoluteString];
    [self saveRecord:urlStr];
    [self updateLoadingStatus];
    
    contentWebView.delegate = nil;
    contentWebView.progressDelegate = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.view bringSubviewToFront:topBarView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.view.backgroundColor = kBgColor;
    
    CGRect frame = self.view.bounds;
//    QBKOverlayMenuViewOffset offset;
//    offset.topOffset = 0;
    
    if (IsIOS7) {
        frame.size.height  = frame.size.height -20;
        frame.origin.y = frame.origin.y + 20;
//         offset.topOffset = 20;
    }
  
    
    
    contentWebView = [[IMTWebView alloc] initWithFrame:frame];
    contentWebView.backgroundColor = [UIColor clearColor];
    contentWebView.delegate = self;
    contentWebView.progressDelegate = (id)self;
    for (id subView in contentWebView.subviews) {
        if([subView isKindOfClass:[UIScrollView class]])
        {
            ((UIScrollView *)subView).delegate = (id)self;
        }
    }
    
    [self.view addSubview:contentWebView];
    
    topBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44+(IsIOS7?20:0))];
    topBarView.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:242/255.0 alpha:1];
    [self.view addSubview:topBarView];
    
    dismissButton = [[[UIButton alloc] initWithFrame:CGRectMake(10, (IsIOS7?20:0), 44, 44)] autorelease];
    [dismissButton setImage:[UIImage imageNamed:@"returnButton"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    [dismissButton setImageEdgeInsets:UIEdgeInsetsMake(10, 10,10, 10)];
    [topBarView addSubview:dismissButton];
    
    titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(60,  (IsIOS7?20:0), 200, 44)] autorelease];
    [topBarView addSubview:titleLabel];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = [UIFont systemFontOfSize:16];
    titleLabel.backgroundColor = [UIColor clearColor];
    stopLoadButton = [[[UIButton alloc] initWithFrame:CGRectMake(270, (IsIOS7?20:0), 44, 44)] autorelease];
    [stopLoadButton setImage:[UIImage imageNamed:@"AddressViewReload.png"] forState:UIControlStateNormal];
   
    [stopLoadButton addTarget:self action:@selector(reloadOrStop:) forControlEvents:UIControlEventTouchUpInside];
//    [stopLoadButton setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
        [stopLoadButton setImageEdgeInsets:UIEdgeInsetsMake(10, 10,10, 10)];
    [topBarView addSubview:stopLoadButton];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 44+(IsIOS7?20:0)-1, self.view.bounds.size.width, 1.0f)];
    lineView.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.8];
    [topBarView addSubview:lineView];
    [lineView release];
    chromeBar = [[[ChromeProgressBar alloc] initWithFrame:CGRectMake(0.0f, 44+(IsIOS7?20:0)-2, self.view.bounds.size.width, 2.0f)] autorelease];
    [topBarView addSubview:chromeBar];


    

    bottomBarView =[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame)-44, 320, 44)];
    bottomBarView.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:242/255.0 alpha:1];
    [self.view addSubview:bottomBarView];
    
    
    goBackButton = [[[UIButton alloc] initWithFrame:CGRectMake(10, 0, 44, 44)] autorelease];
    [goBackButton setImage:[UIImage imageNamed:@"browserBack"] forState:UIControlStateNormal];
     [goBackButton setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [goBackButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [bottomBarView addSubview:goBackButton];
   
    goForwardButton = [[[UIButton alloc] initWithFrame:CGRectMake(70, 0, 44, 44)] autorelease];
    [goForwardButton setImage:[UIImage imageNamed:@"browserForward"] forState:UIControlStateNormal];
    [goForwardButton setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [goForwardButton addTarget:self action:@selector(goForward:) forControlEvents:UIControlEventTouchUpInside];
    [bottomBarView addSubview:goForwardButton];
    
    
    
    favButton = [[[UIButton alloc] initWithFrame:CGRectMake(130, 0, 44, 44)] autorelease];
    [favButton setImage:[UIImage imageNamed:@"favButton_n"] forState:UIControlStateNormal];
    [favButton setImage:[UIImage imageNamed:@"favButton_h"] forState:UIControlStateHighlighted];
    [favButton setImage:[UIImage imageNamed:@"favButton_s"] forState:UIControlStateSelected];
    [favButton setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    //TODO 判断是否收藏
    [favButton addTarget:self action:@selector(favAction) forControlEvents:UIControlEventTouchUpInside];
    [bottomBarView addSubview:favButton];
    
    
    commentButton= [[[UIButton alloc] initWithFrame:CGRectMake(190, 0, 44, 44)] autorelease];
    [commentButton setImage:[UIImage imageNamed:@"commentButton"] forState:UIControlStateNormal];
        [commentButton addTarget:self action:@selector(commentAction) forControlEvents:UIControlEventTouchUpInside];
    [commentButton setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [bottomBarView addSubview:commentButton];
    
    
    bookMarkButton = [[[UIButton alloc] initWithFrame:CGRectMake(250, 0, 44, 44)] autorelease];
    [bookMarkButton setImage:[UIImage imageNamed:@"bookMarkButton"] forState:UIControlStateNormal];
    [bookMarkButton addTarget:self action:@selector(bookMarkAction) forControlEvents:UIControlEventTouchUpInside];
    [bookMarkButton setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [bottomBarView addSubview:bookMarkButton];
 
    
    
    lineView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0, self.view.bounds.size.width, 1.0f)];
    lineView.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.8];
    [bottomBarView addSubview:lineView];
    [lineView release];

//    [self.view bringSubviewToFront:topBarView];

}

- (void)reloadOrStop:(id) sender {
    
     [NSObject cancelPreviousPerformRequestsWithTarget:self];
    if (contentWebView.loading)
    {
        ObjRelease(loadUrl);
        loadUrl = [contentWebView.request.URL copy];
        [contentWebView stopLoading];
    }
    else
    {
        if(loadUrl)
        {
        NSMutableURLRequest *requst= [NSMutableURLRequest requestWithURL:loadUrl cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120];
            [contentWebView loadRequest:requst];
        }
        else{
        [contentWebView reload];
        }
    }
}
- (void)goBack:(id) sender {
    [contentWebView goBack];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
//    [self performSelector:@selector(updateLoadingStatus) withObject:nil afterDelay:1.];
}

- (void)goForward:(id) sender {
    [contentWebView goForward];
     [NSObject cancelPreviousPerformRequestsWithTarget:self];
//    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateLoadingStatus) object:nil];
//    [self performSelector:@selector(updateLoadingStatus) withObject:nil afterDelay:1.];
}
-(void)commentAction
{
//        NSString *title = [contentWebView stringByEvaluatingJavaScriptFromString:@"document.title"];//获取当前页面的title
    [MobClick event:@"commentEvent" label:[self.currentInfo objectForKey:@"title"]];
   MIADViewer* adView = [MIADViewer shareADView];

    SSCCommentListViewController *commentVC =
    [self presentCommentListViewControllerWithContentId:[self.currentInfo objectForKey:@"gid"] title:[self.currentInfo objectForKey:@"title"] animated:YES];
    
    
    adView.frame = CGRectMake(0, commentVC.view.frame.size.height-adView.frame.size.height, adView.frame.size.width, adView.frame.size.height);
    CGRect frame = commentVC.view.bounds;
    float height = frame.size.height;
    
    if (IsIOS7) {
        height = height-20;
        frame.origin.y = frame.origin.y + 20;
    }
    
    frame.size.height  = height-adView.frame.size.height;
    [commentVC.view addSubview:adView];
    if(adView.hidden == NO)
    {
    for (UIView *subView in commentVC.view.subviews) {
        if([subView isKindOfClass:NSClassFromString(@"SSCCommentListView")])
        {
            subView.frame = frame;
        }
        else  if([subView isKindOfClass:NSClassFromString(@"SSCCommentPageToolbar")])
        {
            subView.frame = CGRectMake(0, subView.frame.origin.y-adView.frame.size.height,  subView.frame.size.width,  subView.frame.size.height);
        }
        
    }
    }
//    NSString *title = [contentWebView stringByEvaluatingJavaScriptFromString:@"document.title"];//获取当前页面的title
//    
//    [self presentCommentListViewControllerWithContentId:[self.currentInfo objectForKey:@"gid"] title:title animated:YES];
}
-(void)favAction
{
    
    
    if([[ERDataBase shareDBControl] queryBookIsFav:[self.currentInfo  objectForKey:@"gid"]])
    {
        
        [self.currentInfo setObject:[NSNumber numberWithBool:NO] forKey:@"fav"];
        
    } else
    {
        [self.currentInfo setObject:[NSNumber numberWithBool:YES] forKey:@"fav"];
        
    }
    favButton.selected =[[self.currentInfo objectForKey:@"fav"] boolValue];
    
    
    [[ERDataBase shareDBControl] insertBook:self.currentInfo];
    
    MBProgressHUD *hudView  = [[[MBProgressHUD alloc] initWithView:self.view] autorelease];
    hudView.removeFromSuperViewOnHide = YES;
    [self.view addSubview:hudView];
    hudView.mode = MBProgressHUDModeText;
    hudView.labelText =  favButton.selected?@"已收藏":@"取消收藏";
    [hudView show:YES];
    [hudView hide:YES afterDelay:1.0];

}

-(void)bookMarkAction
{
    NSString *title = [contentWebView stringByEvaluatingJavaScriptFromString:@"document.title"];//获取当前页面的title
    NSString *urlStr = [contentWebView stringByEvaluatingJavaScriptFromString:@"document.location.href"];
    
    //    NSString *lJs = @"document.documentElement.innerHTML";//获取当前网页的html
    //    NSString *currentHTML = [contentWebView stringByEvaluatingJavaScriptFromString:lJs];
    
    NSLog(@"Selected sharer: %@,%@", title,urlStr);
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:self.currentBookName forKey:@"bookName"];
    [dic setObject:title forKey:@"pageName"];
    [dic setObject:urlStr forKey:@"url"];
    [dic setObject:self.currentInfo forKey:@"infos"];
    [FileManager writeFavoritesFile:dic];
    [dic release];
    
    MBProgressHUD *hud = [[[MBProgressHUD alloc] initWithView:self.view] autorelease];
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = @"加入书签成功";
    [self.view addSubview:hud];
    hud.mode = MBProgressHUDModeText;
    [hud show:YES];
    [hud hide:YES afterDelay:1.0];
}

- (void) updateLoadingStatus {
    UIImage *image = nil;
    if (contentWebView.loading) {
        image = [UIImage imageNamed:@"AddressViewStop.png"];
    } else {
        image = [UIImage imageNamed:@"AddressViewReload.png"];
    }
    [stopLoadButton setImage:image forState:UIControlStateNormal];
    
    // update status of back/forward buttons
    goBackButton.enabled = [contentWebView canGoBack];
    goForwardButton.enabled = [contentWebView canGoForward];
 
}
-(void)updateTitle
{
    NSString *title = [contentWebView stringByEvaluatingJavaScriptFromString:@"document.title"];//获取当前页面的title
    
    titleLabel.text = title;
}
- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
     [NSObject cancelPreviousPerformRequestsWithTarget:self];
    self.downBlock=nil;
    self.currentInfo = nil;
    self.currentBookName = nil;
    ObjRelease(topBarView);
//    ObjRelease(chromeBar);
    contentWebView.delegate= nil;
    contentWebView.progressDelegate=nil;
    [contentWebView stopLoading];
    ObjRelease(contentWebView);
    [super dealloc];
}

//阅读地址组装（目录）
- (void)loadInfo:(NSDictionary *)infoDic withCateID:(int)_cateID
{
//    cateID = _cateID;

//    if (infoDic && infoDic.count) {
//        self.currentBookName = [infoDic objectForKey:@"title"];
//        self.currentInfo = infoDic;
//        
//        //TODO http://m.baidu.com/tc?appui=alaxs&gid=3961103225&tj=wise_novel_book_1_0_10_b1
//        //不需要 cateID
//        
//        NSString *listUrl =[infoDic objectForKey:@"listurl"];
//        
//        NSString *path = [NSString stringWithFormat:@"http://m.baidu.com/tc?appui=alaxs&tj=book_cate_1_1_1&gid=%@",[infoDic objectForKey:@"gid"]];
//        if (listUrl.length>0) {
//            path = [path stringByAppendingString:[NSString stringWithFormat:@"&src=%@",listUrl]];
//        }
//        
//        [contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
//        
//    }
}

//直接打开阅读章节
- (void)loadWebView:(NSString *)urlStr withBookName:(NSString *)_bookName withInfo:(NSDictionary *)_infos
{
    if (urlStr && ![urlStr isEqualToString:@""]) {
        self.currentBookName = _bookName;
        self.currentInfo = [NSMutableDictionary dictionaryWithDictionary:_infos];
        
//        UIButton *button = [qbkOverlayMenu buttonWithTag:2];
        favButton.selected =[[_infos objectForKey:@"fav"] boolValue];
        
        
        [contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
    }
}

- (void)closeAction:(UIButton *)sender
{
    if ([contentWebView isLoading]) {
        [contentWebView stopLoading];
    }
    contentWebView.progressDelegate = nil;
    contentWebView.delegate = nil;
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - -UIWebViewDelegate

- (void)webView:(IMTWebView *)_webView didReceiveResourceNumber:(int)resourceNumber totalResources:(int)totalResources {
    //Set progress value
    [chromeBar setProgress:((float)resourceNumber) / ((float)totalResources) animated:NO];
    
    //Reset resource count after finished
    if (resourceNumber == totalResources) {
        _webView.resourceCount = 0;
        _webView.resourceCompletedCount = 0;
        NSString *urlStr = [contentWebView.request.URL absoluteString];
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        [self performSelector:@selector(saveRecord:) withObject:urlStr afterDelay:0.5];
        [self performSelector:@selector(updateLoadingStatus) withObject:nil afterDelay:0.5];
        [self performSelector:@selector(updateTitle) withObject:nil afterDelay:0.5];
        
//        double delayInSeconds = 0.5;
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [self saveRecord:urlStr];
//            [self updateLoadingStatus];
//            [self updateTitle];
//        });
    }
}




- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{

    NSLog(@"url:%@",[request.URL absoluteString]);
    if ([[request.URL absoluteString] rangeOfString:@"/download?"].location != NSNotFound) {
        NSLog(@"下载！！！！！");
      if(self.downBlock)
      {
            self.downBlock();
      }
        [self closeAction:Nil];
        return NO;
        
    }

    if ([[request.URL absoluteString] rangeOfString:@"#!/dir/"].location != NSNotFound) {
        NSLog(@"目录！！！！！");
        [self closeAction:Nil];
        return NO;
        
    }
    if ([[request.URL absoluteString] rangeOfString:@"/tpl/pocket"].location != NSNotFound) {
        NSLog(@"书包！！！！！");
        UIViewController *homeVc =(UIViewController *)[(ERAppDelegate *)[[UIApplication sharedApplication] delegate] homeViewController];
        [homeVc.navigationController popToRootViewControllerAnimated:NO];
        [self closeAction:Nil];
        return NO;
        
    }
    if ([[request.URL absoluteString] rangeOfString:@"http://m.baidu.com/book?tj="].location != NSNotFound) {
        NSLog(@"书城！！！！！");
        [self closeAction:Nil];
        
        ERHomeViewController *homeVc =(ERHomeViewController *)[(ERAppDelegate *)[[UIApplication sharedApplication] delegate] homeViewController];
        [homeVc.navigationController popToRootViewControllerAnimated:NO];
        
        MIMenuCatalog *catalog = [[[MIMenuCatalog alloc] init] autorelease];
        catalog.title = @"排行";
        catalog.type = MIMenuCatalogRank;
        [homeVc changeCatalog:catalog];
        return NO;
       
        
    }
    if ([[request.URL absoluteString] rangeOfString:@"http://zhidao.baidu.com/mmisc/"].location != NSNotFound) {
        NSLog(@"同类小说！！！！！");
        UIViewController *homeVc =(UIViewController *)[(ERAppDelegate *)[[UIApplication sharedApplication] delegate] homeViewController];
        [homeVc.navigationController popToRootViewControllerAnimated:NO];
        [self closeAction:Nil];
        return NO;
    }
//    if ([[request.URL absoluteString] rangeOfString:@"http://itunes.apple.com/cn/app"].location != NSNotFound) {
//        NSLog(@"同类小说！！！！！");
////        [self closeAction:Nil];
//        return NO;
//        
//        
//    }
//    if ([[request.URL absoluteString] rangeOfString:@"http://m.baidu.com/searchbox"].location != NSNotFound) {
//        NSLog(@"同类小说！！！！！");
////        [self closeAction:Nil];
//        return NO;
//    
//    }
    
     chromeBar.hidden = NO;
    chromeBar.progress =0.05;
    return YES;
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    [self.view bringSubviewToFront:topBarView];
    titleLabel.text = @"正在加载...";
    
    [self updateLoadingStatus];
    [self hiddenBar:NO];
}

-(void)saveRecord:(NSString *)urlStr
{
    
    
    
    NSArray *subStrArray = [urlStr componentsSeparatedByString:@"&"];
    for (NSString *gidStr in subStrArray) {
        if([gidStr hasPrefix:@"gid="])
        {
            NSString *gid =[gidStr stringByReplacingOccurrencesOfString:@"gid=" withString:@""];
            NSArray *subGidStrArray = [gid componentsSeparatedByString:@"#!/zw/"];
            gid=[subGidStrArray objectAtIndex:0];
            if([gid isEqualToString:[currentInfo objectForKey:@"gid"]])
            {
                NSString *title = [contentWebView stringByEvaluatingJavaScriptFromString:@"document.title"];//获取当前页面的title
                NSString *urlStr = [contentWebView stringByEvaluatingJavaScriptFromString:@"document.location.href"];
                [currentInfo setObject:title?title:[currentInfo objectForKey:@"title"] forKey:@"lastRedTitle"];
                [currentInfo setObject:urlStr forKey:@"lastRedUrl"];
                
                [[ERDataBase shareDBControl] insertBook:currentInfo];
            }
            
        }
    }

}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
//        NSString *lJs = @"document.documentElement.innerHTML";//获取当前网页的html
//    NSString *title = [contentWebView stringByEvaluatingJavaScriptFromString:lJs];//获取当前页面的title
//
//    CFStringEncoding cfenc = CFStringConvertNSStringEncodingToEncoding(encoding);
//    
//    NSLog(@"%@",title);
//    
//    NSString *meta = [NSString stringWithFormat:@"document.getElementsById(\"div\").className(\"xs-chapterPage-bottom\").content = \"\""];
//    [webView stringByEvaluatingJavaScriptFromString:meta];
//
    //TODO
//  NSLog(@"%@",[contentWebView stringByEvaluatingJavaScriptFromString:@"document.title"]);
    NSString *urlStr = [webView.request.URL absoluteString];
    
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(saveRecord:) withObject:urlStr afterDelay:1];
     [self performSelector:@selector(updateLoadingStatus) withObject:nil afterDelay:1.];
    [self performSelector:@selector(updateTitle) withObject:nil afterDelay:1];
    
//    NSArray *subStrArray = [urlStr componentsSeparatedByString:@"&"];
//    for (NSString *gidStr in subStrArray) {
//        if([gidStr hasPrefix:@"gid="])
//        {
//            NSString *gid =[gidStr stringByReplacingOccurrencesOfString:@"gid=" withString:@""];
//            if([gid isEqualToString:[currentInfo objectForKey:@"gid"]])
//            {
//                NSString *title = [contentWebView stringByEvaluatingJavaScriptFromString:@"document.title"];//获取当前页面的title
//                NSString *urlStr = [contentWebView stringByEvaluatingJavaScriptFromString:@"document.location.href"];
//                
//                [currentInfo setObject:title?title:[currentInfo objectForKey:@"title"] forKey:@"lastRedTitle"];
//                [currentInfo setObject:urlStr forKey:@"lastRedUrl"];
//                
//                [[ERDataBase shareDBControl] insertBook:currentInfo];
//            }
//            
//        }
//    }
    
//    if()
    
//    if( [[ERDataBase shareDBControl] queryBookExist:[infoDic objectForKey:@"gid"]])
//    {
//        NSMutableDictionary *dic= [NSMutableDictionary dictionaryWithDictionary: [[ERDataBase shareDBControl] queryBookforGid:[infoDic objectForKey:@"gid"]]];
//        [dic setObject:[NSNumber numberWithBool:YES] forKey:@"fav"];
//        [[ERDataBase shareDBControl] updateBookWithDic:dic];
//    }
//    else{
//        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
//        [dic setObject:[infoDic objectForKey:@"title"] forKey:@"title"];
//        [dic setObject:[infoDic objectForKey:@"author"] forKey:@"author"];
//        [dic setObject:[infoDic objectForKey:@"gid"] forKey:@"gid"];
//        [dic setObject:[infoDic objectForKey:@"status"] forKey:@"status"];
//        [dic setObject:[NSNumber numberWithBool:YES] forKey:@"fav"];
//        [dic setObject:@"" forKey:@"lastRedUrl"];
//        [dic setObject:@"" forKey:@"lastRedTitle"];
//        
//        [[ERDataBase shareDBControl] insertBook:dic];
//        [dic release];
//    }
//    
//    [self.currentInfo setObject:[NSNumber numberWithBool:YES] forKey:@"fav"];
    [[ERDataBase shareDBControl] insertBook:self.currentInfo];
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
        [chromeBar hideWithFadeOut];
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        [self performSelector:@selector(updateLoadingStatus) withObject:nil afterDelay:1.];
        titleLabel.text = @"加载失败...";
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    if ([contentWebView isLoading]) {
        [contentWebView stopLoading];
    }
    contentWebView.delegate = nil;
    contentWebView.progressDelegate = nil;
}

-(void)hiddenBar:(BOOL)hidden
{
    [UIView beginAnimations:Nil context:Nil];
    if(hidden)
    {
    topBarView.frame = CGRectMake(0, -(CGRectGetHeight(topBarView.frame)), CGRectGetWidth(topBarView.frame), CGRectGetHeight(topBarView.frame));
    bottomBarView.frame = CGRectMake(0, (CGRectGetHeight(self.view.frame)), CGRectGetWidth(bottomBarView.frame), CGRectGetHeight(bottomBarView.frame));
    }
    else{
        topBarView.frame = CGRectMake(0, 0, CGRectGetWidth(topBarView.frame), CGRectGetHeight(topBarView.frame));
        bottomBarView.frame = CGRectMake(0, (CGRectGetHeight(self.view.frame)-CGRectGetHeight(bottomBarView.frame)), CGRectGetWidth(bottomBarView.frame), CGRectGetHeight(bottomBarView.frame));
    
    }
    [UIView commitAnimations];
}
//TODO

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(dragging && contentWebView.loading==NO)
    {
        if(contentOffset > scrollView.contentOffset.y) // 往上
        {
            [self hiddenBar:NO];
        }
        else{
            [self hiddenBar:YES];
        }
        dragging = NO;
    }
    
    contentOffset =scrollView.contentOffset.y;
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    dragging = YES;
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    dragging = NO;
}
@end
