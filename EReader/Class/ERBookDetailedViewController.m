//
//  ERBookDetailedViewController.m
//  EReader
//
//  Created by helfy  on 13-12-1.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import "ERBookDetailedViewController.h"
#import "MIImageBGView.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "ERButtonCellView.h"
#import "ERWebViewController.h"
#import "GRCommentViewController.h"
#import "FileManager.h"
#import "MBProgressHUD.h"
#import <Comment/Comment.h>
#import "NSString+conver.h"
#import "ASIHTTPRequest.h"
#import "SBJson.h"
#import "ERDataBase.h"
#import "ERPageChangeView.h"
#import "EROfflineBookObj.h"
#import "EROfflineDownManger.h"
#import "NSString+conver.h"
#import "PICircularProgressView.h"
#import "MIADViewer.h"
#import "ERReadViewController.h"
#define NumPage     20

@interface ERBookDetailedViewController ()
{
    UITableView *contentScroller;
    UITableView *catalogListTable;
    
    UIView *headView;
    UIImageView *bookIconImageView;
    UILabel *sourceLabel;
    UILabel *authorLabel;
    UILabel *summaryLabelView;
    UILabel *statusLabel;
    ERButtonCellView *lastChapter;
    UIButton *chaseBookButton;
    
    UIButton *commentButton;//评论按钮

    ERButtonCellView *catalogButton;
    ERButtonCellView *downloadButton;
    NSMutableDictionary    *infoDic;
    int             cateID;
    CatalogType             currentCatalogType;
    
    MBProgressHUD   *hudView;
    ASIHTTPRequest *request;
    
    
    NSMutableArray *catalogArray;
    UIView *catalogBar;
    UIButton *desButton;
    UIButton *listButton;
    UIButton *sortButton;
    UIButton *changePageButton;
    
    NSMutableDictionary *favDic;
    
    BOOL isSelect;
    int     currentIndex;
    int     totalNum;
    
    ERPageChangeView *pageView;
    
    EROfflineBookObj *offlineBookElement;
    PICircularProgressView *progressView;
    BOOL waitRead;
    
    MIADViewer *adView;
}
@end

@implementation ERBookDetailedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        infoDic = [[NSMutableDictionary alloc] init];
        cateID = 0;
        isSelect = YES;
        waitRead = NO;
        currentIndex = 0;
        totalNum= 0;
        currentCatalogType = MIMenuCatalogHome;
        catalogArray = [[NSMutableArray alloc] init];
        favDic = [[NSMutableDictionary alloc] init];
        adView = [MIADViewer shareADView];

    }
    return self;
}

-(void)dealloc
{
    
    if(request)
    {
        [request setDelegate:nil];
        [request cancel];
        ObjRelease(request);
    }
    [offlineBookElement removeTarget:(id)self];
    ObjRelease(offlineBookElement);
    ObjRelease(progressView);
    ObjRelease(favDic)
    ObjRelease(catalogArray);
    ObjRelease(catalogListTable);
    ObjRelease(headView);
    ObjRelease(infoDic);
    ObjRelease(contentScroller);
    ObjRelease(bookIconImageView);
    ObjRelease(sourceLabel);
    ObjRelease(authorLabel);
    ObjRelease(summaryLabelView);
    ObjRelease(statusLabel);
    ObjRelease(statusLabel);
    ObjRelease(lastChapter);
    ObjRelease(commentButton);
    ObjRelease(chaseBookButton);
    ObjRelease(hudView);
    ObjRelease(listButton);
    ObjRelease(sortButton);
    ObjRelease(desButton);
    [super dealloc];
}

- (void)referenceData
{
    [self loadDetaiData:infoDic withCateID:cateID withCatalogType:currentCatalogType];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    MIImageBGView *backImage = [[[MIImageBGView alloc] initWithFrame:self.view.bounds] autorelease];
    [self.view addSubview:backImage];
	// Do any additional setup after loading the view.

    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(referenceData)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    CGRect frame = self.view.bounds;
    float height = frame.size.height - self.navigationController.navigationBar.frame.size.height;

    if (IsIOS7) {
        frame.origin.y =self.navigationController.navigationBar.frame.size.height;
        height = height-20;
        frame.origin.y = frame.origin.y + 20;
    }
    else {
        rightItem.tintColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.9];
    }
    
    frame.size.height  = height;
    
    

    
    
    contentScroller = [[UITableView alloc] initWithFrame:frame];
    contentScroller.backgroundColor = [UIColor clearColor];
    contentScroller.delegate = (id)self;
    [self.view addSubview:contentScroller];
    
    headView = [[UIView alloc] initWithFrame:contentScroller.bounds];
    headView.layer.masksToBounds = YES;
    bookIconImageView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 20, 96, 120)];
    [headView addSubview:bookIconImageView];
    
    
 
    authorLabel =[[UILabel alloc] initWithFrame:CGRectMake(125, 20, 320-130, 16)];
    [authorLabel setBackgroundColor:[UIColor clearColor]];
    [authorLabel setTextColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1]];
    [authorLabel setFont:[UIFont systemFontOfSize:14]];
    [headView addSubview:authorLabel];
    
    statusLabel =[[UILabel alloc] initWithFrame:CGRectMake(125, 40, 320-130, 16)];
    [statusLabel setBackgroundColor:[UIColor clearColor]];
    [statusLabel setTextColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1]];
    [statusLabel setFont:[UIFont systemFontOfSize:14]];
    [headView addSubview:statusLabel];
    
    sourceLabel =[[UILabel alloc] initWithFrame:CGRectMake(125, 60, 320-130, 16)];
    [sourceLabel setBackgroundColor:[UIColor clearColor]];
    [sourceLabel setTextColor:[UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:0.5]];
    [sourceLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [headView addSubview:sourceLabel];
    
    
    commentButton = [[UIButton alloc] initWithFrame:CGRectMake(110, 108, 60, 32)];
    [commentButton setTitle:@"评论" forState:UIControlStateNormal];
    [commentButton setTitleColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1] forState:UIControlStateNormal];
    [headView addSubview:commentButton];
    [commentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
    
    commentButton.layer.borderColor =[UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:0.4].CGColor ;
    commentButton.layer.cornerRadius = 3;
    commentButton.layer.borderWidth=1;
//
    
    chaseBookButton = [[UIButton alloc] initWithFrame:CGRectMake(180, 108, 60, 32)];
    [chaseBookButton setTitle:@"收藏" forState:UIControlStateNormal];
    [chaseBookButton addTarget:self action:@selector(chaseBook:) forControlEvents:UIControlEventTouchUpInside];
    [chaseBookButton setTitleColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1] forState:UIControlStateNormal];
    [headView addSubview:chaseBookButton];
    chaseBookButton.layer.borderColor =[UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:0.4].CGColor ;
    chaseBookButton.layer.cornerRadius = 3;
    chaseBookButton.layer.borderWidth=1;

    UIButton *shareButton = [[UIButton alloc] initWithFrame:CGRectMake(250, 108, 60, 32)];
    [shareButton setTitle:@"分享" forState:UIControlStateNormal];
    [shareButton setTitleColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1] forState:UIControlStateNormal];
    [headView addSubview:shareButton];
    [shareButton addTarget:self action:@selector(shareAciton:) forControlEvents:UIControlEventTouchUpInside];
    
    shareButton.layer.borderColor =[UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:0.4].CGColor ;
    shareButton.layer.cornerRadius = 3;
    shareButton.layer.borderWidth=1;
    
   
    downloadButton=[[ERButtonCellView alloc] initWithFrame:CGRectMake(0, 150, 320, 40)];
    downloadButton.titleLabel.text = @"离线阅读";
    downloadButton.backgroundColor = [UIColor whiteColor];
    [downloadButton setTitleColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1] forState:UIControlStateNormal];
    [downloadButton addTarget:self action:@selector(offLineRead:) forControlEvents:UIControlEventTouchUpInside];
    [headView addSubview:downloadButton];
    downloadButton.layer.borderColor =[UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:0.4].CGColor ;
    downloadButton.layer.cornerRadius = 3;
    downloadButton.layer.borderWidth=1;
    [downloadButton.iconView setImage:[UIImage imageNamed:@"ERBookIcon_down.png"]];
    
    
    catalogButton =[[ERButtonCellView alloc] initWithFrame:CGRectMake(0, 189, 320, 40)];
    catalogButton.titleLabel.text = @"在线阅读";
    catalogButton.backgroundColor = [UIColor whiteColor];
    [catalogButton setTitleColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1] forState:UIControlStateNormal];
    [catalogButton addTarget:self action:@selector(loadWebView:) forControlEvents:UIControlEventTouchUpInside];
    [headView addSubview:catalogButton];
    catalogButton.layer.borderColor =[UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:0.4].CGColor ;
    catalogButton.layer.cornerRadius = 3;
    catalogButton.layer.borderWidth=1;
    [catalogButton.iconView setImage:[UIImage imageNamed:@"ERBookIcon.png"]];
    
    
    lastChapter =[[ERButtonCellView alloc] initWithFrame:CGRectMake(0, 228, 320, 40)];
    //    [lastChapter setTitle:@"" forState:UIControlStateNormal];
    [lastChapter setTitleColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1] forState:UIControlStateNormal];
    [headView addSubview:lastChapter];
        lastChapter.backgroundColor = [UIColor whiteColor];
    [lastChapter addTarget:self action:@selector(loadLastChapterWebView:) forControlEvents:UIControlEventTouchUpInside];
    [lastChapter.iconView setImage:[UIImage imageNamed:@"ERNewChapter.png"]];
    lastChapter.layer.borderColor =[UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:0.4].CGColor ;
    lastChapter.layer.cornerRadius = 3;
    lastChapter.layer.borderWidth=1;
    
    catalogBar =[[UIView alloc] initWithFrame:CGRectMake(0, 240, 320, 40)];
    catalogBar.backgroundColor = [UIColor whiteColor];
    [headView addSubview:catalogBar];
    desButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
    [desButton setTitle:@"简介" forState:UIControlStateNormal];
    [desButton setTitleColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1] forState:UIControlStateNormal];
    [desButton setTitleColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1] forState:UIControlStateSelected];
    [desButton addTarget:self action:@selector(changeCatalog:) forControlEvents:UIControlEventTouchUpInside];
    [catalogBar addSubview:desButton];
    
    UIView *lineView =[[[UIView alloc] initWithFrame:CGRectMake(60, 10, 1, 20)] autorelease];
    lineView.backgroundColor = [UIColor blackColor];
    [catalogBar addSubview:lineView];
    
    listButton = [[UIButton alloc] initWithFrame:CGRectMake(61, 0, 60, 40)];
    [listButton setTitle:@"目录" forState:UIControlStateNormal];
    [listButton setTitleColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1] forState:UIControlStateNormal];
    [listButton setTitleColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1] forState:UIControlStateSelected];
    [listButton addTarget:self action:@selector(changeCatalog:) forControlEvents:UIControlEventTouchUpInside];
    
    [catalogBar addSubview:listButton];
    
    //排序
    sortButton = [[UIButton alloc] initWithFrame:CGRectMake(230, 0, 80, 40)];
    sortButton.hidden = YES;
    sortButton.selected = YES;
    [sortButton setTitle:@"正序" forState:UIControlStateNormal];
    [sortButton setTitle:@"反序" forState:UIControlStateSelected];
    [sortButton setTitleColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1] forState:UIControlStateNormal];
    [sortButton setTitleColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1] forState:UIControlStateSelected];
    [sortButton addTarget:self action:@selector(sortListAction:) forControlEvents:UIControlEventTouchUpInside];
    [catalogBar addSubview:sortButton];

    
    summaryLabelView = [[UILabel alloc] initWithFrame:CGRectMake(15, 240, 320-30, 20)];
    [summaryLabelView setBackgroundColor:[UIColor clearColor]];
    [summaryLabelView setTextColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1]];
    [summaryLabelView setFont:[UIFont systemFontOfSize:15]];
    summaryLabelView.numberOfLines =MAXFLOAT;
    [headView addSubview:summaryLabelView];
    
    [contentScroller setTableHeaderView:headView];
    contentScroller.separatorStyle=UITableViewCellSeparatorStyleNone;
    
//    CGRect catalogRect = frame;
//    catalogRect.size.height =frame.size.height - 50;
    catalogListTable = [[UITableView alloc] initWithFrame:frame];
    catalogListTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    catalogListTable.backgroundColor = [UIColor clearColor];
    catalogListTable.dataSource = (id)self;
    catalogListTable.delegate = (id)self;
    [self.view addSubview:catalogListTable];

    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    footerView.backgroundColor = [UIColor whiteColor];
    footerView.userInteractionEnabled = YES;
    
    UIButton *upButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [upButton addTarget:self action:@selector(upPageAction:) forControlEvents:UIControlEventTouchUpInside];
    upButton.frame = CGRectMake(10, 5, 60, 40);
    [upButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [upButton setTitle:@"上一页" forState:UIControlStateNormal];
    [footerView addSubview:upButton];
    
    UIButton *downButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [downButton addTarget:self action:@selector(downPageAction:) forControlEvents:UIControlEventTouchUpInside];
    downButton.frame = CGRectMake(250, 5, 60, 40);
    [downButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [downButton setTitle:@"下一页" forState:UIControlStateNormal];
    [footerView addSubview:downButton];
    
    changePageButton = [[UIButton alloc] init];
    [changePageButton addTarget:self action:@selector(changePageAction:) forControlEvents:UIControlEventTouchUpInside];
    changePageButton.frame = CGRectMake(110, 5, 100, 40);
    [changePageButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [changePageButton setTitle:@"0/0" forState:UIControlStateNormal];
    [footerView addSubview:changePageButton];

    [catalogListTable setTableFooterView:footerView];
    [footerView release];
    
    [self changeCatalog:desButton];
    [self changeCatalog:desButton.selected?desButton:catalogButton];
    


}
-(void)viewWillAppear:(BOOL)animated
{
//    [self reloadData];
    if( [[ERDataBase shareDBControl] queryBookExist:[infoDic objectForKey:@"gid"]])
    {
        NSMutableDictionary *dic= [NSMutableDictionary dictionaryWithDictionary: [[ERDataBase shareDBControl] queryBookforGid:[infoDic objectForKey:@"gid"]]];
        NSString *readTitle = [dic objectForKey:@"lastRedTitle"];
        NSString *readUrl = [dic objectForKey:@"lastRedUrl"];
        if(readTitle.length >0 && readUrl.length>0)
        {
            catalogButton.titleLabel.text = [NSString stringWithFormat:@"已读 : %@ ",readTitle];
        }
    }
    else{
        NSString *reason =  [infoDic objectForKey:@"reason"];
        if(reason && reason.length >0)
        {
            catalogButton.titleLabel.text = [NSString stringWithFormat:@"在线阅读 ( %@ )",reason];
        }
    }
    
    
    [offlineBookElement addTarget:(id)self];
    [adView setDelegate:(id)self];
    [self.view addSubview:adView];
    if(adView.hidden == NO)
    {
        [self adLoadFilish:adView];
    }
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [adView setDelegate:nil];
    [super viewWillDisappear:animated];
    [offlineBookElement removeTarget:(id)self];
    [self SelectPageAndCloseView];
    waitRead = NO;
}


-(void)adLoadFilish:(MIADViewer*)view
{
    view.frame = CGRectMake(0, self.view.frame.size.height-view.frame.size.height, view.frame.size.width, view.frame.size.height);
    [self.view addSubview:view];
    
    CGRect frame = self.view.bounds;
    float height = frame.size.height - self.navigationController.navigationBar.frame.size.height;
    
    if (IsIOS7) {
        frame.origin.y =self.navigationController.navigationBar.frame.size.height;
        height = height-20;
        frame.origin.y = frame.origin.y + 20;
    }
    else{
        height = height + self.navigationController.navigationBar.frame.size.height;
    }
    
    frame.size.height =height - view.frame.size.height;
    catalogListTable.frame = frame;
    contentScroller.frame = frame;
}
-(void)loadAdError:(NSString *)error
{
    adView.frame = CGRectMake(0, self.view.frame.size.height-adView.frame.size.height, adView.frame.size.width, adView.frame.size.height);
    [self.view addSubview:adView];
    
    CGRect frame = self.view.bounds;
    float height = frame.size.height - self.navigationController.navigationBar.frame.size.height;
    
    if (IsIOS7) {
        frame.origin.y =self.navigationController.navigationBar.frame.size.height;
        height = height-20;
        frame.origin.y = frame.origin.y + 20;
    }
    else{
        height = height + self.navigationController.navigationBar.frame.size.height;
    }
    
    frame.size.height =height - adView.frame.size.height;
    catalogListTable.frame = frame;
    contentScroller.frame = frame;
}


-(void)setrefDataFinsihBlock:(void (^)(NSDictionary *dic))newRefDataFinish
{
    refDataFinish = Block_copy(newRefDataFinish);
}
#pragma mark -Page
- (void)upPageAction:(UIButton *)button
{
    if (currentIndex > 0) {
        --currentIndex;
        [changePageButton setTitle:[NSString stringWithFormat:@"%i/%i",currentIndex+1,(int)ceilf(totalNum/(NumPage*1.0))] forState:UIControlStateNormal];
        
        [catalogListTable reloadData];
    }
}

- (void)downPageAction:(UIButton *)button
{
    if (currentIndex+1 < (int)ceilf(totalNum/(NumPage*1.0))){
        ++currentIndex;
        [changePageButton setTitle:[NSString stringWithFormat:@"%i/%i",currentIndex+1,(int)ceilf(totalNum/(NumPage*1.0))] forState:UIControlStateNormal];
        
        [catalogListTable reloadData];
    }
}

- (void)changePageAction:(UIButton *)button
{
    CGRect frame = self.view.bounds;

    if (IsIOS7) {
        float height = frame.size.height - self.navigationController.navigationBar.frame.size.height-20;
        frame.origin.y =self.navigationController.navigationBar.frame.size.height+20;
        frame.size.height  = height;
    }
    
    if (!pageView) {
        
        pageView = [[[[NSBundle mainBundle] loadNibNamed:@"PageChangeView" owner:self options:nil] lastObject] retain];
        pageView.frame = frame;
        pageView.pageDelegate = (id)self;
        pageView.selectNum = currentIndex;
        [pageView loadDataWithNumCount:ceilf(totalNum/(NumPage*1.0))];
        [self.view addSubview:pageView];
//        [pageView.pickerView reloadAllComponents];
    }
}

- (void)SelectPageAndCloseView
{
    if (pageView) {
        [pageView removeFromSuperview];
        [pageView release];
    }
    pageView = nil;
}
-(void)selectPage:(int)index
{
    if(index>=0 && index<totalNum)
    {
        currentIndex=index;
        [changePageButton setTitle:[NSString stringWithFormat:@"%i/%i",currentIndex+1,(int)ceilf(totalNum/(NumPage*1.0))] forState:UIControlStateNormal];
        [catalogListTable reloadData];
    }
    
}
#pragma mark  -ASIHTTPRequest
- (void)getRequestFailed:(ASIHTTPRequest *)theRequest
{
        self.navigationItem.rightBarButtonItem.enabled = YES;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)getRequestComplete:(ASIHTTPRequest *)theRequest
{
            self.navigationItem.rightBarButtonItem.enabled = YES;
    NSString *resultStr = [request responseString];
    
    NSDictionary *rootDic = [resultStr JSONValue];
    
    NSDictionary *data = [rootDic objectForKey:@"data"];
    if(data)
    {
        NSArray *group = [data objectForKey:@"group"];
        if(group)
        {
            [catalogArray addObjectsFromArray:group];
            totalNum =group.count;
        }
        [infoDic addEntriesFromDictionary:data];
        [catalogListTable reloadData];
    }
    currentCatalogType=MIMenuCatalogHome;
    
    [changePageButton setTitle:[NSString stringWithFormat:@"%i/%i",currentIndex+1,(int)ceilf(totalNum/(NumPage*1.0))] forState:UIControlStateNormal];
    
    [self reloadData];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if(refDataFinish)
    {
    refDataFinish(infoDic);
    Block_release(refDataFinish);
    refDataFinish = nil;
    }
}

-(void)reloadData
{
    
    //标题 作者  gid 状态
    if( [[ERDataBase shareDBControl] queryBookExist:[infoDic objectForKey:@"gid"]])
    {
        [favDic addEntriesFromDictionary: [[ERDataBase shareDBControl] queryBookforGid:[infoDic objectForKey:@"gid"]]];

    }
    else{
        NSString *title =[infoDic objectForKey:@"title"];
        if(title)
        {
            [favDic setObject:title forKey:@"title"];
        }
        NSString *author =[infoDic objectForKey:@"author"];
        if(author)
        {
        [favDic setObject:author forKey:@"author"];
        }
        NSString *gid =[infoDic objectForKey:@"gid"];
        if(gid)
        {
            [favDic setObject:gid forKey:@"gid"];
        }
        NSString *status =[infoDic objectForKey:@"status"];
        if(status)
        {
            [favDic setObject:status forKey:@"status"];
        }
        [favDic setObject:[NSNumber numberWithBool:NO] forKey:@"fav"];
        [favDic setObject:@"" forKey:@"lastRedUrl"];
        [favDic setObject:@"" forKey:@"lastRedTitle"];
    }
    
    self.navigationItem.title = [infoDic objectForKey:@"title"];
    
    NSString *urlStr = [[infoDic objectForKey:@"coverImage"] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
    [bookIconImageView setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:@"book_cover.png"] options:SDWebImageRetryFailed |
     SDWebImageProgressiveDownload andResize:CGSizeMake(96, 120)];
    
    //    [sourceLabel setText:[NSString stringWithFormat:@"来源：%@",[detailInfo objectForKey:@"listurl"]]];
    [authorLabel setText:[NSString stringWithFormat:@"作者：%@",[infoDic objectForKey:@"author"]]];
    if([NSString_conver NULLStrChangeToStr:[infoDic objectForKey:@"status"]])
    {
        [statusLabel setText:[NSString stringWithFormat:@"状态：%@",[infoDic objectForKey:@"status"]]];
    }
 
    NSString *size = [NSString_conver NULLStrChangeToStr:[infoDic objectForKey:@"size"]];
    if(size && [size integerValue]>0)
    {
    downloadButton.titleLabel.text = [NSString stringWithFormat:@"离线阅读 (%.2fM)",(float)[size intValue]/1024/1024 ];
    }
    NSDictionary *lastChapterDic= [infoDic objectForKey:@"lastChapter"];
    lastChapter.hidden = NO;
    if(lastChapterDic)
    {
        lastChapter.titleLabel.text =[NSString_conver NULLStrChangeToStr:[lastChapterDic objectForKey:@"text"]];
        if( lastChapter.titleLabel.text==nil || lastChapter.titleLabel.text.length <1)
        {
            lastChapter.hidden = YES;
        }
    }
    else{
        lastChapter.hidden = YES;
    }
    if(lastChapter.hidden==NO)
    {
       summaryLabelView.frame = CGRectMake(15, 320, 320-30, 20);
    }
    else
    {
           summaryLabelView.frame = CGRectMake(15, 280, 320-30, 20);
    }
    if([[ERDataBase shareDBControl] queryBookIsFav:[infoDic objectForKey:@"gid"]])
    {
        chaseBookButton.enabled = NO;
        [chaseBookButton setTitle:@"已收藏" forState:UIControlStateNormal];
    }
    else {
        chaseBookButton.enabled = YES;
         [chaseBookButton setTitle:@"收藏" forState:UIControlStateNormal];
    }
    
    summaryLabelView.text = [infoDic objectForKey:@"summary"];
    
    if(summaryLabelView.text.length==0)
    {
        summaryLabelView.text = @"该来源暂无简介";
    }
    CGSize textSize = [summaryLabelView.text sizeWithFont:summaryLabelView.font constrainedToSize:CGSizeMake(summaryLabelView.frame.size.width, MAXFLOAT)];
    [summaryLabelView setFrame:CGRectMake(summaryLabelView.frame.origin.x, summaryLabelView.frame.origin.y, summaryLabelView.frame.size.width,  textSize.height)];
    
    if( [[ERDataBase shareDBControl] queryBookExist:[infoDic objectForKey:@"gid"]])
    {
        NSMutableDictionary *dic= [NSMutableDictionary dictionaryWithDictionary: [[ERDataBase shareDBControl] queryBookforGid:[infoDic objectForKey:@"gid"]]];
        NSString *readTitle = [dic objectForKey:@"lastRedTitle"];
        NSString *readUrl = [dic objectForKey:@"lastRedUrl"];
        if(readTitle.length >0 && readUrl.length>0)
        {
            catalogButton.titleLabel.text = [NSString stringWithFormat:@"已读 : %@ ",readTitle];
        }
    }
    else{
    NSString *reason =  [infoDic objectForKey:@"reason"];
    if(reason && reason.length >0)
    {
        catalogButton.titleLabel.text = [NSString stringWithFormat:@"在线阅读 ( %@ )",reason];
    }
    }

//    [contentScroller setTableHeaderView:headView];
    if(contentScroller.hidden == NO)
    {
          [self scrollViewDidScroll:contentScroller];

        headView.frame = CGRectMake(0, 0,320, summaryLabelView.frame.size.height+ summaryLabelView.frame.origin.y+50);
        [contentScroller setTableHeaderView:headView];
        [self scrollViewDidScroll:contentScroller];
    }
    else{
          [self scrollViewDidScroll:catalogListTable];
        headView.frame = CGRectMake(0, 0,320, summaryLabelView.frame.origin.y-45 +catalogBar.frame.size.height+5);
        [catalogListTable setTableHeaderView:headView];
        [catalogListTable reloadData];
        [self scrollViewDidScroll:catalogListTable];
    }
  
    [offlineBookElement removeTarget:(id)self];
    ObjRelease(offlineBookElement);
    
    offlineBookElement = [[[ERDataBase shareDBControl] queryOfflineBookWithGid:[infoDic objectForKey:@"gid"]] retain];
    if(offlineBookElement.gId)
    {
        EROfflineBookObj *downBook = [[EROfflineDownManger shareManger] getBookObjWithGid:offlineBookElement.gId];
        if(downBook)
        {
            ObjRelease(offlineBookElement);
            offlineBookElement =[downBook retain];
        }
    }
    else
    {
        ObjRelease(offlineBookElement);
        offlineBookElement=[[EROfflineBookObj alloc] init];
        offlineBookElement.gId=[infoDic objectForKey:@"gid"];
        offlineBookElement.author =[infoDic objectForKey:@"author"];
        offlineBookElement.title =[infoDic objectForKey:@"title"];
        offlineBookElement.size = [infoDic objectForKey:@"size"];
        offlineBookElement.downLoadStatus = EROfflineBookDownLoadStatusIsUnDown;
    }
    [offlineBookElement addTarget:(id)self];
    [self loadProgressView];
    switch (offlineBookElement.downLoadStatus) {
        case EROfflineBookDownLoadStatusIsUnDown:
        {
            progressView.hidden = YES;
        }
            break;
        case EROfflineBookLoadStatusIsWait:
        {
            [progressView setShowTextSring:@"等待"];
             [self EROfflineBookDownProgress:[offlineBookElement downProgress]];
            progressView.hidden = NO;
        }
            break;
        case EROfflineBookLoadStatusIsDowning:
        {
            progressView.hidden = NO;
            [self EROfflineBookDownProgress:[offlineBookElement downProgress]];
            break;
        }
        case EROfflineBookDownLoadStatusIsPaused:
        {
         
            [progressView setShowTextSring:@"暂停"];
          
             [self EROfflineBookDownProgress:[offlineBookElement downProgress]];
            progressView.hidden = NO;
        }
            break;
        case EROfflineBookDownLoadStatusIsDowned:
        {
            progressView.hidden = YES;
        }
            break;

    }
  
}

- (void)loadDetaiData:(NSDictionary *)detailInfo withCateID:(int)_cateID withCatalogType:(CatalogType)catalogType
{
    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.view.backgroundColor = kBgColor;
    cateID = _cateID;
    currentCatalogType = catalogType;
    
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.removeFromSuperViewOnHide = YES;;
    [self.view addSubview:hudView];
    hudView.labelText = @"获取中";
    [hudView show:YES];

    
    [request setDelegate:nil];
	[request cancel];
    NSString *urlString = [@"http://m.baidu.com/tc?" stringByAppendingString:[NSString stringWithFormat:@"srd=1&appui=alaxs&ajax=1&alalog=1&gid=%@&tj=wise_novel_book_1_0_10_b1&dir=1",[detailInfo objectForKey:@"gid"]]];
    request = [[ASIHTTPRequest requestWithURL:[NSURL URLWithString:urlString]] retain];
    [request setTimeOutSeconds:20];
    [request setDelegate:self];
    [request setDidFailSelector:@selector(getRequestFailed:)];
    [request setDidFinishSelector:@selector(getRequestComplete:)];
    [request startAsynchronous];
    
   
    [infoDic addEntriesFromDictionary:detailInfo];
    [self reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//排序
- (void)sortListAction:(UIButton *)button
{
    BOOL is_selected = button.selected;
//    if (is_selected == isSelect) {
//        return;
//    }
    
    if (!is_selected) {
        //正序
        button.selected = YES;
        isSelect = YES;
        
//        NSMutableArray *newlist = [[NSMutableArray alloc] init];
//        for (id str  in [catalogArray reverseObjectEnumerator]) {
//            
//            [newlist addObject:str];
//        }
//        [catalogArray removeAllObjects];
//        [catalogArray addObjectsFromArray:newlist];
    }
    else{
        //反序
        button.selected = NO;
        isSelect = NO;
        
//        NSMutableArray *newlist = [[NSMutableArray alloc] init];
//        for (id str  in [catalogArray reverseObjectEnumerator]) {
//            
//            [newlist addObject:str];
//        }
//        [catalogArray removeAllObjects];
//        [catalogArray addObjectsFromArray:newlist];
    }
    
    NSMutableArray *newlist = [[NSMutableArray alloc] init];
    for (id str  in [catalogArray reverseObjectEnumerator]) {
        
        [newlist addObject:str];
    }
    [catalogArray removeAllObjects];
    [catalogArray addObjectsFromArray:newlist];
    [newlist release];
    [catalogListTable reloadData];
}


-(void)changeCatalog:(UIButton *)button
{
    button.selected = YES;
    if(button == desButton)
    {
        sortButton.hidden = YES;
        listButton.selected = NO;
        summaryLabelView.hidden = NO;
        catalogListTable.hidden = YES;
        contentScroller.hidden = NO;
        headView.frame = CGRectMake(0, 0,320, summaryLabelView.frame.size.height+ summaryLabelView.frame.origin.y+50);
        
        catalogListTable.delegate=nil;
        [catalogListTable setTableHeaderView:nil];
        contentScroller.delegate=(id)self;
        [contentScroller setTableHeaderView:headView];
//      [UIView beginAnimations:Nil context:Nil];
        [self scrollViewDidScroll:contentScroller];
//      [UIView commitAnimations];
    }
    else
    {
        sortButton.hidden = NO;
        desButton.selected = NO;
        summaryLabelView.hidden = YES;
        catalogListTable.hidden = NO;
        contentScroller.hidden =  YES;
        headView.frame = CGRectMake(0, 0,320, summaryLabelView.frame.origin.y-45 +catalogBar.frame.size.height+5);
        contentScroller.delegate=nil;
        [contentScroller setTableHeaderView:Nil];
        catalogListTable.delegate=(id)self;
        [catalogListTable setTableHeaderView:headView];
//      [UIView beginAnimations:Nil context:Nil];
        [self scrollViewDidScroll:catalogListTable];
//      [UIView commitAnimations];
        
       
    }

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        //
    
        [self loadProgressView];
        [offlineBookElement deleteBook];
        [self saveCatalog];
        [offlineBookElement startDownBook];
        
//        MBProgressHUD *hud = [[[MBProgressHUD alloc] initWithView:self.view] autorelease];
//        hud.mode = MBProgressHUDModeText;
//        hud.removeFromSuperViewOnHide = YES;;
//        [self.view addSubview:hud];
//        hud.labelText = @"开始下载...";
//        [hud show:YES];
//        [hud hide:YES afterDelay:1];
    
    }
    else  if (buttonIndex==2)
    {
    //阅读
      
        [self readBook:offlineBookElement];
    }
}
-(void)readBook:(EROfflineBookObj *)bookObj
{
//    [[RJBookData sharedRJBookData] loadBookName:bookObj.title withGid:bookObj.gId withPath:[bookObj bookPath] withDirectoryFilePath:[bookObj bookCatalogPath]];
    
    ERReadViewController *readVC = [[ERReadViewController alloc] initWithBookObj:bookObj];
    [self presentViewController:readVC animated:YES completion:nil];
    [readVC release];
}

-(void)saveCatalog
{
    if(catalogArray.count >0)
    {
    
        [EROfflineDownManger creatFolder:[offlineBookElement bookCatalogPath]];
    [[catalogArray JSONRepresentation] writeToFile:[offlineBookElement bookCatalogPath] atomically:YES encoding:NSUTF8StringEncoding error:nil];
//        NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/book/%@.plist",offlineBookElement.gId]];
//
//        [catalogArray writeToFile:path atomically:YES];
    }
}

-(void)offLineRead:(ERButtonCellView *)button
{
     [MobClick event:@"offLineReadEvent" label:offlineBookElement.title];
//    http://npacking.baidu.com/novel/packing?gid=2833112486&filetype=txt
//    整本下载
    
  //判断是否在下载或已下载
   if([NSString_conver NULLStrChangeToStr:offlineBookElement.size] == nil)
   {
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"抱歉" message:@"该书籍暂时不能离线" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
       [alert show];
       [alert release];
       return;
   }
//   if(button)
//   {
    waitRead = YES;
//   }
  
        if([offlineBookElement.size intValue] < [[infoDic objectForKey:@"size"] intValue])
        {//已下载完成
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"有更新" message:@"是否更新到最新的文件" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"更新",@"继续阅读" ,nil];
            [alert show];
            [alert release];
        }
        else{
            
            switch (offlineBookElement.downLoadStatus) {
                case EROfflineBookDownLoadStatusIsUnDown:
                {
                    [self loadProgressView];
                    [self saveCatalog];
                    [offlineBookElement startDownBook];
                    
//                    MBProgressHUD *hud = [[[MBProgressHUD alloc] initWithView:self.view] autorelease];
//                    hud.mode = MBProgressHUDModeText;
//                    hud.removeFromSuperViewOnHide = YES;;
//                    [self.view addSubview:hud];
//                    hud.labelText = @"开始下载...\n请稍等";
//                    [hud show:YES];
//                    [hud hide:YES afterDelay:1];
                }
                    break;
                case EROfflineBookLoadStatusIsWait:
                case EROfflineBookLoadStatusIsDowning:
                case EROfflineBookDownLoadStatusIsPaused:
                    if([[EROfflineDownManger shareManger] queueContainsBook:offlineBookElement])
                    {
                        [offlineBookElement paushDown];
                        
                    }
                    else
                    {
                        [offlineBookElement startDownBook];
                    }
                    break;
                case EROfflineBookDownLoadStatusIsDowned:
                {
//                    [[RJBookData sharedRJBookData] loadBookName:bookObj.title withPath:[bookObj bookPath] withDirectoryFilePath:[bookObj bookCatalogPath]];
                    
                    
                       [self readBook:offlineBookElement];
                }
                    break;
            }
            
           
           
           
     
        }
 
}
- (void)loadWebView:(UIButton *)sender
{
    if( [[ERDataBase shareDBControl] queryBookExist:[infoDic objectForKey:@"gid"]])
    {
        NSMutableDictionary *dic= [NSMutableDictionary dictionaryWithDictionary: [[ERDataBase shareDBControl] queryBookforGid:[infoDic objectForKey:@"gid"]]];
    
        NSString *href = [dic objectForKey:@"lastRedUrl"];
        if (href && ![href isEqualToString:@""]) {
            NSString *path = href;//[NSString stringWithFormat:@"http://m.baidu.com/tc?appui=alaxs&tj=book_cate_1_1_1&gid=%@&src=%@",[infoDic objectForKey:@"gid"],href];
//            path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            ERWebViewController   *bookWebViewController = [[ERWebViewController alloc] initWithNibName:nil bundle:nil];
            bookWebViewController.currentCatalogType = currentCatalogType;
            [bookWebViewController setDownBlock:^{
                [self offLineRead:nil];
            }];
            [self presentViewController:bookWebViewController animated:YES completion:^{
                [bookWebViewController loadWebView:path withBookName:[infoDic objectForKey:@"title"] withInfo:favDic];
            }];
            [bookWebViewController release];
        }
        else{
            if(catalogArray.count==0)return;
            NSDictionary *contentDic = [catalogArray objectAtIndex:0];
            NSString *href =[contentDic objectForKey:@"href"];
            if (href && ![href isEqualToString:@""]) {
                NSString *path = [NSString stringWithFormat:@"http://m.baidu.com/tc?appui=alaxs&tj=book_cate_1_1_1&gid=%@#!/zw/%@",[infoDic objectForKey:@"gid"],href];
                //            path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                ERWebViewController   *bookWebViewController = [[ERWebViewController alloc] initWithNibName:nil bundle:nil];
                bookWebViewController.currentCatalogType = currentCatalogType;
                [bookWebViewController setDownBlock:^{
                    [self offLineRead:nil];
                }];
                [self presentViewController:bookWebViewController animated:YES completion:^{
                    [bookWebViewController loadWebView:path withBookName:[infoDic objectForKey:@"title"] withInfo:favDic];
                }];
                [bookWebViewController release];
            }
        }
    }
    else{
        if(catalogArray.count==0)return;
        NSDictionary *contentDic = [catalogArray objectAtIndex:0];
        NSString *href =[contentDic objectForKey:@"href"];
        if (href && ![href isEqualToString:@""]) {
            NSString *path = [NSString stringWithFormat:@"http://m.baidu.com/tc?appui=alaxs&tj=book_cate_1_1_1&gid=%@#!/zw/%@",[infoDic objectForKey:@"gid"],href];
//            path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            ERWebViewController   *bookWebViewController = [[ERWebViewController alloc] initWithNibName:nil bundle:nil];
            bookWebViewController.currentCatalogType = currentCatalogType;
            [bookWebViewController setDownBlock:^{
                [self offLineRead:nil];
            }];
            [self presentViewController:bookWebViewController animated:YES completion:^{
                [bookWebViewController loadWebView:path withBookName:[infoDic objectForKey:@"title"] withInfo:favDic];
            }];
            [bookWebViewController release];
        }
    }
    
    
}

- (void)loadLastChapterWebView:(UIButton *)sender
{
    

        NSDictionary *lastChapterDic= [infoDic objectForKey:@"lastChapter"];
        if(lastChapterDic)
        {
            NSString *href =[lastChapterDic objectForKey:@"href"];
            if (href && ![href isEqualToString:@""]) {
                NSString *path = [NSString stringWithFormat:@"http://m.baidu.com/tc?appui=alaxs&tj=book_cate_1_1_1&gid=%@#!/zw/%@",[infoDic objectForKey:@"gid"],href];
//                path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                ERWebViewController   *bookWebViewController = [[ERWebViewController alloc] initWithNibName:nil bundle:nil];
                bookWebViewController.currentCatalogType = currentCatalogType;
                [bookWebViewController setDownBlock:^{
                    [self offLineRead:nil];
                }];
                [self presentViewController:bookWebViewController animated:YES completion:^{
                    [bookWebViewController loadWebView:path withBookName:[infoDic objectForKey:@"title"] withInfo:favDic];
                }];
                [bookWebViewController release];
            }

        }

    
}

//分享
- (void)shareAciton:(UIButton *)button
{

    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"icon_120" ofType:@"png"];
    
    NSString *urlStr = [[infoDic objectForKey:@"coverImage"] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
    id<ISSCAttachment> imageAttach = [ShareSDK imageWithPath:imagePath];
    
    if (urlStr && ![urlStr isEqualToString:@""]) {
        imageAttach = [ShareSDK imageWithUrl:urlStr];
    }
    
    SSPublishContentMediaType mediaType = SSPublishContentMediaTypeNews;

    
    NSString *content = [NSString stringWithFormat:@"我正在使用小说宅阅读《%@》。。快来下载一个吧!下载地址:%@",[infoDic objectForKey:@"title"],APPStoreURL];
    NSString *url = APPStoreURL;

    id<ISSContainer> container = [ShareSDK container];
    [container setIPhoneContainerWithViewController:self];
    

    id<ISSContent> contentObj = [ShareSDK content:content
                                   defaultContent:@""
                                            image:imageAttach
                                            title:[infoDic objectForKey:@"title"]
                                              url:url
                                      description:nil
                                        mediaType:mediaType];
    
   
    id<ISSShareOptions> shareOptions = [ShareSDK defaultShareOptionsWithTitle:@"分享"
                                                              oneKeyShareList:nil
                                                           cameraButtonHidden:NO
                                                          mentionButtonHidden:YES
                                                            topicButtonHidden:YES
                                                               qqButtonHidden:YES
                                                        wxSessionButtonHidden:YES
                                                       wxTimelineButtonHidden:YES
                                                         showKeyboardOnAppear:YES
                                                            shareViewDelegate:nil
                                                          friendsViewDelegate:nil
                                                        picViewerViewDelegate:nil];
    
    [ShareSDK showShareActionSheet:container shareList:nil content:contentObj statusBarTips:YES authOptions:nil shareOptions:shareOptions result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
        if (state == SSPublishContentStateSuccess)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"提示"
                                                                message:@"分享成功!"
                                                               delegate:nil
                                                      cancelButtonTitle: @"知道了"
                                                      otherButtonTitles: nil];
            [alertView show];
            [alertView release];
        }
        else if (state == SSPublishContentStateFail)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"提示"
                                                                message:error.errorDescription
                                                               delegate:nil
                                                      cancelButtonTitle: @"知道了"
                                                      otherButtonTitles: nil];
            [alertView show];
            [alertView release];
        }
    }];
    


}

-(void)comment:(UIButton *)button
{
    [MobClick event:@"commentEvent" label:[infoDic objectForKey:@"title"]];
    
    SSCCommentListViewController *commentVC =
    [self presentCommentListViewControllerWithContentId:[infoDic objectForKey:@"gid"] title:[infoDic objectForKey:@"title"] animated:YES];

    
    adView.frame = CGRectMake(0, commentVC.view.frame.size.height-adView.frame.size.height, adView.frame.size.width, adView.frame.size.height);
    CGRect frame = commentVC.view.bounds;
    float height = frame.size.height;
    
    if (IsIOS7) {
        height = height-20;
        frame.origin.y = frame.origin.y + 20;
    }
    
    frame.size.height  = height-adView.frame.size.height;
    [commentVC.view addSubview:adView];
 
    for (UIView *subView in commentVC.view.subviews) {
        if([subView isKindOfClass:NSClassFromString(@"SSCCommentListView")])
        {
            subView.frame = frame;
        }
        else  if([subView isKindOfClass:NSClassFromString(@"SSCCommentPageToolbar")])
        {
         subView.frame = CGRectMake(0, subView.frame.origin.y-adView.frame.size.height,  subView.frame.size.width,  subView.frame.size.height);
        }
            
    }
//    for (id tempView in [commentVC.view subviews]) {
//        if ([[tempView class] isSubclassOfClass:NSClassFromString(@"SSCCommentPageToolbar")]) {
//            UIView *tool = (UIView*)tempView;
//            ((UIView *)[[tool subviews] objectAtIndex:1]).hidden = YES;
//         
//        }
//    }
    
//    GRCommentViewController   *bookWebViewController = [[GRCommentViewController alloc] initWithNibName:nil bundle:nil];
//    
//    [self.navigationController pushViewController:commentVC animated:YES];
//    [bookWebViewController setfinishBlock:^(int count) {
//        
//    }];
//    
//    [bookWebViewController setGameObj:infoDic];
//
////    UIBarButtonItem *temporaryBarButtonItem=[[UIBarButtonItem alloc] init];
////    temporaryBarButtonItem.title=@"返回";
////    self.navigationItem.backBarButtonItem = temporaryBarButtonItem;
////    [temporaryBarButtonItem release];
//    [bookWebViewController release];
    
}

- (void)chaseBook:(UIButton *)button
{
    [MobClick event:@"chaseBookEvent" label:[infoDic objectForKey:@"title"]];
    
    //标题 作者  gid 状态
    [favDic setObject:[NSNumber numberWithBool:YES] forKey:@"fav"];
    [[ERDataBase shareDBControl] insertBook:favDic];
    

    if([[ERDataBase shareDBControl] queryBookIsFav:[infoDic objectForKey:@"gid"]])
    {
        chaseBookButton.enabled = NO;
        [chaseBookButton setTitle:@"已收藏" forState:UIControlStateNormal];
    }
    else {
        chaseBookButton.enabled = YES;
        [chaseBookButton setTitle:@"收藏" forState:UIControlStateNormal];
    }
    ObjRelease(hudView);
    hudView = [[MBProgressHUD alloc] initWithView:self.view];
    hudView.removeFromSuperViewOnHide = YES;;
    [self.view addSubview:hudView];
    hudView.mode = MBProgressHUDModeText;
    hudView.labelText = @"收藏成功!";
    [hudView show:YES];
    [hudView hide:YES afterDelay:2.0];
}
//http://m.baidu.com/from=0/ssid=0/uid=undefined/baiduid=8F18B084E5E2EBEDB0EC7605F26A3AFC/bd_page_type=1/tc?appui=alaxs&srd=1&dir=1&ref=book_iphone&tj=book_cate_1_1_1&gid=2833112486&sec=35571&di=1beb931593296bd9#!/dir/http://www.vodtw.com/Html/Book/0/420/Index.html


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView.contentOffset.y>=summaryLabelView.frame.origin.y-45)
    {
//        if(catalogBar.superview == self.view)return;
        [self.view addSubview:catalogBar];
        
        CGRect frame = self.view.bounds;
        float height = frame.size.height - self.navigationController.navigationBar.frame.size.height;
        
        if (IsIOS7) {
            frame.origin.y =self.navigationController.navigationBar.frame.size.height;
            height = height-20;
            frame.origin.y = frame.origin.y + 20;
        }
        
        catalogBar.frame = CGRectMake(0, frame.origin.y, 320, catalogBar.frame.size.height);
    }
    else{
//        if(catalogBar.superview == headView)return;
        [headView addSubview:catalogBar];
        catalogBar.frame = CGRectMake(0, summaryLabelView.frame.origin.y-45, 320, catalogBar.frame.size.height);
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (totalNum - NumPage * currentIndex)>NumPage?NumPage:(totalNum - NumPage * currentIndex);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier=@"cell";
    UITableViewCell* cell=(UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
       
        
        UIView *lineView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)] autorelease];
        lineView.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.8];
        [cell addSubview:lineView];
    }
    
    if (catalogArray && catalogArray.count) {
        NSRange range;
        range.location = NumPage * currentIndex;
        range.length = (catalogArray.count - NumPage * currentIndex)>NumPage?NumPage:(catalogArray.count - NumPage * currentIndex);
    
        NSArray *array = [catalogArray subarrayWithRange:range];
        
        cell.tag=indexPath.row;
        cell.textLabel.font =[UIFont systemFontOfSize:14];
        cell.textLabel.text = [[array objectAtIndex:indexPath.row] objectForKey:@"text"];
    }

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSRange range;
    range.location = NumPage * currentIndex;
    range.length = (catalogArray.count - NumPage * currentIndex)>NumPage?NumPage:(catalogArray.count - NumPage * currentIndex);;
    NSArray *array = [catalogArray subarrayWithRange:range];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *contentDic = [array objectAtIndex:indexPath.row];
 
    
    NSString *href =[contentDic objectForKey:@"href"];
    if (href && ![href isEqualToString:@""]) {
        NSString *path = [NSString stringWithFormat:@"http://m.baidu.com/tc?appui=alaxs&tj=book_cate_1_1_1&gid=%@#!/zw/%@",[infoDic objectForKey:@"gid"],href];
//        path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        ERWebViewController   *bookWebViewController = [[ERWebViewController alloc] initWithNibName:nil bundle:nil];
        bookWebViewController.currentCatalogType = currentCatalogType;
        [bookWebViewController setDownBlock:^{
            [self offLineRead:nil];
        }];
        [self presentViewController:bookWebViewController animated:YES completion:^{
            [bookWebViewController loadWebView:path withBookName:[infoDic objectForKey:@"title"] withInfo:favDic];
        }];
        [bookWebViewController release];
    }
    


}
-(void)loadProgressView
{
    if(progressView == nil)
    {
        progressView =  [[PICircularProgressView alloc] initWithFrame:CGRectMake(downloadButton.frame.size.width-40, 5, 30, 30)];
        [progressView setThicknessRatio:2];
        
        [progressView setInnerBackgroundColor:[UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1]];
        [progressView setTextColor:[UIColor blackColor]];
        [progressView setShowFontSize:14];
//        [progressView setShowText:NO];
        [downloadButton addSubview:progressView];
      
    }
}
//============下载代理=================
-(void)EROfflineBookPauseDown      //暂停下载
{
     progressView.hidden = NO;
     [progressView setShowTextSring:@"暂停"];
}
-(void)EROfflineBookDownStartDown  // 开始下载
{
     progressView.hidden = NO;
    [progressView setShowTextSring:@"等待"];
}
-(void)EROfflineBookDownFailed     // 下载失败
{
    [progressView setShowTextSring:@"失败"];
    MBProgressHUD *hud = [[[MBProgressHUD alloc] initWithView:self.view] autorelease];
    hud.mode = MBProgressHUDModeText;
    hud.removeFromSuperViewOnHide = YES;;
    [self.view addSubview:hud];
    hud.labelText = @"离线失败,请重试";
    [hud show:YES];
    [hud hide:YES afterDelay:2];
}
-(void)EROfflineBookDownFinish     // 下载完成
{
    if(progressView)
    {
        [progressView removeFromSuperview];
    }
    if(waitRead)
    {
        [self readBook:offlineBookElement];
    }
    else {
    
        MBProgressHUD *hud = [[[MBProgressHUD alloc] initWithView:self.view] autorelease];
        hud.mode = MBProgressHUDModeText;
        hud.removeFromSuperViewOnHide = YES;;
        [self.view addSubview:hud];
        hud.labelText = @"离线完成";
        [hud show:YES];
        [hud hide:YES afterDelay:1];
    }
}
-(void)EROfflineBookCancelDown     // 取消下载
{
    if(progressView)
    {
        [progressView removeFromSuperview];
        
    }
}
-(void)EROfflineBookDowning        //下载中
{
     progressView.hidden = NO;
        [progressView setShowTextSring:nil];
}
-(void)EROfflineBookDownProgress:(float)progressValue{
    progressView.progress =progressValue;

}

@end
