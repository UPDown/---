//
//  EROfflineManagerViewController.m
//  EReader
//
//  Created by Hudajiang on 14-1-20.
//  Copyright (c) 2014年 ER. All rights reserved.
//

#import "EROfflineManagerViewController.h"
#import "ERReadViewController.h"
#import "ERDataBase.h"
#import "EROfflineBookObj.h"
#import "EROfflineDownManger.h"
#import "PICircularProgressView.h"
@interface OfflineTableViewCell : UITableViewCell<EROfflineBookObjDelegate>
{

}
@property (nonatomic,retain)UILabel *titleLabel;
@property (nonatomic,retain)UILabel *bookSizeLabel;
@property (nonatomic,retain)UILabel *autorLabel;
@property (nonatomic,retain)PICircularProgressView *progressView;

@property (nonatomic,retain)EROfflineBookObj *bookObj;

- (void)loadData;
@end

@implementation OfflineTableViewCell
@synthesize titleLabel;
@synthesize bookSizeLabel;
@synthesize autorLabel;
@synthesize progressView;
@synthesize bookObj = _bookObj;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, 10, 200, 25)] autorelease];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [self.contentView addSubview:self.titleLabel];
        
        
        self.autorLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, 35, 100, 15)] autorelease];
        //        self.bookSizeLabel.hidden = YES;
        self.autorLabel.backgroundColor = [UIColor clearColor];
        self.autorLabel.textColor = [UIColor grayColor];
        self.autorLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.autorLabel];
        
        self.bookSizeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, 50, 100, 15)] autorelease];
//        self.bookSizeLabel.hidden = YES;
        self.bookSizeLabel.backgroundColor = [UIColor clearColor];
        self.bookSizeLabel.textColor = [UIColor grayColor];
        self.bookSizeLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.bookSizeLabel];
        
//        self.stateLabel = [[[UILabel alloc] initWithFrame:CGRectMake(230, 20, 80, 30)] autorelease];
//        self.stateLabel.backgroundColor = [UIColor clearColor];
//        self.stateLabel.textColor = [UIColor blueColor];
//        self.stateLabel.textAlignment = UITextAlignmentCenter;
//        self.stateLabel.font = [UIFont boldSystemFontOfSize:14.0];
//        [self.contentView addSubview:self.stateLabel];
        
        
        self.progressView =  [[[PICircularProgressView alloc] initWithFrame:CGRectMake(320-50, 15, 40, 40)] autorelease];
        [self.progressView setThicknessRatio:2];
        
        [self.progressView setInnerBackgroundColor:[UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1]];
        [self.progressView setTextColor:[UIColor blackColor]];
        [self.progressView setShowFontSize:20];
        //        [progressView setShowText:NO];
        [self.contentView addSubview:self.progressView];


        
        UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 69, self.frame.size.width, 1)];
        tempView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:tempView];
        tempView.layer.borderColor = [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1].CGColor;
        tempView.layer.borderWidth = 1;
        [tempView release];

        
    }
    return self;
}

- (void)dealloc
{

    self.titleLabel = Nil;
    self.bookSizeLabel = nil;
    self.progressView = nil;
    self.bookObj = nil;
    self.autorLabel = nil;
    [super dealloc];
}

-(void)removeFromSuperview
{
    [self.bookObj removeTarget:(id)self];
    [super removeFromSuperview];
 
}


-(void)setBookObj:(EROfflineBookObj *)newBookObj
{
    [_bookObj removeTarget:self];
    ObjRelease(_bookObj);
    _bookObj=[newBookObj retain];
    [_bookObj addTarget:self];
}
- (void)loadData
{
  
    self.titleLabel.text = self.bookObj.title;
    self.bookSizeLabel.text = [NSString stringWithFormat:@"%.2fM",(float)[self.bookObj.size intValue]/1024/1024];
    self.autorLabel.text =self.bookObj.author;
    self.progressView.progress = self.bookObj.downProgress;
    
    switch (self.bookObj.downLoadStatus) {
        case EROfflineBookDownLoadStatusIsUnDown:
        {

     
            self.progressView.hidden = NO;
        }
            break;
             case EROfflineBookLoadStatusIsWait:
        {
            [self.progressView setShowTextSring:@"等待"];
                   progressView.hidden = NO;
        }
                     break;
        case EROfflineBookLoadStatusIsDowning:
        {
            self.progressView.hidden = NO;
  
            break;
        }
        case EROfflineBookDownLoadStatusIsPaused:
        {
//            if([[EROfflineDownManger shareManger] queueContainsBook:self.bookObj])
//            {
//                [self.progressView setShowTextSring:@"等待"];
//            }
//            else{
                [self.progressView setShowTextSring:@"暂停"];
//            }
//            if([[EROfflineDownManger shareManger] queueContainsBook:self.bookObj])
//            {
//              ;
//            }
//            else{
//                [self.progressView setShowTextSring:@"暂停"];
//            }
        
     
            self.progressView.hidden = NO;
        }
            break;
        case EROfflineBookDownLoadStatusIsDowned:
        {
    
   
            self.progressView.hidden = YES;
        }
            break;
            
        default:
            break;
    }

}
-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    [UIView beginAnimations:nil context:nil];
    if(editing)
    {
    progressView.frame =  CGRectMake(320-50-40, 15, 40, 40);
    }
    else{
    
      progressView.frame =  CGRectMake(320-50, 15, 40, 40);
    }
    [UIView commitAnimations];
}
#pragma mark - - EROfflineBookObjDelegate
//暂停下载
-(void)EROfflineBookPauseDown
{
    self.progressView.hidden = NO;
    [self.progressView setShowTextSring:@"暂停"];
}

// 开始下载
-(void)EROfflineBookDownStartDown
{
    [self.progressView setShowTextSring:@"等待"];
    self.progressView.hidden = NO;
}
-(void)EROfflineBookDownFailed
{
    [self.progressView setShowTextSring:@"失败"];
    [self.bookObj removeTarget:self];
}

-(void)EROfflineBookDownFinish
{
 
    self.progressView.hidden = YES;
    
    [self.bookObj removeTarget:self];
}

-(void)EROfflineBookCancelDown
{
    [self.bookObj removeTarget:self];
}

//下载中
-(void)EROfflineBookDowning
{
    [self.progressView setShowTextSring:nil];
}

-(void)EROfflineBookDownProgress:(float)progressValue
{

    self.progressView.progress = progressValue;
}

//书将要被删除
-(void)EROfflineBookWillDelete
{
    
}

//书被删除
-(void)EROfflineBookDidDelete
{
    [self.bookObj removeTarget:self];
}

@end


@interface EROfflineManagerViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *listTableView;
    
    NSMutableArray  *listArray;
}
@end

@implementation EROfflineManagerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        listArray = [[NSMutableArray alloc] initWithArray:[[EROfflineDownManger shareManger] mergerArray:[[ERDataBase shareDBControl] queryOfflineBooks]]];

        
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *docDir = [[paths objectAtIndex:0] stringByAppendingFormat:@"/onlineBook"];
//        
//        NSFileManager* fm=[NSFileManager defaultManager];
//        if ([fm fileExistsAtPath:docDir]) {
//            NSArray *array = [fm subpathsOfDirectoryAtPath:docDir error:Nil];
//            for (NSString *fileName in array) {
//                NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[docDir stringByAppendingFormat:@"/%@",fileName],@"filePath",fileName,@"fileName", nil];
//                [listArray addObject:dic];
//                [dic release];
//            }
//            
//        }

    }
    return self;
}

- (void)backView
{
    [self dismissModalViewControllerAnimated:YES];
}

 -(void)setEditing:(BOOL)editing animated:(BOOL)animated {
     [super setEditing:editing animated:animated];
     [listTableView setEditing:editing animated:animated];
 }

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];;
    self.view.layer.cornerRadius = 6;
    self.view.layer.masksToBounds = YES;
    self.title = @"离线管理";
    
    UIBarButtonItem *lefItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"返 回", nil) style:UIBarButtonItemStylePlain target:self action:@selector(backView)];
    self.navigationItem.leftBarButtonItem = lefItem;
//    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    CGRect frame = self.view.bounds;
    float height = frame.size.height - self.navigationController.navigationBar.frame.size.height;
    
    if (IsIOS7) {
        self.automaticallyAdjustsScrollViewInsets = NO;

        frame.origin.y =self.navigationController.navigationBar.frame.size.height;
        height = height-20;
        frame.origin.y = frame.origin.y + 20;
    }
    else{
         lefItem.tintColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.9];
        self.navigationItem.rightBarButtonItem.tintColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.9];
    }
    
    frame.size.height  = height;
    
    listTableView = [[UITableView alloc] initWithFrame:frame];
    [self.view addSubview:listTableView];
    listTableView.delegate = self;
    listTableView.dataSource = self;
    listTableView.backgroundColor = [UIColor clearColor];
    
//    if (listArray && listArray.count) {
//        listTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//    }
//    else{
        listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userAddTxtFile) name:@"userAddTxtFile" object:nil];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    ObjRelease(listTableView);
    [listArray release];
    [super dealloc];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)userAddTxtFile
{
    ObjRelease(listArray);
  listArray = [[NSMutableArray alloc] initWithArray:[[EROfflineDownManger shareManger] mergerArray:[[ERDataBase shareDBControl] queryOfflineBooks]]];
    [listTableView reloadData];
}


#pragma mark - Table view data source
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
       
        OfflineTableViewCell *cell = (OfflineTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        EROfflineBookObj *bookObj = [listArray objectAtIndex:indexPath.row];
        [bookObj removeTarget:cell];
        [bookObj deleteBook];
        [listArray removeObjectAtIndex:indexPath.row];
        
        // Delete the row from the data source.
        [listTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    OfflineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[OfflineTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    // Configure the cell...
    
    if (listArray && listArray.count && indexPath.row < listArray.count) {
        EROfflineBookObj *bookObj = [listArray objectAtIndex:indexPath.row];
        cell.bookObj = bookObj;
        [cell loadData];
    }

    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
     OfflineTableViewCell *cell = (OfflineTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell) {
        if (listArray && listArray.count && indexPath.row < listArray.count) {
            EROfflineBookObj *bookObj = [listArray objectAtIndex:indexPath.row];
            
            
            switch (bookObj.downLoadStatus) {
                case EROfflineBookDownLoadStatusIsUnDown:
                {
              
                }
                    break;
                case EROfflineBookLoadStatusIsWait:
                case EROfflineBookLoadStatusIsDowning:
                case EROfflineBookDownLoadStatusIsPaused:
                    if([[EROfflineDownManger shareManger] queueContainsBook:bookObj])
                    {
                        [bookObj paushDown];
                    }
                    else
                    {
                        [bookObj startDownBook];
                    }
                    break;
                case EROfflineBookDownLoadStatusIsDowned:
                {
//                    [[RJBookData sharedRJBookData] loadBookName:bookObj.title withGid:bookObj.gId withPath:[bookObj bookPath] withDirectoryFilePath:[bookObj bookCatalogPath]];
                    
                    ERReadViewController *readVC = [[ERReadViewController alloc] initWithBookObj:bookObj];
                    [self presentViewController:readVC animated:YES completion:nil];
                    [readVC release];
                }
                    break;
                    
                default:
                    break;
            }
            
            
        }
    }

}

@end
