//
//  MIImageBGView.h
//  MIViewer
//
//  Created by helfy  on 13-9-22.
//  Copyright (c) 2013年 MIViewer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIImageBGView : UIView
{
    CGImageRef image;
}

- (void)setImage:(UIImage *)image;
@end
