//
//  ERUserRegistViewController.m
//  EReader
//
//  Created by helfy  on 13-11-26.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import "ERUserRegistViewController.h"
#import "ERApi.h"
#import "FKTextFiledView.h"
#import "MBProgressHUD.h"
#import "ERUserObj.h"
#import "MIImageBGView.h"
#import <QuartzCore/QuartzCore.h>
@interface ERUserRegistViewController ()
{
    FKTextFiledView *userNameTextFiled;
    //    FKTextFiledView *emailTextFiled;
    FKTextFiledView *passwordTextFiled;
    FKTextFiledView *rePasswordTextFiled;
    
    MBProgressHUD *hud;
    ERApi*api;
    
    
    //    NSString *iden;
    
    UIButton *pinglunBtn;
    UIScrollView *bgView ;
    UITextView *textView;
    
    UIButton *malebutton;
    UIButton *femalenButton;
}
@end

@implementation ERUserRegistViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view = [[[MIImageBGView alloc] initWithFrame:self.view.frame] autorelease];
    //  self.view.backgroundColor = [UIColor whiteColor];
    
    self.view.layer.cornerRadius = 6;
    self.view.layer.masksToBounds = YES;
    
    CGRect frame = CGRectMake(0, 0, 0, 0);
    frame.size.height = 44;
    frame.size.width = 320;
    
    UINavigationBar *naviBar = [[UINavigationBar alloc] initWithFrame:frame];
    UIImageView *imageabg = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navbar_bg"]] autorelease];
    imageabg.frame = naviBar.bounds;
    [naviBar addSubview:imageabg];
    
    naviBar.tintColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 1,320, 43)];
    titleLabel.textColor = [UIColor colorWithRed:249/255.0 green:248/255.0 blue:241/255.0 alpha:1];
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    titleLabel.textAlignment = UITextAlignmentCenter;
    titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    titleLabel.numberOfLines = 2;
    titleLabel.text = NSLocalizedString(@"用户注册", nil);
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.backgroundColor = [UIColor clearColor];
    [naviBar addSubview:titleLabel];
    [titleLabel release];
    
    [self.view addSubview:naviBar];
    [naviBar release];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.backgroundColor = [UIColor clearColor];
    [backBtn setBackgroundImage:[[UIImage imageNamed:@"navBar_button_bg_portrait"] stretchableImageWithLeftCapWidth:20 topCapHeight:15] forState:UIControlStateNormal];
    //    [backBtn setBackgroundImage:[[UIImage imageNamed:@"BtnBluePress.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:15] forState:UIControlStateHighlighted];
    backBtn.frame = CGRectMake(7, 7, 55, 30);
    [backBtn setTitle:@"返 回" forState:UIControlStateNormal];
    backBtn.titleLabel.textColor = [UIColor whiteColor];
    backBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [backBtn addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
    [naviBar addSubview:backBtn];
    
    pinglunBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    pinglunBtn.backgroundColor = [UIColor clearColor];
    [pinglunBtn setBackgroundImage:[UIImage imageNamed:@"navBar_button_bg_portrait"] forState:UIControlStateNormal];
    //    [pinglunBtn setBackgroundImage:[UIImage imageNamed:@"BtnBluePress.png"] forState:UIControlStateHighlighted];
    pinglunBtn.frame = CGRectMake(320 - 7 - 55, 7, 55, 30);
    [pinglunBtn setTitle:NSLocalizedString(@"注 册", nil) forState:UIControlStateNormal];
    pinglunBtn.titleLabel.textColor = [UIColor colorWithRed:249/255.0 green:248/255.0 blue:241/255.0 alpha:1];
    pinglunBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [pinglunBtn addTarget:self action:@selector(registration) forControlEvents:UIControlEventTouchUpInside];
    [naviBar addSubview:pinglunBtn];
    
    
    textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 54, 300, 196)];
    textView.hidden = YES;
    //textView.numberOfLines = 100;
    textView.layer.cornerRadius = 10;
    textView.layer.masksToBounds = YES;
    [self.view addSubview:textView];
    textView.textColor = [UIColor grayColor];
    textView.font = [UIFont boldSystemFontOfSize:16];
    textView.text = NSLocalizedString(@"\n\n\n恭喜，注册成功！\n现在您可以吐槽", nil);
    textView.textAlignment = UITextAlignmentCenter;
    
    [textView release];
    
    
    bgView = [[UIScrollView alloc] initWithFrame:CGRectMake(5, 50, 310, self.view.bounds.size.height)];
    bgView.contentSize = CGSizeMake(310, 461);
    bgView.backgroundColor = [UIColor clearColor];
    [bgView setShowsVerticalScrollIndicator:NO];
    [self.view addSubview:bgView];
    
    UIFont *font = [UIFont systemFontOfSize:16];
    UIColor *clolor = [UIColor grayColor];//[UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
    label.text = NSLocalizedString(@"邮 箱:", nil);
    label.adjustsFontSizeToFitWidth = YES;
    label.textColor = clolor;
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    
    //用户名：
    userNameTextFiled = [[FKTextFiledView alloc] initWithFrame:CGRectMake(26, 30, 268, 20)];
    //    userNameTextFiled.borderStyle = UITextBorderStyleRoundedRect;
    userNameTextFiled.delegate = (id)self;
    [bgView addSubview:userNameTextFiled];
    userNameTextFiled.borderStyle = UITextBorderStyleNone;
    userNameTextFiled.font = [UIFont systemFontOfSize:16];
    userNameTextFiled.leftView = label;
    userNameTextFiled.leftViewMode = UITextFieldViewModeAlways;
    
    [label release];
    [userNameTextFiled becomeFirstResponder];
    userNameTextFiled.placeholder = NSLocalizedString(@"您的邮箱地址", nil);
    [userNameTextFiled release];
    
    //    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 64, 70, 13)];
    //    label.text = NSLocalizedString(@"电子邮件:", nil);
    //    //    label.adjustsFontSizeToFitWidth = YES;
    //
    //    label.backgroundColor = [UIColor clearColor];
    //    label.textColor = clolor;
    //    label.font = font;
    //    [bgView addSubview:label];
    //    [label release];
    //    emailTextFiled = [[FKTextFiledView alloc] initWithFrame:CGRectMake(26, 86, 268, 20)];
    //    emailTextFiled.borderStyle = UITextBorderStyleNone;
    //    [bgView addSubview:emailTextFiled];
    //    emailTextFiled.delegate = self;
    //    emailTextFiled.placeholder = NSLocalizedString(@"您的电子邮件", nil);
    //    emailTextFiled.borderStyle = UITextBorderStyleNone;
    //    emailTextFiled.font = [UIFont systemFontOfSize:16];
    //
    //    emailTextFiled.leftView = label;
    //    emailTextFiled.leftViewMode = UITextFieldViewModeAlways;
    //
    //    [label release];
    //    [emailTextFiled release];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(0,0, 40, 20)];
    label.text = NSLocalizedString(@"密 码:", nil);
    label.adjustsFontSizeToFitWidth = YES;
    label.textColor = clolor;
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    //
    //    [bgView addSubview:label];
    //    [label release];
    passwordTextFiled = [[FKTextFiledView alloc] initWithFrame:CGRectMake(26, 86,268, 20)];
    passwordTextFiled.borderStyle = UITextBorderStyleNone;
    [bgView addSubview:passwordTextFiled];
    passwordTextFiled.secureTextEntry = YES;
    
    passwordTextFiled.delegate = (id)self;
    passwordTextFiled.placeholder = NSLocalizedString(@"您的密码", nil);
    passwordTextFiled.borderStyle = UITextBorderStyleNone;
    passwordTextFiled.font = [UIFont systemFontOfSize:16];
    
    passwordTextFiled.leftView = label;
    passwordTextFiled.leftViewMode = UITextFieldViewModeAlways;
    
    [label release];
    [passwordTextFiled release];
    
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 163+20, 320-20*2, 50)];
    //    imageView.image = [[UIImage imageNamed:@"btn_grey"] stretchableImageWithLeftCapWidth:35 topCapHeight:35];
    //
    //    [self.view addSubview:imageView];
    //    [imageView release];
    //
    //    UILabel* tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(26, 178+20, 120, 20)];
    //    tempLabel.backgroundColor = [UIColor clearColor];
    //    tempLabel.font = [UIFont systemFontOfSize:15];
    //    tempLabel.text= @"请选择您的性别:";
    //    [self.view addSubview:tempLabel];
    //    [tempLabel release];
    
    //
    //    label = [[UILabel alloc] initWithFrame:CGRectMake(126+60, 178+10, 40, 20)];
    //    label.backgroundColor = [UIColor clearColor];
    //    label.text= @"男";
    //    [self.view addSubview:label];
    //    [label release];
    //
    //    label = [[UILabel alloc] initWithFrame:CGRectMake(210+60, 178+10, 40, 20)];
    //    label.backgroundColor = [UIColor clearColor];
    //    label.text= @"女";
    //    [self.view addSubview:label];
    //    [label release];
    //
    //    malebutton =[[UIButton alloc] initWithFrame:CGRectMake(210+20, 168+10, 40, 40)];
    //    //    [malebutton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    //    [malebutton setImage:[UIImage imageNamed:@"111.png"] forState:UIControlStateNormal];
    //    [malebutton setImage:[UIImage imageNamed:@"222.png"] forState:UIControlStateSelected];
    //    [malebutton addTarget:self action:@selector(chooseSex:) forControlEvents:UIControlEventTouchUpInside];
    //    [self.view addSubview:malebutton];
    //
    //
    //    femalenButton =[[UIButton alloc] initWithFrame:CGRectMake(126+20, 168+10, 40, 40)];
    //    //    [femalenButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    //    [femalenButton setImage:[UIImage imageNamed:@"111.png"] forState:UIControlStateNormal];
    //    [femalenButton setImage:[UIImage imageNamed:@"222.png"] forState:UIControlStateSelected];
    //    [femalenButton addTarget:self action:@selector(chooseSex:) forControlEvents:UIControlEventTouchUpInside];
    //
    //    [self.view addSubview:femalenButton];
    
    
    //    sexIndx = 0;
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70, 20)];
    label.text = NSLocalizedString(@"验证密码:", nil);
    label.adjustsFontSizeToFitWidth = YES;
    label.textColor = clolor;
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
//
//    [bgView addSubview:label];
//    [label release];
    rePasswordTextFiled = [[FKTextFiledView alloc] initWithFrame:CGRectMake(26, 136, 268, 20)];
    rePasswordTextFiled.borderStyle = UITextBorderStyleNone;
    rePasswordTextFiled.placeholder = NSLocalizedString(@"再次输入密码", nil);
    rePasswordTextFiled.secureTextEntry = YES;
    rePasswordTextFiled.delegate = (id)self;
    rePasswordTextFiled.font = [UIFont systemFontOfSize:16];
    rePasswordTextFiled.leftView = label;
    rePasswordTextFiled.leftViewMode = UITextFieldViewModeAlways;
    [bgView addSubview:rePasswordTextFiled];
    [label release];
    [rePasswordTextFiled release];
    
    
    //     verificationView =[[UIView alloc] initWithFrame:CGRectMake(10, 145, 120, 50)];
    //    [bgView addSubview:verificationView];
    //    //verificationView.layer.contents = (id)[UIImage imageNamed:@"1.png"].CGImage;
    //    [verificationView release];
    
    
    //    captchaImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 220, 120, 50)];
    //    captchaImage.backgroundColor = [UIColor lightGrayColor];
    //    captchaImage.userInteractionEnabled = YES;
    //    UITapGestureRecognizer *tapp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(refreshCaptcha)];
    //    [captchaImage addGestureRecognizer:tapp];
    //    [tapp release];
    //    [bgView addSubview:captchaImage];
    //
    //    activity = [[UIActivityIndicatorView alloc] init];
    //    activity.alpha = 1;
    //    [activity startAnimating];
    //    activity.center = CGPointMake(10+60-activity.frame.size.width/2, 220+25-activity.frame.size.height/2);
    //    [bgView addSubview:activity];
    
    
    
    //    captchaTextFiled = [[UITextField alloc] initWithFrame:CGRectMake(140, 230, 160, 25)];
    //    captchaTextFiled.borderStyle = UITextBorderStyleRoundedRect;
    //    captchaTextFiled.placeholder = NSLocalizedString(@"请输入验证码", nil);
    //      captchaTextFiled.delegate = self;
    //    [bgView addSubview:captchaTextFiled];
    //    [captchaTextFiled release];
    
    [bgView release];
    //    [self performSelectorInBackground:@selector(refreshCaptcha) withObject:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHidden:) name:UIKeyboardWillHideNotification object:nil];
}



-(void)keyboardShow:(NSNotification *)notify
{
    NSDictionary* info = [notify userInfo];
    
    NSValue* aValue = [info objectForKey:@"UIKeyboardBoundsUserInfoKey"];
    //    //键盘的大小
    CGSize keyboardRect = [aValue CGRectValue].size;
    bgView.contentSize = CGSizeMake(310, 320 + keyboardRect.height);
}

-(void)keyboardHidden:(NSNotification *)notify
{
    bgView.contentSize = CGSizeMake(310, 461);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //    if(textField == userNameTextFiled)
    //    {
    //        [bgView setContentOffset:CGPointMake(0, 0) animated:YES];
    //    }
    //      else if(textField == emailTextFiled){
    //       [bgView setContentOffset:CGPointMake(0, 0) animated:YES];
    //      }
    //    else if(textField == passwordTextFiled){
    //     [bgView setContentOffset:CGPointMake(0, emailTextFiled.frame.origin.y) animated:YES];
    //    }
    //    else if(textField == rePasswordTextFiled){
    //     [bgView setContentOffset:CGPointMake(0, emailTextFiled.frame.origin.y) animated:YES];
    //    }
    //    else if(textField == captchaTextFiled){
    //     [bgView setContentOffset:CGPointMake(0, bgView.frame.size.height - kScreenHightWithOutStatusBar) animated:YES];
    //    }
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //    if(iden)
    //    {
    //        [iden release];
    //        iden = nil;
    //    }
    
    
    //    [captchaImage release];
    //    [activity release];
    [malebutton release];
    [femalenButton release];
    [super dealloc];
}

-(void)backToHome
{
    //      [self.navigationController popViewControllerAnimated:YES];
    //     [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController dismissModalViewControllerAnimated:YES];
    //    UIView * vbgview  = nil;
    //    if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) {
    //        vbgview = [[self.view superview] viewWithTag:10031];
    //
    //    }
    //
    //    [UIView animateWithDuration:0.3 animations:^{
    //
    //        vbgview.alpha = .0f;
    //        self.view.frame = CGRectMake(self.view.frame.origin.x, 768, self.view.frame.size.width, self.view.frame.size.height);
    //    }completion:^(BOOL finished) {
    //
    //
    //        [vbgview removeFromSuperview];
    //        [self.view removeFromSuperview];
    //    } ];
    
}

- (void)refreshCaptcha
{
    
    //    NSString *url = [RedditAPIs getNewCaptchaUrl];
    //
    //    if (iden) {
    //        [iden release];
    //    }
    //    iden = [url stringByReplacingOccurrencesOfString:@"http://www.reddit.com/captcha/" withString:@""];
    //    iden = [[iden stringByReplacingOccurrencesOfString:@".png" withString:@""] copy];
    //    [self performSelectorOnMainThread:@selector(setImageView:) withObject:url waitUntilDone:NO];
}

- (void)setImageView:(NSString *)url
{
    
    //    [captchaImage setImageWithURL:[NSURL URLWithString:url]];
    //    activity.alpha = 0;
    //    [activity stopAnimating];
}

-(BOOL)check:(UITextField *)textFiled
{
    if(textFiled.text)
    {
        if(![textFiled.text isEqualToString:@""])
        {
            return NO;
        }
    }
    
    return YES;
}

-(BOOL)isValidateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(BOOL)isValidateUserName:(NSString *)userName
{
    
    if([userName lengthOfBytesUsingEncoding:NSUTF8StringEncoding] >32)return NO;
    
    return YES;
    //    NSString *emailRegex = @"^[a-zA-Z0-9]+$";
    //    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    //    return [emailTest evaluateWithObject:userName];
}

-(void)showAlertWithTitle:(NSString *)message
{
    MBProgressHUD *newHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	newHUD.mode = MBProgressHUDModeText;
	newHUD.labelText = message;
    [newHUD setRemoveFromSuperViewOnHide:YES];
    [newHUD show:YES];
    [newHUD hide:YES afterDelay:2];
}

-(void)registration
{
    
    [userNameTextFiled resignFirstResponder];
    //    [emailTextFiled resignFirstResponder];
    [passwordTextFiled resignFirstResponder];
    [rePasswordTextFiled resignFirstResponder];
    
    //    [captchaTextFiled resignFirstResponder];
    if ([self check:userNameTextFiled]) {
        [self showAlertWithTitle:NSLocalizedString(@"请输入邮箱地址", nil)];
        return;
    }
    else{
        if(![self isValidateEmail:userNameTextFiled.text])
        {
            [self showAlertWithTitle:NSLocalizedString(@"无效的邮箱地址", nil)];
            return;
        }
    }
    //    if ([self check:emailTextFiled]) {
    //        //TODO  邮箱合法性检测
    //        [self showAlertWithTitle:NSLocalizedString(@"请输入邮箱", nil)];
    //        return;
    //    }else{
    //        if(![self isValidateEmail:emailTextFiled.text])
    //        {
    //            [self showAlertWithTitle:NSLocalizedString(@"请输入正确的邮箱", nil)];
    //            return;
    //        }
    //    }
    if (passwordTextFiled.text.length < 6) {
        [self showAlertWithTitle:NSLocalizedString(@"密码不能少于6位", nil)];
        return;
    }
    
    if ([self check:rePasswordTextFiled]) {
        [self showAlertWithTitle:NSLocalizedString(@"请再次输入密码", nil)];
        return;
    }
    if (![rePasswordTextFiled.text isEqualToString:passwordTextFiled.text]) {
        [self showAlertWithTitle:NSLocalizedString(@"两次输入的密码不一致", nil)];
        return;
    }
    
    //    if ([self check:captchaTextFiled]) {
    //        [self showAlertWithTitle:NSLocalizedString(@"请输入验证码", nil)];
    //        return;
    //    }
    
    [userNameTextFiled resignFirstResponder];
    //    [emailTextFiled resignFirstResponder];
    [passwordTextFiled resignFirstResponder];
    [rePasswordTextFiled resignFirstResponder];
    
    //    [captchaTextFiled resignFirstResponder];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	hud.mode = MBProgressHUDModeIndeterminate;
	hud.labelText = @"注册中";
    [hud setRemoveFromSuperViewOnHide:YES];
    [hud show:YES];
    
    api = [[ERApi alloc] init];
    api.apiDelegate = (id)self;
    [api registerWithUserEmail:userNameTextFiled.text userPassword:passwordTextFiled.text];
    
    
}
-(void)ERApiDidFinish:(id)_api  respone:(id)jsonValue{
    
    int status = [[jsonValue objectForKey:@"status"] intValue];
    if(status!=1)
    {
        [self ERApiDidFail:_api error:[jsonValue objectForKey:@"error"]];
        if(status==2)
        {
            [[ERCurrentUserObj currentUserInfo] userLogOut];
        }
    }
    else
    {
        NSDictionary *data =[jsonValue objectForKey:@"data"];
        
        if (hud) {
            [hud hide:YES];
        }
        
        ERCurrentUserObj *currentUser = [ERCurrentUserObj currentUserInfo];
        currentUser.userId = [data objectForKey:@"userId"];
        currentUser.userName = [data objectForKey:@"userName"];
        currentUser.userToken = [data objectForKey:@"token"];
        [currentUser userLogFinish];
        [self backToHome];
    }
    
}
//获取信息失败
-(void)ERApiDidFail:(id)api  error:(NSString *)errorStr{
    if (hud) {
        [hud hide:YES];
    }
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	hud.mode = MBProgressHUDModeText;
	hud.labelText = errorStr;
    [hud setRemoveFromSuperViewOnHide:YES];
    [hud show:YES];
    [hud hide:YES afterDelay:2];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
