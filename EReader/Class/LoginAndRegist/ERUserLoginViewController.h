//
//  ERUserLoginViewController.h
//  EReader
//
//  Created by helfy  on 13-11-26.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ERUserLoginViewController : UIViewController
@property (nonatomic,assign) id delegate;
@property (nonatomic,assign) SEL loginFinish;
@end
