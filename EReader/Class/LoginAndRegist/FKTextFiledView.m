//
//  FKTextFiledView.m
//  fuckex
//
//  Created by helfy  on 13-6-2.
//  Copyright (c) 2013年 helfy . All rights reserved.
//

#import "FKTextFiledView.h"
#import <QuartzCore/QuartzCore.h>
@implementation FKTextFiledView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
       frame.origin.x =  frame.origin.x- 6;
        frame.origin.y = frame.origin.y - 6;
        frame.size.width =  frame.size.width +12;
        frame.size.height = frame.size.height+ 12;
        
        titleTextFiledBg=[[UIImageView alloc] initWithFrame:frame];
//        [self addSubview:titleTextFiledBg];
        titleTextFiledBg.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        titleTextFiledBg.image = [[UIImage imageNamed:@"tv_bg.png"] stretchableImageWithLeftCapWidth:15 topCapHeight:15];
    
        
//        bgView = [[UIView alloc] initWithFrame:frame];
//        bgView.layer.cornerRadius =5;
//        bgView.layer.borderColor = [UIColor colorWithRed:10/255.0 green:30/255.0 blue:25/255.0 alpha:1].CGColor;
//        bgView.layer.borderWidth = 1;
        
    }
    return self;
}
-(void)dealloc
{
    
    if(titleTextFiledBg)
    {
    [titleTextFiledBg release];
    titleTextFiledBg = nil;
    }
    [super dealloc];
//    [bgView release];
//    bgView =nil;
      
}
-(void)setFrame:(CGRect)frame
{
    
    [super setFrame:frame];
    frame.origin.x = frame.origin.x- 6;
    frame.origin.y = frame.origin.y- 6;
    frame.size.width =  frame.size.width +12;
    frame.size.height = frame.size.height+ 12;
    
    titleTextFiledBg.frame = frame;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
        [newSuperview addSubview:titleTextFiledBg];
    [super willMoveToSuperview:newSuperview];

}

-(void)removeFromSuperview{
     [titleTextFiledBg removeFromSuperview];
    [super removeFromSuperview];
   
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
