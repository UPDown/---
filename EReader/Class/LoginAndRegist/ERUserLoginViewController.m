//
//  ERUserLoginViewController.m
//  EReader
//
//  Created by helfy  on 13-11-26.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import "ERUserLoginViewController.h"
#import "ERUserRegistViewController.h"
#import "ERApi.h"
#import "MBProgressHUD.h"
#import "FKTextFiledView.h"
#import "ERUserObj.h"
#import <QuartzCore/QuartzCore.h>
#import "MIImageBGView.h"

#import <ShareSDK/ShareSDK.h>

@interface ERUserLoginViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
//    FKTextFiledView *userNameTextField;
//    FKTextFiledView *passwordTextField;
//    MBProgressHUD *hud;
//    
//    UIButton *pinglunBtn;
//    UIView *bgView ;
    
    UITableView *platformTableView;
}
@end

@implementation ERUserLoginViewController
@synthesize delegate;
@synthesize loginFinish;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        //        self.view.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
/*
-(void)keyboardHidde:(UITapGestureRecognizer *)tap
{
    
    [userNameTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    
}


-(void)showAlertWithTitle:(NSString *)message
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	hud.mode = MBProgressHUDModeText;
	hud.labelText = message;
    [hud setRemoveFromSuperViewOnHide:YES];
    [hud show:YES];
    [hud hide:YES afterDelay:2];
}

-(BOOL)check:(UITextField *)textFiled
{
    if(textFiled.text)
    {
        if(![textFiled.text isEqualToString:@""])
        {
            return NO;
        }
    }
    
    return YES;
}
-(BOOL)isValidateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(void)login
{
    
    [userNameTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    
    if([self check:userNameTextField])
    {
        [self showAlertWithTitle:NSLocalizedString(@"请输入登陆邮箱", nil)];
        return;
    }
    else{
        if(![self isValidateEmail:userNameTextField.text])
        {
            [self showAlertWithTitle:NSLocalizedString(@"无效的邮箱地址", nil)];
            return;
        }
    }
    if ([self check:passwordTextField]) {
        [self showAlertWithTitle:NSLocalizedString(@"密码不能为空", nil)];
        return;
    }
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	hud.mode = MBProgressHUDModeIndeterminate;
	hud.labelText = @"登录中";
    [hud setRemoveFromSuperViewOnHide:YES];
    [hud show:YES];
    
    ERApi *api = [[ERApi alloc] init];
    api.apiDelegate = (id)self;
    //    [api setRequstFailed:@selector(loginFailed:)];
    //    [api setRequstFinish:@selector(loginFinish:)];
    //    [api loginWithUserId:userNameTextField.text userPassword:passwordTextField.text];
    [api loginWithUserEmail:userNameTextField.text userPassword:passwordTextField.text];
    [api release];
}
 

//
-(void)ERApiDidFinish:(id)api  respone:(id)jsonValue{
    
    int status = [[jsonValue objectForKey:@"status"] intValue];
    if(status!=1)
    {
        [self ERApiDidFail:api error:[jsonValue objectForKey:@"error"]];
        if(status==2)
        {
            [[ERCurrentUserObj currentUserInfo] userLogOut];
        }
    }
    else
    {
        NSDictionary *data =[jsonValue objectForKey:@"data"];
        //        [[NSUserDefaults standardUserDefaults] setObject: userNameTextField.text forKey:kUserName];
        //        [[NSUserDefaults standardUserDefaults] setObject:passwordTextField.text forKey:kUserPassword];
        //        [[NSUserDefaults standardUserDefaults] setObject:[data objectForKey:@"token"] forKey:kUserToken];
        //        [[NSUserDefaults standardUserDefaults] setObject:[data objectForKey:@"userId"] forKey:kUserId];
        //        [[NSUserDefaults standardUserDefaults] setObject:[data objectForKey:@"isAdmin"] forKey:kUserIsAdmin];
        //
        
        if (hud) {
            [hud hide:YES];
        }
        
        ERCurrentUserObj *currentUser = [ERCurrentUserObj currentUserInfo];
        //        currentUser.userId = [data objectForKey:@"userId"];
        //        currentUser.userName = [data objectForKey:@"userName"];
        
        [currentUser setUserInfoWithDic:data];
        
        //        currentUser.userToken = [data objectForKey:@"token"];
        [currentUser userLogFinish];
        [delegate performSelector:loginFinish];
        
        [self backToHome];
    }
    
}
//获取信息失败
-(void)ERApiDidFail:(id)api  error:(NSString *)errorStr{
    if (hud) {
        [hud hide:YES];
    }
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	hud.mode = MBProgressHUDModeText;
	hud.labelText = errorStr;
    [hud setRemoveFromSuperViewOnHide:YES];
    [hud show:YES];
    [hud hide:YES afterDelay:2];
}


-(void)registration
{
    ERUserRegistViewController *regist =[[ERUserRegistViewController alloc] init];
    [self.navigationController pushViewController:regist animated:YES];
    [regist release];
    //    [self dismissViewControllerAnimated:YES completion:^(void)
    //     {
    //         UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    //         GRRegistViewController *regist =[[GRRegistViewController alloc] init];
    //         [rootViewController presentModalViewController:regist animated:YES];
    //         [regist release];
    //     }];
    
    
}
*/



-(void)dealloc
{
    [platformTableView release];
//    [userNameTextField release];
//    [passwordTextField release];
    [super dealloc];
}

-(void)backToHome
{
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = kBgColor;

    self.view.layer.cornerRadius = 6;
    self.view.layer.masksToBounds = YES;
    self.title = @"登录授权";
    
    UIBarButtonItem *lefItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"返 回", nil) style:UIBarButtonItemStylePlain target:self action:@selector(backToHome)];
    self.navigationItem.leftBarButtonItem = lefItem;
    
    
    CGRect frame = self.view.bounds;
    float height = frame.size.height - self.navigationController.navigationBar.frame.size.height;
    
    if (IsIOS7) {
        self.automaticallyAdjustsScrollViewInsets = NO;

        frame.origin.y =self.navigationController.navigationBar.frame.size.height;
        height = height-20;
        frame.origin.y = frame.origin.y + 20;
    }
    else{
        lefItem.tintColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.9];
    }

    frame.size.height  = height;
    
    
    platformTableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
    platformTableView.delegate = self;
    platformTableView.dataSource = self;
    platformTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:platformTableView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

//授权登录
- (void)authorizeLogin:(int )index
{
    switch (index) {
        case 0:
        {
            //新浪
            if ([ShareSDK hasAuthorizedWithType:ShareTypeSinaWeibo])
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"是否注销新浪微博帐号?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"注销", nil];
                alertView.tag = 101;
                [alertView show];
                [alertView release];
            }
            else{
                [MobClick event:@"LoginEvent" label:@"新浪微博"];
                [ShareSDK getUserInfoWithType:ShareTypeSinaWeibo authOptions:nil result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {
                    //                if (!hud) {
                    //                    hud=[[ATMHud alloc]init];
                    //                    [self.view addSubview:hud.view];
                    //                }
                    //                [hud setCaption:@"登录中..."];
                    //                [hud show];
                    
                    if (result) {
//                        NSString *uid = userInfo.uid;
//                        NSString *nick = userInfo.nickname;
//                        NSString *profileImage = userInfo.profileImage;
//                        int type = userInfo.type;
                        
                        //                    [self thirdLogin:uid NickName:nick Type:type Avatar:profileImage];
                    }
                    else{
                        //                    [hud hideAfter:0.5];
                        
                        NSLog(@"%i",error.errorCode);
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"微博登录失败" message:error.errorDescription delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertView show];
                    }
                    [platformTableView reloadData];
                    
                }];
            }
 
        }
            break;
        case 1:
        {
            //腾讯微博
            if ([ShareSDK hasAuthorizedWithType:ShareTypeTencentWeibo])
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"是否注销腾讯微博帐号?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"注销", nil];
                alertView.tag = 102;
                [alertView show];
                [alertView release];
                
            }
            else{
                [MobClick event:@"LoginEvent" label:@"腾讯微博"];
                [ShareSDK getUserInfoWithType:ShareTypeTencentWeibo authOptions:nil result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {
                    //                if (!hud) {
                    //                    hud=[[ATMHud alloc]init];
                    //                    [self.view addSubview:hud.view];
                    //                }
                    //                [hud setCaption:@"登录中..."];
                    //                [hud show];
                    
                    if (result) {
//                        NSString *uid = userInfo.uid;
//                        NSString *nick = userInfo.nickname;
//                        NSString *profileImage = userInfo.profileImage;
//                        int type = userInfo.type;
                        
                        
                        //                    [self thirdLogin:uid NickName:nick Type:type Avatar:profileImage];
                    }
                    else{
                        //                    [hud hideAfter:0.5];
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"腾讯微博登录失败" message:error.errorDescription delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertView show];
                    }
                    [platformTableView reloadData];
                }];
            }

        }
            break;
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 101) {
        if (alertView.cancelButtonIndex != buttonIndex) {
            [ShareSDK cancelAuthWithType:ShareTypeSinaWeibo];
            [platformTableView reloadData];
        }
    }
    else if (alertView.tag == 102){
        if (alertView.cancelButtonIndex != buttonIndex) {
            [ShareSDK cancelAuthWithType:ShareTypeTencentWeibo];
            [platformTableView reloadData];
        }
    }
    else
    {
        
    }
}

#pragma mark -Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 54;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[[UIView alloc] init] autorelease];
    view.backgroundColor = [UIColor clearColor];
    
    UILabel *laber = [[UILabel alloc] initWithFrame:CGRectMake(10, 24, 200, 20)];
    laber.text = @"请选择登录方式";
    laber.textColor = [UIColor darkGrayColor];
    laber.backgroundColor = [UIColor clearColor];
    laber.font = [UIFont systemFontOfSize:14.0];
    [view addSubview:laber];
    [laber release];
    
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    // Configure the cell...
    switch (indexPath.row) {
        case 0:
        {
            [cell.imageView setImage:[ShareSDK getClientIconWithType:ShareTypeSinaWeibo]];
            
            if ([ShareSDK hasAuthorizedWithType:ShareTypeSinaWeibo])
            {
                cell.textLabel.text = [ShareSDK currentAuthUserWithType:ShareTypeSinaWeibo].nickname;
                cell.detailTextLabel.text = @"注销帐号";
            }
            else
            {
                cell.textLabel.text = [ShareSDK getClientNameWithType:ShareTypeSinaWeibo];
                cell.detailTextLabel.text = @"绑定帐号";
            }

        }
            break;
        case 1:
        {
            [cell.imageView setImage:[ShareSDK getClientIconWithType:ShareTypeTencentWeibo]];
            
            if ([ShareSDK hasAuthorizedWithType:ShareTypeTencentWeibo])
            {
                cell.textLabel.text = [ShareSDK currentAuthUserWithType:ShareTypeTencentWeibo].nickname;
                cell.detailTextLabel.text = @"注销帐号";
            }
            else
            {
                cell.textLabel.text = [ShareSDK getClientNameWithType:ShareTypeTencentWeibo];
                cell.detailTextLabel.text = @"绑定帐号";
            }
        }
            break;
        default:
            break;
    }
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark -Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self authorizeLogin:indexPath.row];
    
}

@end
