//
//  ERUserRegistViewController.h
//  EReader
//
//  Created by helfy  on 13-11-26.
//  Copyright (c) 2013年 ER. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ERUserRegistViewControllerDelegate <NSObject>

- (IBAction)closeViewController:(id)sender ;
- (void)hiddenViewController;
@end
@interface ERUserRegistViewController : UIViewController
@property(nonatomic,assign)id<ERUserRegistViewControllerDelegate> delegate;
@end
