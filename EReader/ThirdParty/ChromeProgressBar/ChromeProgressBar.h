//
//  ChromeProgressBar.h
//  ChromeProgressBar
//
//  Created by Mario Nguyen on 01/12/11.
//  Copyright (c) 2012 Mario Nguyen. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class UIProgressView;

@interface ChromeProgressBar : UIView {
//	UIColor *_tintColor;
    NSTimer *_animationTimer;
    UIView *progressView;
}
@property(nonatomic,assign) float progress;

- (void) hideWithFadeOut;
- (ChromeProgressBar *)initWithFrame:(CGRect)frame;

- (void)setProgress:(CGFloat)value animated:(BOOL)animated;

@end

